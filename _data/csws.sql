-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Pon 20. čec 2020, 08:10
-- Verze serveru: 10.1.32-MariaDB
-- Verze PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `csws`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `cs_admin_novinky`
--

DROP TABLE IF EXISTS `cs_admin_novinky`;
CREATE TABLE `cs_admin_novinky` (
  `id` int(6) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `published` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_cs_user` int(4) NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_admin_novinky`
--

TRUNCATE TABLE `cs_admin_novinky`;
-- --------------------------------------------------------

--
-- Struktura tabulky `cs_banner_slot`
--

DROP TABLE IF EXISTS `cs_banner_slot`;
CREATE TABLE `cs_banner_slot` (
  `id` int(3) NOT NULL,
  `name` varchar(150) COLLATE utf8_czech_ci DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `active` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_banner_slot`
--

TRUNCATE TABLE `cs_banner_slot`;
--
-- Vypisuji data pro tabulku `cs_banner_slot`
--

INSERT INTO `cs_banner_slot` (`id`, `name`, `code`, `active`) VALUES
(1, 'Nad hlavičkou', 'before-header', 1),
(2, 'Pod hlavičkou', 'after-header', 1),
(3, 'Úvod - 1', 'homepage-1', 1),
(4, 'Úvod - 2', 'homepage-2', 1),
(5, 'Úvod - 3', 'homepage-3', 1),
(6, 'Úvod - 4', 'homepage-4', 1),
(7, 'Úvod - 5', 'homepage-5', 1),
(8, 'Úvod - 6', 'homepage-6', 1),
(9, 'Nad patičkou - 1', 'before-footer-1', 1),
(10, 'Nad patičkou - 2', 'before-footer-2', 1),
(11, 'Nad patičkou - 3', 'before-footer-3', 1),
(12, 'Nad článkem', 'before-article', 1),
(13, 'Pod nadpisem článku', 'after-article-header', 1),
(14, 'Pod článkem', 'after-article', 1),
(15, 'V článku - 1', 'article-1', 1),
(16, 'Patička - 1', 'footer-1', 1),
(17, 'Patička - 2', 'footer-2', 1),
(18, 'Patička - 3', 'footer-3', 1),
(19, 'Patička - 4', 'footer-4', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `cs_banner_type`
--

DROP TABLE IF EXISTS `cs_banner_type`;
CREATE TABLE `cs_banner_type` (
  `id` int(3) NOT NULL,
  `name` varchar(150) COLLATE utf8_czech_ci DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_banner_type`
--

TRUNCATE TABLE `cs_banner_type`;
--
-- Vypisuji data pro tabulku `cs_banner_type`
--

INSERT INTO `cs_banner_type` (`id`, `name`, `code`, `active`) VALUES
(1, 'Vlastní HTML', 'html', 1),
(2, 'HTML (editor)', 'wysiwyg', 1),
(3, 'Vlastní HTML + obrázek', 'image', 1),
(4, 'Článek', 'article', 1),
(5, 'Seznam', 'list', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `cs_cron`
--

DROP TABLE IF EXISTS `cs_cron`;
CREATE TABLE `cs_cron` (
  `id` int(6) NOT NULL,
  `id_cs_web` int(6) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data` longtext COLLATE utf8_czech_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_cron`
--

TRUNCATE TABLE `cs_cron`;
-- --------------------------------------------------------

--
-- Struktura tabulky `cs_longtexts`
--

DROP TABLE IF EXISTS `cs_longtexts`;
CREATE TABLE `cs_longtexts` (
  `id` int(4) NOT NULL,
  `name` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `text` longtext COLLATE utf8_czech_ci,
  `class` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_longtexts`
--

TRUNCATE TABLE `cs_longtexts`;
-- --------------------------------------------------------

--
-- Struktura tabulky `cs_mail`
--

DROP TABLE IF EXISTS `cs_mail`;
CREATE TABLE `cs_mail` (
  `id` int(6) NOT NULL,
  `subject` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` longtext COLLATE utf8_czech_ci,
  `sent` datetime DEFAULT NULL,
  `sender` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `recipient` varchar(150) COLLATE utf8_czech_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_mail`
--

TRUNCATE TABLE `cs_mail`;
--
-- Vypisuji data pro tabulku `cs_mail`
--

INSERT INTO `cs_mail` (`id`, `subject`, `created`, `content`, `sent`, `sender`, `recipient`) VALUES
(1, 'Tipovacka.org - obnova hesla', '2020-03-23 18:51:10', '<div>\n    <h3>Žádost o obnovu hesla</h3>\n    <p>Odkaz pro obnovu hesla: http://tipovacka-admin.local/obnova-hesla/hash=6bf5802ab1cf455ab313f04e96551069</p>\n    <p>Platnost odkazu je do 23. 3. 2020 - 19:21:10.</p>\n    <hr>\n    <p><i>Administrátor</i></p>\n</div>\n', '2020-03-23 18:51:11', NULL, 'apotesil@gmail.com'),
(2, 'Tipovacka.org - obnova hesla', '2020-03-23 18:54:56', '<div>\n    <h3>Žádost o obnovu hesla</h3>\n    <p>Odkaz pro obnovu hesla: http://tipovacka-admin.local/obnova-hesla/hash=a1ed8a57c7252375a24560cd11ace607</p>\n    <p>Platnost odkazu je do 23. 3. 2020 - 19:24:56.</p>\n    <hr>\n    <p><i>Administrátor</i></p>\n</div>\n', '2020-03-23 18:54:57', NULL, 'apotesil@gmail.com');

-- --------------------------------------------------------

--
-- Struktura tabulky `cs_templates`
--

DROP TABLE IF EXISTS `cs_templates`;
CREATE TABLE `cs_templates` (
  `id` int(4) NOT NULL,
  `name` varchar(150) COLLATE utf8_czech_ci DEFAULT NULL,
  `subject` varchar(150) COLLATE utf8_czech_ci DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `template` longtext COLLATE utf8_czech_ci,
  `description` text COLLATE utf8_czech_ci,
  `type` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_templates`
--

TRUNCATE TABLE `cs_templates`;
--
-- Vypisuji data pro tabulku `cs_templates`
--

INSERT INTO `cs_templates` (`id`, `name`, `subject`, `code`, `template`, `description`, `type`) VALUES
(1, 'Nový tiket', '{$tipovacka} - založen nový tiket', 'mail-new-ticket', '<p>Vítej v tipovačce: <strong>{$tipovacka}</strong>.</p>\r\n<p>Tímto emailem potvrzujeme, že tiket <strong>{$name}</strong> byl založen.</p>\r\n<p>Úpravu svých tipů můžeš až do uzávěrky <strong>{$uzaverka}</strong> provádět na adrese <strong>{$tiket_url}</strong>.\r\n</p>\r\n<p>Případně si můžeš svůj tiket otevřít na adrese tipovačky <strong>{$tipovacka_url}</strong>, kde je potřeba zadat email tiketu a kód tiketu <strong>{$tiket_kod}</strong>.\r\n</p>\r\n<p>V období, kdy se zadávají tipy, mohou správci tipovačky upravovat tvé tipy ve smyslu sjednocení jmen sportovců, týmů atd. <em>Např.: ČR, CZE, Česko, Česká republika by byly pro aplikaci čtyři různé tipy a proto je ručně musíme sjednotit.</em> O takto\r\n    provedených změnách budeš informován emailem.</p>\r\n<p>Pro zaslání vstupního poplatku je možné využí tento QR kód, který můžeš načíst v bankovní aplikaci svého telefonu:</p>\r\n<p>\r\n{$qr}\r\n\r\n</p><hr><p>Děkujeme za účast v tipovačce a držíme palce.</p>\r\n<p>Za tým tipovačky Saša.</p><hr>Toto je automaticky generovaný email.<br><p><br></p>', 'Po založení nového tiketu odešle email s kódem a linkem pro úpravy.', 'mail');

-- --------------------------------------------------------

--
-- Struktura tabulky `cs_text`
--

DROP TABLE IF EXISTS `cs_text`;
CREATE TABLE `cs_text` (
  `id` int(6) NOT NULL,
  `textcode` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `content` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `description` varchar(150) COLLATE utf8_czech_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_text`
--

TRUNCATE TABLE `cs_text`;
-- --------------------------------------------------------

--
-- Struktura tabulky `cs_user`
--

DROP TABLE IF EXISTS `cs_user`;
CREATE TABLE `cs_user` (
  `id` int(6) NOT NULL,
  `username` varchar(55) COLLATE utf8_czech_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `salt` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `firstname` text COLLATE utf8_czech_ci NOT NULL,
  `lastname` text COLLATE utf8_czech_ci NOT NULL,
  `status` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `phone` varchar(12) COLLATE utf8_czech_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `sex` varchar(6) COLLATE utf8_czech_ci NOT NULL,
  `id_cs_user_role` int(2) NOT NULL,
  `id_cs_web` int(6) NOT NULL,
  `active` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_user`
--

TRUNCATE TABLE `cs_user`;
--
-- Vypisuji data pro tabulku `cs_user`
--

INSERT INTO `cs_user` (`id`, `username`, `password`, `salt`, `firstname`, `lastname`, `status`, `email`, `phone`, `image`, `sex`, `id_cs_user_role`, `id_cs_web`, `active`) VALUES
(1, 'DarkHelm', '$2y$10$Dk99yCjoWnRLQTIyS86mL.RDbIjwtNebIlF6oIeYX5mA//HM82RNK', 'clpa2019', 'Alexandr', 'Potěšil', 'superadmin', 'apotesil@gmail.com', '', '', '', 1, 1, 1),
(2, '', '$2y$10$jBIaDYcLxbROQLwW3Jo5/u1if0KHOdSZrDiMNhqslT/He9TuEef9O', 'clpa2019', 'Igor', 'Hnízdo', '', 'editor@csadmin.cz', '', '', '', 3, 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `cs_user_current_web`
--

DROP TABLE IF EXISTS `cs_user_current_web`;
CREATE TABLE `cs_user_current_web` (
  `id` int(4) NOT NULL,
  `id_cs_user` int(4) DEFAULT NULL,
  `id_cs_web` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_user_current_web`
--

TRUNCATE TABLE `cs_user_current_web`;
--
-- Vypisuji data pro tabulku `cs_user_current_web`
--

INSERT INTO `cs_user_current_web` (`id`, `id_cs_user`, `id_cs_web`) VALUES
(1, 1, 2),
(2, 2, 1),
(3, 1, 2);

-- --------------------------------------------------------

--
-- Struktura tabulky `cs_user_login`
--

DROP TABLE IF EXISTS `cs_user_login`;
CREATE TABLE `cs_user_login` (
  `id` int(6) NOT NULL,
  `id_cs_user` int(4) NOT NULL,
  `id_cs_web` int(6) DEFAULT NULL,
  `login` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mail` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `error` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_user_login`
--

TRUNCATE TABLE `cs_user_login`;
--
-- Vypisuji data pro tabulku `cs_user_login`
--

INSERT INTO `cs_user_login` (`id`, `id_cs_user`, `id_cs_web`, `login`, `mail`, `error`, `ip`) VALUES
(1, 1, 0, '2019-12-07 18:31:38', NULL, NULL, '127.0.0.1'),
(2, 1, 8, '2019-12-08 11:20:58', NULL, NULL, '127.0.0.1'),
(3, 1, 2, '2019-12-21 13:09:23', NULL, NULL, '127.0.0.1'),
(4, 1, 2, '2020-01-07 17:02:16', NULL, NULL, '127.0.0.1'),
(5, 1, 2, '2020-01-11 17:11:46', NULL, NULL, '127.0.0.1'),
(6, 1, 2, '2020-01-14 18:11:55', NULL, NULL, '127.0.0.1'),
(7, 1, 2, '2020-01-19 17:21:50', NULL, NULL, '127.0.0.1'),
(8, 1, 2, '2020-01-23 06:54:12', NULL, NULL, '127.0.0.1'),
(9, 1, 2, '2020-01-23 06:55:41', NULL, NULL, '127.0.0.1'),
(10, 1, 2, '2020-01-26 18:27:55', NULL, NULL, '127.0.0.1'),
(11, 1, 2, '2020-01-27 07:32:38', NULL, NULL, '127.0.0.1'),
(12, 1, 2, '2020-01-27 20:52:48', NULL, NULL, '127.0.0.1'),
(13, 1, 2, '2020-01-30 12:17:43', NULL, NULL, '127.0.0.1'),
(14, 1, 2, '2020-02-01 22:16:32', NULL, NULL, '127.0.0.1'),
(15, 1, 2, '2020-02-14 19:36:25', NULL, NULL, '127.0.0.1'),
(16, 1, 2, '2020-02-14 22:05:43', NULL, NULL, '127.0.0.1'),
(17, 1, 2, '2020-02-15 12:03:49', NULL, NULL, '127.0.0.1'),
(18, 1, 2, '2020-02-15 14:57:44', NULL, NULL, '127.0.0.1'),
(19, 1, 2, '2020-02-15 22:51:15', NULL, NULL, '127.0.0.1'),
(20, 1, 2, '2020-02-16 21:48:23', NULL, NULL, '127.0.0.1'),
(21, 1, 2, '2020-02-18 00:59:20', NULL, NULL, '127.0.0.1'),
(22, 1, 2, '2020-02-18 19:41:34', NULL, NULL, '127.0.0.1'),
(23, 1, 2, '2020-02-21 21:27:05', NULL, NULL, '127.0.0.1'),
(24, 1, 1, '2020-02-21 21:59:59', NULL, NULL, '127.0.0.1'),
(25, 1, 1, '2020-02-22 00:35:10', NULL, NULL, '127.0.0.1'),
(26, 1, 1, '2020-02-26 22:44:19', NULL, NULL, '127.0.0.1'),
(27, 1, 1, '2020-02-27 00:51:58', NULL, NULL, '127.0.0.1'),
(28, 1, 1, '2020-03-01 19:37:33', NULL, NULL, '127.0.0.1'),
(29, 1, 1, '2020-03-03 19:04:31', NULL, NULL, '127.0.0.1'),
(30, 1, 1, '2020-03-05 21:32:12', NULL, NULL, '127.0.0.1'),
(31, 1, 1, '2020-03-06 22:45:06', NULL, NULL, '127.0.0.1'),
(32, 1, 1, '2020-03-07 16:42:03', NULL, NULL, '127.0.0.1'),
(33, 1, 1, '2020-03-07 20:07:28', NULL, NULL, '127.0.0.1'),
(34, 1, 1, '2020-03-08 13:13:13', NULL, NULL, '127.0.0.1'),
(35, 1, 1, '2020-03-08 14:23:20', NULL, NULL, '127.0.0.1'),
(36, 1, 1, '2020-03-08 19:09:05', NULL, NULL, '127.0.0.1'),
(37, 2, 1, '2020-03-08 23:56:35', NULL, NULL, '127.0.0.1'),
(38, 1, 1, '2020-03-09 20:04:59', NULL, NULL, '127.0.0.1'),
(39, 1, 1, '2020-03-09 21:18:47', NULL, NULL, '127.0.0.1'),
(40, 1, 1, '2020-03-10 08:01:33', NULL, NULL, '127.0.0.1'),
(41, 1, 3, '2020-03-19 17:59:42', NULL, NULL, '127.0.0.1'),
(42, 1, 1, '2020-03-20 19:29:01', NULL, NULL, '127.0.0.1'),
(43, 1, 1, '2020-03-22 15:31:20', NULL, NULL, '127.0.0.1'),
(44, 1, 1, '2020-03-22 23:28:21', NULL, NULL, '127.0.0.1'),
(45, 1, 1, '2020-03-23 17:20:19', NULL, NULL, '127.0.0.1'),
(46, 1, 4, '2020-03-23 18:55:53', NULL, NULL, '127.0.0.1'),
(47, 0, NULL, '2020-03-23 18:56:39', 'apotesil@gmail.com', 'password', '127.0.0.1'),
(48, 1, 4, '2020-03-23 18:56:45', NULL, NULL, '127.0.0.1'),
(49, 1, 4, '2020-03-23 19:01:01', NULL, NULL, '127.0.0.1'),
(50, 0, NULL, '2020-03-23 19:01:24', 'drobna@mdproduction.cz', 'not found', '127.0.0.1'),
(51, 2, 1, '2020-03-23 19:03:20', NULL, NULL, '127.0.0.1'),
(52, 1, 4, '2020-03-23 22:07:01', NULL, NULL, '127.0.0.1'),
(53, 1, 4, '2020-03-23 22:22:39', NULL, NULL, '127.0.0.1'),
(54, 1, 4, '2020-03-23 22:27:47', NULL, NULL, '127.0.0.1'),
(55, 1, 4, '2020-04-20 17:17:59', NULL, NULL, '127.0.0.1'),
(56, 1, 4, '2020-04-21 14:23:51', NULL, NULL, '127.0.0.1'),
(57, 1, 1, '2020-04-24 10:04:41', NULL, NULL, '127.0.0.1'),
(58, 1, 1, '2020-05-29 14:32:07', NULL, NULL, '127.0.0.1'),
(59, 2, 1, '2020-05-29 15:05:52', NULL, NULL, '127.0.0.1'),
(60, 1, 1, '2020-05-29 18:55:28', NULL, NULL, '127.0.0.1'),
(61, 1, 1, '2020-06-02 17:49:11', NULL, NULL, '127.0.0.1'),
(62, 1, 1, '2020-06-13 16:04:27', NULL, NULL, '127.0.0.1'),
(63, 1, 1, '2020-06-14 10:54:06', NULL, NULL, '127.0.0.1'),
(64, 2, 1, '2020-06-14 11:01:11', NULL, NULL, '127.0.0.1'),
(65, 1, 1, '2020-06-14 11:13:24', NULL, NULL, '127.0.0.1'),
(66, 1, 1, '2020-06-18 17:49:23', NULL, NULL, '127.0.0.1'),
(67, 1, 1, '2020-06-24 17:56:13', NULL, NULL, '127.0.0.1'),
(68, 1, 2, '2020-07-09 18:00:26', NULL, NULL, '127.0.0.1'),
(69, 1, 2, '2020-07-13 23:27:33', NULL, NULL, '127.0.0.1'),
(70, 1, 2, '2020-07-14 18:31:43', NULL, NULL, '127.0.0.1'),
(71, 1, 2, '2020-07-16 17:29:08', NULL, NULL, '127.0.0.1'),
(72, 1, 2, '2020-07-17 18:56:26', NULL, NULL, '127.0.0.1'),
(73, 1, 2, '2020-07-17 21:27:43', NULL, NULL, '127.0.0.1'),
(74, 1, 2, '2020-07-18 14:19:18', NULL, NULL, '127.0.0.1'),
(75, 1, 2, '2020-07-18 17:04:39', NULL, NULL, '127.0.0.1'),
(76, 1, 2, '2020-07-19 12:29:08', NULL, NULL, '127.0.0.1');

-- --------------------------------------------------------

--
-- Struktura tabulky `cs_user_renew`
--

DROP TABLE IF EXISTS `cs_user_renew`;
CREATE TABLE `cs_user_renew` (
  `id` int(6) NOT NULL,
  `time` datetime NOT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `finished` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_user_renew`
--

TRUNCATE TABLE `cs_user_renew`;
--
-- Vypisuji data pro tabulku `cs_user_renew`
--

INSERT INTO `cs_user_renew` (`id`, `time`, `email`, `finished`) VALUES
(1, '2020-03-23 18:51:10', 'apotesil@gmail.com', NULL),
(2, '2020-03-23 18:54:56', 'apotesil@gmail.com', '2020-03-23 18:56:30');

-- --------------------------------------------------------

--
-- Struktura tabulky `cs_user_role`
--

DROP TABLE IF EXISTS `cs_user_role`;
CREATE TABLE `cs_user_role` (
  `id` int(4) NOT NULL,
  `name` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `code` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `description` text COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_user_role`
--

TRUNCATE TABLE `cs_user_role`;
--
-- Vypisuji data pro tabulku `cs_user_role`
--

INSERT INTO `cs_user_role` (`id`, `name`, `code`, `description`) VALUES
(1, 'Super administrátor', 'superAdmin', '<p>Absolutn&iacute; nadvl&aacute;da</p>\r\n'),
(2, 'Administrátor', 'admin', ''),
(3, 'Editor', 'editor', '');

-- --------------------------------------------------------

--
-- Struktura tabulky `cs_user_web`
--

DROP TABLE IF EXISTS `cs_user_web`;
CREATE TABLE `cs_user_web` (
  `id` int(6) NOT NULL,
  `id_cs_user` int(5) DEFAULT NULL,
  `id_cs_web` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_user_web`
--

TRUNCATE TABLE `cs_user_web`;
--
-- Vypisuji data pro tabulku `cs_user_web`
--

INSERT INTO `cs_user_web` (`id`, `id_cs_user`, `id_cs_web`) VALUES
(29, 2, 1),
(30, 1, 1),
(31, 1, 2),
(32, 1, 3);

-- --------------------------------------------------------

--
-- Struktura tabulky `cs_web`
--

DROP TABLE IF EXISTS `cs_web`;
CREATE TABLE `cs_web` (
  `id` int(6) NOT NULL,
  `code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `domain` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `theme` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` longtext COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `logo` text COLLATE utf8_unicode_ci,
  `favicon` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_web`
--

TRUNCATE TABLE `cs_web`;
--
-- Vypisuji data pro tabulku `cs_web`
--

INSERT INTO `cs_web` (`id`, `code`, `domain`, `theme`, `active`, `created`, `name`, `settings`, `description`, `logo`, `favicon`) VALUES
(1, NULL, 'www.google.cz', 'zs1', 1, '2019-12-07 19:10:32', 'ZŠ Jedna', '', 'Základní Škola Jednička Liberec<br>', 'resources/cs_web/1-pyeongchang_2018_winter_olympics.svg', NULL),
(2, 'md', 'md.local', 'md', 1, '2020-06-24 18:04:04', 'MD Production', '', '', NULL, '/resources/web/2/2-ikona-md-fef83d30.ico'),
(3, 'namorka', 'namorka.local', 'namorka', 1, '2020-07-19 20:03:34', 'Námořní akademie', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `cs_web_article`
--

DROP TABLE IF EXISTS `cs_web_article`;
CREATE TABLE `cs_web_article` (
  `id` int(4) NOT NULL,
  `id_cs_web_article` int(5) DEFAULT NULL,
  `id_cs_web` int(4) DEFAULT NULL,
  `name` varchar(150) COLLATE utf8_czech_ci DEFAULT NULL,
  `code` varchar(150) COLLATE utf8_czech_ci DEFAULT NULL,
  `annotation` longtext COLLATE utf8_czech_ci,
  `text` longtext COLLATE utf8_czech_ci,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_cs_user` int(4) DEFAULT NULL,
  `class` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `valid_from` datetime DEFAULT NULL,
  `valid_to` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `heading_image` text COLLATE utf8_czech_ci,
  `list_image` text COLLATE utf8_czech_ci,
  `list_annotation` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `item_order` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `item_count` int(4) DEFAULT NULL,
  `show_date` int(1) DEFAULT '0',
  `show_image` int(1) NOT NULL DEFAULT '0',
  `show_tag` int(1) NOT NULL DEFAULT '0',
  `active` int(1) NOT NULL DEFAULT '1',
  `in_menu` int(1) NOT NULL DEFAULT '0',
  `sort` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_web_article`
--

TRUNCATE TABLE `cs_web_article`;
--
-- Vypisuji data pro tabulku `cs_web_article`
--

INSERT INTO `cs_web_article` (`id`, `id_cs_web_article`, `id_cs_web`, `name`, `code`, `annotation`, `text`, `created`, `id_cs_user`, `class`, `valid_from`, `valid_to`, `created_at`, `heading_image`, `list_image`, `list_annotation`, `item_order`, `item_count`, `show_date`, `show_image`, `show_tag`, `active`, `in_menu`, `sort`) VALUES
(1, 0, 2, 'Teste tt', 'teste-tt', '', '<p>wer</p>\r\n', '2020-07-16 21:17:20', 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-07-18 23:40:58', '/resources/web_article/2/1-tiskove_stredisko-c8c9fda8.jpg', '', 'text', 'user', 0, 0, 0, 1, 1, 1, 1),
(2, 1, 2, 'Test podčlánek 2', 'test-podclanek', NULL, '<p>ahoj</p>\r\n', '2020-07-17 18:56:55', 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-07-18 15:47:51', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, 2),
(4, 2, 2, 'Podčlánek 3', 'podclanek-3', '', '<p>asfdads</p>\r\n', '2020-07-17 21:47:09', 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-07-18 23:40:15', NULL, '/resources/web_article/2/4-ffmdia2-94b7a36d.jpg', 'none', 'user', 0, 0, 0, 0, 1, 0, 3);

-- --------------------------------------------------------

--
-- Struktura tabulky `cs_web_article_web_tag`
--

DROP TABLE IF EXISTS `cs_web_article_web_tag`;
CREATE TABLE `cs_web_article_web_tag` (
  `id` int(6) NOT NULL,
  `id_cs_web_article` int(5) DEFAULT NULL,
  `id_cs_web_tag` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_web_article_web_tag`
--

TRUNCATE TABLE `cs_web_article_web_tag`;
--
-- Vypisuji data pro tabulku `cs_web_article_web_tag`
--

INSERT INTO `cs_web_article_web_tag` (`id`, `id_cs_web_article`, `id_cs_web_tag`) VALUES
(4, 1, 2),
(6, 4, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `cs_web_banner`
--

DROP TABLE IF EXISTS `cs_web_banner`;
CREATE TABLE `cs_web_banner` (
  `id` int(6) NOT NULL,
  `id_cs_web` int(6) NOT NULL,
  `id_cs_banner_slot` int(6) NOT NULL,
  `id_cs_banner_type` int(6) NOT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `content` longtext COLLATE utf8_czech_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `class` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `active` int(1) NOT NULL,
  `sort` int(1) NOT NULL,
  `visibility` text COLLATE utf8_czech_ci NOT NULL,
  `valid_from` datetime DEFAULT '0000-00-00 00:00:00',
  `valid_to` datetime DEFAULT '0000-00-00 00:00:00',
  `id_cs_web_list` int(5) DEFAULT NULL,
  `item_count` int(3) DEFAULT NULL,
  `item_order` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `list_annotation` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `list_articles` longtext COLLATE utf8_czech_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_web_banner`
--

TRUNCATE TABLE `cs_web_banner`;
--
-- Vypisuji data pro tabulku `cs_web_banner`
--

INSERT INTO `cs_web_banner` (`id`, `id_cs_web`, `id_cs_banner_slot`, `id_cs_banner_type`, `name`, `content`, `image`, `class`, `active`, `sort`, `visibility`, `valid_from`, `valid_to`, `id_cs_web_list`, `item_count`, `item_order`, `list_annotation`, `list_articles`) VALUES
(1, 2, 3, 1, 'Test', 'asdasdasd', '', '', 1, 1, 'homepage', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 2, 2, 1, 'Pod hlavičkou', 'test pod hlavičkou', '', '', 1, 2, 'homepage', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 2, 16, 4, 'Footer 1x', 'LOGO 2', '', '', 1, 3, '*', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 3, 'random', 'annotation', '[{\"id\":\"2\"},{\"id\":\"4\"}]'),
(4, 2, 9, 1, 'Pred patou', 'fdg', '', '', 1, 4, 'homepage', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `cs_web_banner_web_tag`
--

DROP TABLE IF EXISTS `cs_web_banner_web_tag`;
CREATE TABLE `cs_web_banner_web_tag` (
  `id` int(5) NOT NULL,
  `id_cs_web_banner` int(5) DEFAULT NULL,
  `id_cs_web_tag` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_web_banner_web_tag`
--

TRUNCATE TABLE `cs_web_banner_web_tag`;
--
-- Vypisuji data pro tabulku `cs_web_banner_web_tag`
--

INSERT INTO `cs_web_banner_web_tag` (`id`, `id_cs_web_banner`, `id_cs_web_tag`) VALUES
(1, 3, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `cs_web_list`
--

DROP TABLE IF EXISTS `cs_web_list`;
CREATE TABLE `cs_web_list` (
  `id` int(5) NOT NULL,
  `id_cs_web` int(5) DEFAULT NULL,
  `name` varchar(150) COLLATE utf8_czech_ci DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `settings` longtext COLLATE utf8_czech_ci,
  `active` int(1) NOT NULL DEFAULT '1',
  `class` varchar(150) COLLATE utf8_czech_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_web_list`
--

TRUNCATE TABLE `cs_web_list`;
--
-- Vypisuji data pro tabulku `cs_web_list`
--

INSERT INTO `cs_web_list` (`id`, `id_cs_web`, `name`, `code`, `settings`, `active`, `class`) VALUES
(3, 2, 'Lidé', 'lide', '[{\"name\":\"Jméno\",\"code\":\"jmeno\",\"type\":\"input\"},{\"name\":\"Obrázek\",\"code\":\"image\",\"type\":\"image\"}]', 1, NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `cs_web_longtext`
--

DROP TABLE IF EXISTS `cs_web_longtext`;
CREATE TABLE `cs_web_longtext` (
  `id` int(4) NOT NULL,
  `name` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `text` longtext COLLATE utf8_czech_ci,
  `class` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `id_cs_web` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_web_longtext`
--

TRUNCATE TABLE `cs_web_longtext`;
--
-- Vypisuji data pro tabulku `cs_web_longtext`
--

INSERT INTO `cs_web_longtext` (`id`, `name`, `code`, `text`, `class`, `id_cs_web`) VALUES
(1, 'Odpočet', 'homepage', '<div class=\"countdown-wrapper\" data-countdown=\"Jul 24, 2020 13:00:00\">\r\n    <h1><img src=\"https://upload.wikimedia.org/wikipedia/en/1/1d/2020_Summer_Olympics_logo_new.svg\" alt=\"Tokio 2020\"></h1>\r\n    <div class=\"countdown-box\" role=\"alert\">\r\n        <p>již za</p>\r\n        <p class=\"countdown\"></p>\r\n    </div>\r\n</div>\r\n<div class=\"\">\r\n    <p>Připravujeme testovací tipovačku...</p>\r\n</div>', '', 1),
(2, 'Tipovačka - neveřejná', 'tipovacka-not-visible', '\r\n<p>Tipovačka zatím není veřejná. Vraťte se prosím později.</p>\r\n<p>Děkujeme za pochopení.</p>', '', 1),
(3, 'Tipovačka - termíny pro tiket', 'tipovacka-visible', '\r\n<p>Tikety bude možné zadávat v termínu</p>\r\n<p><b>{$tiket_od} - {$tiket_do}.</b></p>', '', 1),
(4, 'Založení nového tiketu', 'new-ticket', '\r\n<h2>Založení nového tiketu</h2>\r\n<ul>\r\n    <li>Po založení tiketu budete přesměrováni na samotný tiket, kde můžete vyplňovat své tipy.</li>\r\n    <li>Bude Vám na e-mail zaslán odkaz, na kterém je možné tiket do uzávěrky upravovat. </li>\r\n    <li><em>Zároveň Vám bude vygenerován kód, který společně s e-mailem otevře tiket přímo z hlavní stránky tipovačky</em>.</li>\r\n    <li>Po uzávěrce tiketů Vám bude zaslán Váš aktuální tiket ke kontrole.</li>\r\n</ul>', '', 1),
(5, 'Vyplnění tiketu', 'edit-ticket', 'Text', '', 1),
(6, 'Odpočet', 'homepage', '<div class=\"countdown-wrapper\" data-countdown=\"Jul 24, 2020 13:00:00\">\r\n    <h1><img src=\"https://upload.wikimedia.org/wikipedia/en/1/1d/2020_Summer_Olympics_logo_new.svg\" alt=\"Tokio 2020\"></h1>\r\n    <div class=\"countdown-box\" role=\"alert\">\r\n        <p>již za</p>\r\n        <p class=\"countdown\"></p>\r\n    </div>\r\n</div>\r\n<div class=\"\">\r\n    <p>Připravujeme testovací tipovačku...</p>\r\n</div>', '', 2),
(7, 'Tipovačka - neveřejná', 'tipovacka-not-visible', '\r\n<p>Tipovačka zatím není veřejná. Vraťte se prosím později.</p>\r\n<p>Děkujeme za pochopení.</p>', '', 2),
(8, 'Tipovačka - termíny pro tiket', 'tipovacka-visible', '\r\n<p>Tikety bude možné zadávat v termínu</p>\r\n<p><b>{$tiket_od} - {$tiket_do}.</b></p>', '', 2),
(9, 'Založení nového tiketu', 'new-ticket', '\r\n<h2>Založení nového tiketu</h2>\r\n<ul>\r\n    <li>Po založení tiketu budete přesměrováni na samotný tiket, kde můžete vyplňovat své tipy.</li>\r\n    <li>Bude Vám na e-mail zaslán odkaz, na kterém je možné tiket do uzávěrky upravovat. </li>\r\n    <li><em>Zároveň Vám bude vygenerován kód, který společně s e-mailem otevře tiket přímo z hlavní stránky tipovačky</em>.</li>\r\n    <li>Po uzávěrce tiketů Vám bude zaslán Váš aktuální tiket ke kontrole.</li>\r\n</ul>', '', 2),
(10, 'Vyplnění tiketu', 'edit-ticket', 'Text', '', 2),
(11, 'Odpočet', 'homepage', '<div class=\"countdown-wrapper\" data-countdown=\"Jul 24, 2020 13:00:00\">\r\n    <h1><img src=\"https://upload.wikimedia.org/wikipedia/en/1/1d/2020_Summer_Olympics_logo_new.svg\" alt=\"Tokio 2020\"></h1>\r\n    <div class=\"countdown-box\" role=\"alert\">\r\n        <p>již za</p>\r\n        <p class=\"countdown\"></p>\r\n    </div>\r\n</div>\r\n<div class=\"\">\r\n    <p>Připravujeme testovací tipovačku...</p>\r\n</div>', '', 3),
(12, 'Tipovačka - neveřejná', 'tipovacka-not-visible', '\r\n<p>Tipovačka zatím není veřejná. Vraťte se prosím později.</p>\r\n<p>Děkujeme za pochopení.</p>', '', 3),
(13, 'Tipovačka - termíny pro tiket', 'tipovacka-visible', '\r\n<p>Tikety bude možné zadávat v termínu</p>\r\n<p><b>{$tiket_od} - {$tiket_do}.</b></p>', '', 3),
(14, 'Založení nového tiketu', 'new-ticket', '\r\n<h2>Založení nového tiketu</h2>\r\n<ul>\r\n    <li>Po založení tiketu budete přesměrováni na samotný tiket, kde můžete vyplňovat své tipy.</li>\r\n    <li>Bude Vám na e-mail zaslán odkaz, na kterém je možné tiket do uzávěrky upravovat. </li>\r\n    <li><em>Zároveň Vám bude vygenerován kód, který společně s e-mailem otevře tiket přímo z hlavní stránky tipovačky</em>.</li>\r\n    <li>Po uzávěrce tiketů Vám bude zaslán Váš aktuální tiket ke kontrole.</li>\r\n</ul>', '', 3),
(15, 'Vyplnění tiketu', 'edit-ticket', 'Text', '', 3),
(16, 'Odpočet', 'homepage', '<div class=\"countdown-wrapper\" data-countdown=\"Jul 24, 2020 13:00:00\">\r\n    <h1><img src=\"https://upload.wikimedia.org/wikipedia/en/1/1d/2020_Summer_Olympics_logo_new.svg\" alt=\"Tokio 2020\"></h1>\r\n    <div class=\"countdown-box\" role=\"alert\">\r\n        <p>již za</p>\r\n        <p class=\"countdown\"></p>\r\n    </div>\r\n</div>\r\n<div class=\"\">\r\n    <p>Připravujeme testovací tipovačku...</p>\r\n</div>', '', 4),
(17, 'Tipovačka - neveřejná', 'tipovacka-not-visible', '\r\n<p>Tipovačka zatím není veřejná. Vraťte se prosím později.</p>\r\n<p>Děkujeme za pochopení.</p>', '', 4),
(18, 'Tipovačka - termíny pro tiket', 'tipovacka-visible', '\r\n<p>Tikety bude možné zadávat v termínu</p>\r\n<p><b>{$tiket_od} - {$tiket_do}.</b></p>', '', 4),
(19, 'Založení nového tiketu', 'new-ticket', '\r\n<h2>Založení nového tiketu</h2>\r\n<ul>\r\n    <li>Po založení tiketu budete přesměrováni na samotný tiket, kde můžete vyplňovat své tipy.</li>\r\n    <li>Bude Vám na e-mail zaslán odkaz, na kterém je možné tiket do uzávěrky upravovat. </li>\r\n    <li><em>Zároveň Vám bude vygenerován kód, který společně s e-mailem otevře tiket přímo z hlavní stránky tipovačky</em>.</li>\r\n    <li>Po uzávěrce tiketů Vám bude zaslán Váš aktuální tiket ke kontrole.</li>\r\n</ul>', '', 4),
(20, 'Vyplnění tiketu', 'edit-ticket', 'Text', '', 4),
(21, 'Odpočet', 'homepage', '<div class=\"countdown-wrapper\" data-countdown=\"Jul 24, 2020 13:00:00\">\r\n    <h1><img src=\"https://upload.wikimedia.org/wikipedia/en/1/1d/2020_Summer_Olympics_logo_new.svg\" alt=\"Tokio 2020\"></h1>\r\n    <div class=\"countdown-box\" role=\"alert\">\r\n        <p>již za</p>\r\n        <p class=\"countdown\"></p>\r\n    </div>\r\n</div>\r\n<div class=\"\">\r\n    <p>Připravujeme testovací tipovačku...</p>\r\n</div>', '', 2),
(22, 'Tipovačka - neveřejná', 'tipovacka-not-visible', '\r\n<p>Tipovačka zatím není veřejná. Vraťte se prosím později.</p>\r\n<p>Děkujeme za pochopení.</p>', '', 2),
(23, 'Tipovačka - termíny pro tiket', 'tipovacka-visible', '\r\n<p>Tikety bude možné zadávat v termínu</p>\r\n<p><b>{$tiket_od} - {$tiket_do}.</b></p>', '', 2),
(24, 'Založení nového tiketu', 'new-ticket', '\r\n<h2>Založení nového tiketu</h2>\r\n<ul>\r\n    <li>Po založení tiketu budete přesměrováni na samotný tiket, kde můžete vyplňovat své tipy.</li>\r\n    <li>Bude Vám na e-mail zaslán odkaz, na kterém je možné tiket do uzávěrky upravovat. </li>\r\n    <li><em>Zároveň Vám bude vygenerován kód, který společně s e-mailem otevře tiket přímo z hlavní stránky tipovačky</em>.</li>\r\n    <li>Po uzávěrce tiketů Vám bude zaslán Váš aktuální tiket ke kontrole.</li>\r\n</ul>', '', 2),
(25, 'Vyplnění tiketu', 'edit-ticket', 'Text', '', 2),
(26, 'Odpočet', 'homepage', '<div class=\"countdown-wrapper\" data-countdown=\"Jul 24, 2020 13:00:00\">\r\n    <h1><img src=\"https://upload.wikimedia.org/wikipedia/en/1/1d/2020_Summer_Olympics_logo_new.svg\" alt=\"Tokio 2020\"></h1>\r\n    <div class=\"countdown-box\" role=\"alert\">\r\n        <p>již za</p>\r\n        <p class=\"countdown\"></p>\r\n    </div>\r\n</div>\r\n<div class=\"\">\r\n    <p>Připravujeme testovací tipovačku...</p>\r\n</div>', '', 3),
(27, 'Tipovačka - neveřejná', 'tipovacka-not-visible', '\r\n<p>Tipovačka zatím není veřejná. Vraťte se prosím později.</p>\r\n<p>Děkujeme za pochopení.</p>', '', 3),
(28, 'Tipovačka - termíny pro tiket', 'tipovacka-visible', '\r\n<p>Tikety bude možné zadávat v termínu</p>\r\n<p><b>{$tiket_od} - {$tiket_do}.</b></p>', '', 3),
(29, 'Založení nového tiketu', 'new-ticket', '\r\n<h2>Založení nového tiketu</h2>\r\n<ul>\r\n    <li>Po založení tiketu budete přesměrováni na samotný tiket, kde můžete vyplňovat své tipy.</li>\r\n    <li>Bude Vám na e-mail zaslán odkaz, na kterém je možné tiket do uzávěrky upravovat. </li>\r\n    <li><em>Zároveň Vám bude vygenerován kód, který společně s e-mailem otevře tiket přímo z hlavní stránky tipovačky</em>.</li>\r\n    <li>Po uzávěrce tiketů Vám bude zaslán Váš aktuální tiket ke kontrole.</li>\r\n</ul>', '', 3),
(30, 'Vyplnění tiketu', 'edit-ticket', 'Text', '', 3);

-- --------------------------------------------------------

--
-- Struktura tabulky `cs_web_mail`
--

DROP TABLE IF EXISTS `cs_web_mail`;
CREATE TABLE `cs_web_mail` (
  `id` int(6) NOT NULL,
  `code` varchar(150) COLLATE utf8_czech_ci DEFAULT NULL,
  `id_cs_web` int(6) DEFAULT NULL,
  `recipient` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` longtext COLLATE utf8_czech_ci,
  `subject` text COLLATE utf8_czech_ci,
  `sent` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_web_mail`
--

TRUNCATE TABLE `cs_web_mail`;
--
-- Vypisuji data pro tabulku `cs_web_mail`
--

INSERT INTO `cs_web_mail` (`id`, `code`, `id_cs_web`, `recipient`, `created`, `content`, `subject`, `sent`) VALUES
(13, 'mail-admin-tip-updates', 1, 'apotesil@gmail.com', '2020-03-22 18:11:23', '<p>Ahoj,</p><p>posíláme ti aktuální změny ve tvých tipech, které provedl správce tipovačky.</p><p><div class=\"alert ticket-changes\" role=\"alert\">\n        <div class=\"ticket-wrapper\">\n        <h3>Povinné tipy</h3>\n        <div class=\"question-wrapper\">\n            <h4>Roman Koudelka – střední můstek</h4>\n            <div class=\"changes\">\n                <p><span  style=\"color: #888888; margin-right: 10px;\">20. 3. 2020 19:46:27</span>Změna tipu: 20 → 25, Změna žolíka: ANO → NE</p>\n            </div>\n            <div class=\"changes\">\n                <p><span  style=\"color: #888888; margin-right: 10px;\">20. 3. 2020 20:41:59</span>Změna tipu: 25 → 21</p>\n            </div>\n            <div class=\"changes\">\n                <p><span  style=\"color: #888888; margin-right: 10px;\">20. 3. 2020 21:25:05</span>Změna tipu: 21 → 20</p>\n            </div>\n        </div>\n        <div class=\"question-wrapper\">\n            <h4>Šárka Pančochová – slopestyle</h4>\n            <div class=\"changes\">\n                <p><span  style=\"color: #888888; margin-right: 10px;\">20. 3. 2020 20:33:08</span>Změna tipu: 3 → 2</p>\n            </div>\n            <div class=\"changes\">\n                <p><span  style=\"color: #888888; margin-right: 10px;\">20. 3. 2020 21:58:16</span>Změna tipu: 2 → 3</p>\n            </div>\n        </div>\n    </div>\n    <div class=\"ticket-wrapper\">\n        <h3>Zlaté tipy</h3>\n        <div class=\"question-wrapper\">\n            <h4>Curling muži</h4>\n            <div class=\"changes\">\n                <p><span  style=\"color: #888888; margin-right: 10px;\">20. 3. 2020 20:36:33</span>Změna tipu: Kanada → USA</p>\n            </div>\n        </div>\n        <div class=\"question-wrapper\">\n            <h4>Biatlon štafeta Ž</h4>\n            <div class=\"changes\">\n                <p><span  style=\"color: #888888; margin-right: 10px;\">20. 3. 2020 22:00:39</span>Změna žolíka: NE → ANO</p>\n            </div>\n        </div>\n    </div>\n    <div class=\"ticket-wrapper\">\n        <h3>Biatlon</h3>\n        <div class=\"question-wrapper\">\n            <h4>Získá Laura Dahlmaier více individuálních medailí než Kaisa Mäkäräinen?</h4>\n            <div class=\"changes\">\n                <p><span  style=\"color: #888888; margin-right: 10px;\">20. 3. 2020 21:48:17</span>Změna tipu: yes → no</p>\n            </div>\n            <div class=\"changes\">\n                <p><span  style=\"color: #888888; margin-right: 10px;\">20. 3. 2020 21:57:38</span>Změna tipu: NE → ANO</p>\n            </div>\n        </div>\n        <div class=\"question-wrapper\">\n            <h4>Ve kterém individuálním závodě získá některá z žen ČR nejlepší umístění?</h4>\n            <div class=\"changes\">\n                <p><span  style=\"color: #888888; margin-right: 10px;\">20. 3. 2020 21:50:47</span>Změna tipu: 2 → 1</p>\n            </div>\n            <div class=\"changes\">\n                <p><span  style=\"color: #888888; margin-right: 10px;\">20. 3. 2020 21:57:14</span>Změna tipu: hromadný → sprint</p>\n            </div>\n        </div>\n    </div>\n</div>\n</p><p></p><hr>Toto je automaticky generovaný email.<br><p></p>', 'Pchjongcchang 2018 - Přehled úrav tiketu administrátorem', '2020-03-22 18:11:24'),
(14, 'mail-admin-tip-updates', 1, 'apotesil@gmail.com', '2020-03-22 18:51:10', '<p>Ahoj,</p><p>posíláme ti aktuální změny ve tvých tipech, které provedl správce tipovačky.</p><p></p><div class=\"alert ticket-changes\" role=\"alert\">\r\n        <div class=\"ticket-wrapper\">\r\n        <h3>Povinné tipy</h3>\r\n        <div class=\"question-wrapper\">\r\n            <h4>Roman Koudelka – střední můstek</h4>\r\n            <div class=\"changes\">\r\n                <p><span style=\"color: #888888; margin-right: 10px;\">20. 3. 2020 19:46:27</span>Změna tipu: 20 → 25, Změna žolíka: ANO → NE</p>\r\n            </div>\r\n            <div class=\"changes\">\r\n                <p><span style=\"color: #888888; margin-right: 10px;\">20. 3. 2020 20:41:59</span>Změna tipu: 25 → 21</p>\r\n            </div>\r\n            <div class=\"changes\">\r\n                <p><span style=\"color: #888888; margin-right: 10px;\">20. 3. 2020 21:25:05</span>Změna tipu: 21 → 20</p>\r\n            </div>\r\n        </div>\r\n        <div class=\"question-wrapper\">\r\n            <h4>Šárka Pančochová – slopestyle</h4>\r\n            <div class=\"changes\">\r\n                <p><span style=\"color: #888888; margin-right: 10px;\">20. 3. 2020 20:33:08</span>Změna tipu: 3 → 2</p>\r\n            </div>\r\n            <div class=\"changes\">\r\n                <p><span style=\"color: #888888; margin-right: 10px;\">20. 3. 2020 21:58:16</span>Změna tipu: 2 → 3</p>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"ticket-wrapper\">\r\n        <h3>Zlaté tipy</h3>\r\n        <div class=\"question-wrapper\">\r\n            <h4>Curling muži</h4>\r\n            <div class=\"changes\">\r\n                <p><span style=\"color: #888888; margin-right: 10px;\">20. 3. 2020 20:36:33</span>Změna tipu: Kanada → USA</p>\r\n            </div>\r\n        </div>\r\n        <div class=\"question-wrapper\">\r\n            <h4>Biatlon štafeta Ž</h4>\r\n            <div class=\"changes\">\r\n                <p><span style=\"color: #888888; margin-right: 10px;\">20. 3. 2020 22:00:39</span>Změna žolíka: NE → ANO</p>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"ticket-wrapper\">\r\n        <h3>Biatlon</h3>\r\n        <div class=\"question-wrapper\">\r\n            <h4>Získá Laura Dahlmaier více individuálních medailí než Kaisa Mäkäräinen?</h4>\r\n            <div class=\"changes\">\r\n                <p><span style=\"color: #888888; margin-right: 10px;\">20. 3. 2020 21:48:17</span>Změna tipu: yes → no</p>\r\n            </div>\r\n            <div class=\"changes\">\r\n                <p><span style=\"color: #888888; margin-right: 10px;\">20. 3. 2020 21:57:38</span>Změna tipu: NE → ANO</p>\r\n            </div>\r\n        </div>\r\n        <div class=\"question-wrapper\">\r\n            <h4>Ve kterém individuálním závodě získá některá z žen ČR nejlepší umístění?</h4>\r\n            <div class=\"changes\">\r\n                <p><span style=\"color: #888888; margin-right: 10px;\">20. 3. 2020 21:50:47</span>Změna tipu: 2 → 1</p>\r\n            </div>\r\n            <div class=\"changes\">\r\n                <p><span style=\"color: #888888; margin-right: 10px;\">20. 3. 2020 21:57:14</span>Změna tipu: hromadný → sprint</p>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<p></p><p></p><hr>Toto je automaticky generovaný email.<br><p></p>', 'Pchjongcchang 2018 - Přehled úrav tiketu administrátorem', '2020-03-22 18:51:12'),
(16, 'mail-new-ticket', 1, 'potesil@mdproduction.cz', '2020-03-22 22:20:11', '<p>Vítej v tipovačce: <strong>Pchjongcchang 2018</strong>.</p>\r\n<p>Tímto emailem potvrzujeme, že tiket <strong>Alexandr Potěšil</strong> byl založen.</p>\r\n<p>Úpravu svých tipů můžeš až do uzávěrky <strong>31. 3. 2020 0:00</strong> provádět na adrese <strong>https://www.tipovacka.org/pchjongcchang/tiket/tiket=8-b7f6ba9b14b55e24cc7c6398389dbbe415a2057d</strong>.\r\n</p>\r\n<p>Případně si můžeš svůj tiket otevřít na adrese tipovačky <strong>https://www.tipovacka.org/pchjongcchang</strong>, kde je potřeba zadat email tiketu a kód tiketu <strong>0e2799-1</strong>.\r\n</p>\r\n<p>V období, kdy se zadávají tipy, mohou správci tipovačky upravovat tvé tipy ve smyslu sjednocení jmen sportovců, týmů atd. <em>Např.: ČR, CZE, Česko, Česká republika by byly pro aplikaci čtyři různé tipy a proto je ručně musíme sjednotit.</em> O takto\r\n    provedených změnách budeš informován emailem.</p>\r\n<p>Pro zaslání vstupního poplatku (cena: <strong>500 Kč</strong>, č. účtu: \r\n<strong>268078589/0300</strong>, banka: \r\n<strong></strong>) je možné využí tento QR kód, který můžeš načíst v bankovní aplikaci svého telefonu:</p>\r\n<p>\r\n<img src=\"https://www.tipovacka.org/public/images/qr/1/0e2799-1.png\" style=\"width: 150px;\" title=\"QR kód pro platbu\">\r\n\r\n</p><hr><p>Děkujeme za účast v tipovačce a držíme palce.</p>\r\n<p>Za tým tipovačky Saša.</p><hr>Toto je automaticky generovaný email.<br><p><br></p>', 'Pchjongcchang 2018 - Nový tiket', '2020-03-22 22:20:12'),
(17, 'mail-new-ticket', 1, 'apotesil@gmail.com', '2020-03-22 22:43:42', '<p>Vítej v tipovačce: <strong>Pchjongcchang 2018</strong>.</p>\r\n<p>Tímto emailem potvrzujeme, že tiket <strong>Alex</strong> byl založen.</p>\r\n<p>Úpravu svých tipů můžeš až do uzávěrky <strong>31. 3. 2020 0:00</strong> provádět na adrese <strong>https://www.tipovacka.org/pchjongcchang/tiket/tiket=9-e4f1865dd374e1115e19ac92ccceb76c19e13334</strong>.\r\n</p>\r\n<p>Případně si můžeš svůj tiket otevřít na adrese tipovačky <strong>https://www.tipovacka.org/pchjongcchang</strong>, kde je potřeba zadat email tiketu a kód tiketu <strong>85902f-1</strong>.\r\n</p>\r\n<p>V období, kdy se zadávají tipy, mohou správci tipovačky upravovat tvé tipy ve smyslu sjednocení jmen sportovců, týmů atd. <em>Např.: ČR, CZE, Česko, Česká republika by byly pro aplikaci čtyři různé tipy a proto je ručně musíme sjednotit.</em> O takto\r\n    provedených změnách budeš informován emailem.</p>\r\n<p>Pro zaslání vstupního poplatku (cena: <strong>500 Kč</strong>, č. účtu: \r\n<strong>268078589/0300</strong>, banka: \r\n<strong>ČSOB a.s.</strong>, vs: <strong>{$banka_vs}</strong>) je možné využí tento QR kód, který můžeš načíst v bankovní aplikaci svého telefonu:</p>\r\n<p>\r\n<img src=\"https://www.tipovacka.org/public/images/qr/1/85902f-1.png\" style=\"width: 150px;\" title=\"QR kód pro platbu\">\r\n\r\n</p><p><em>Pokud platíš víc tiketů naráz, údaje si patřičně uprav.</em></p><hr><p>Děkujeme za účast v tipovačce a držíme palce.</p>\r\n<p>Za tým tipovačky Saša.</p><hr>Toto je automaticky generovaný email.<br><p><br></p>', 'Pchjongcchang 2018 - Nový tiket', NULL),
(18, 'mail-new-ticket', 4, 'apotesil@gmail.com', '2020-03-23 17:41:23', '<p>Vítej v tipovačce: <strong>Předolympijská tipovačka Tokyo</strong>.</p>\r\n<p>Tímto emailem potvrzujeme, že tiket <strong>Saša</strong> byl založen.</p>\r\n<p>Úpravu svých tipů můžeš až do uzávěrky <strong>24. 3. 2020 0:00</strong> provádět na adrese <strong>https://www.tipovacka.org/loh-2020/tiket/tiket=10-fdfaf7ab62ed875e71eb59d7a9034ae7bd848f96</strong>.\r\n</p>\r\n<p>Případně si můžeš svůj tiket otevřít na adrese tipovačky <strong>https://www.tipovacka.org/loh-2020</strong>, kde je potřeba zadat email tiketu a kód tiketu <strong>b3dffe-4</strong>.\r\n</p>\r\n<p>V období, kdy se zadávají tipy, mohou správci tipovačky upravovat tvé tipy ve smyslu sjednocení jmen sportovců, týmů atd. <em>Např.: ČR, CZE, Česko, Česká republika by byly pro aplikaci čtyři různé tipy a proto je ručně musíme sjednotit.</em> O takto\r\n    provedených změnách budeš informován emailem.</p>\r\n<p>Pro zaslání vstupního poplatku je možné využí tento QR kód, který můžeš načíst v bankovní aplikaci svého telefonu:</p>\r\n<p>\r\n<img src=\"https://www.tipovacka.org/public/images/qr/4/1064760436.png\" style=\"width: 150px;\" title=\"QR kód pro platbu\">\r\n\r\n</p><hr><p>Děkujeme za účast v tipovačce a držíme palce.</p>\r\n<p>Za tým tipovačky Saša.</p><hr>Toto je automaticky generovaný email.<br><p><br></p>', 'Nový tiket', NULL),
(19, 'mail-new-ticket', 4, 'apotesil@gmail.com', '2020-03-23 17:43:57', '<p>Vítej v tipovačce: <strong>Předolympijská tipovačka Tokyo</strong>.</p>\r\n<p>Tímto emailem potvrzujeme, že tiket <strong>Saša</strong> byl založen.</p>\r\n<p>Úpravu svých tipů můžeš až do uzávěrky <strong>24. 3. 2020 0:00</strong> provádět na adrese <strong>https://www.tipovacka.org/loh-2020/tiket/tiket=11-250bc6d7e7f8a64271a1332cb3170fb27db611c3</strong>.\r\n</p>\r\n<p>Případně si můžeš svůj tiket otevřít na adrese tipovačky <strong>https://www.tipovacka.org/loh-2020</strong>, kde je potřeba zadat email tiketu a kód tiketu <strong>86bf9a-4</strong>.\r\n</p>\r\n<p>V období, kdy se zadávají tipy, mohou správci tipovačky upravovat tvé tipy ve smyslu sjednocení jmen sportovců, týmů atd. <em>Např.: ČR, CZE, Česko, Česká republika by byly pro aplikaci čtyři různé tipy a proto je ručně musíme sjednotit.</em> O takto\r\n    provedených změnách budeš informován emailem.</p>\r\n<p>Pro zaslání vstupního poplatku je možné využí tento QR kód, který můžeš načíst v bankovní aplikaci svého telefonu:</p>\r\n<p>\r\n<img src=\"https://www.tipovacka.org/public/images/qr/4/1412113373.png\" style=\"width: 150px;\" title=\"QR kód pro platbu\">\r\n\r\n</p><hr><p>Děkujeme za účast v tipovačce a držíme palce.</p>\r\n<p>Za tým tipovačky Saša.</p><hr>Toto je automaticky generovaný email.<br><p><br></p>', 'Nový tiket', '2020-03-23 17:43:57');

-- --------------------------------------------------------

--
-- Struktura tabulky `cs_web_navigation`
--

DROP TABLE IF EXISTS `cs_web_navigation`;
CREATE TABLE `cs_web_navigation` (
  `id` int(6) NOT NULL,
  `id_cs_web` int(6) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `code` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `position` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `class` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `navigation_items` longtext COLLATE utf8_czech_ci,
  `active` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_web_navigation`
--

TRUNCATE TABLE `cs_web_navigation`;
--
-- Vypisuji data pro tabulku `cs_web_navigation`
--

INSERT INTO `cs_web_navigation` (`id`, `id_cs_web`, `name`, `code`, `position`, `class`, `navigation_items`, `active`) VALUES
(2, 2, 'Hlavní navigace', 'hlavni-navigace', 'main-menu', '', '[{\"name\":\"aa\",\"id_web_article\":\"1\",\"link\":\"\",\"class\":\"\",\"active\":\"1\"},{\"name\":\"\",\"id_web_article\":\"2\",\"link\":\"\",\"class\":\"\",\"active\":\"1\"}]', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `cs_web_tag`
--

DROP TABLE IF EXISTS `cs_web_tag`;
CREATE TABLE `cs_web_tag` (
  `id` int(5) NOT NULL,
  `id_cs_web` int(5) DEFAULT NULL,
  `name` varchar(150) COLLATE utf8_czech_ci DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_web_tag`
--

TRUNCATE TABLE `cs_web_tag`;
--
-- Vypisuji data pro tabulku `cs_web_tag`
--

INSERT INTO `cs_web_tag` (`id`, `id_cs_web`, `name`, `code`) VALUES
(1, 2, 'Novinka', 'novinka'),
(2, 2, 'Blog', 'blog');

-- --------------------------------------------------------

--
-- Struktura tabulky `cs_web_template`
--

DROP TABLE IF EXISTS `cs_web_template`;
CREATE TABLE `cs_web_template` (
  `id` int(4) NOT NULL,
  `id_cs_web` int(4) NOT NULL,
  `code` varchar(150) COLLATE utf8_czech_ci DEFAULT NULL,
  `subject` varchar(250) COLLATE utf8_czech_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `description` text COLLATE utf8_czech_ci,
  `template` longtext COLLATE utf8_czech_ci,
  `id_cs_user` int(4) NOT NULL,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_web_template`
--

TRUNCATE TABLE `cs_web_template`;
-- --------------------------------------------------------

--
-- Struktura tabulky `cs_web_text`
--

DROP TABLE IF EXISTS `cs_web_text`;
CREATE TABLE `cs_web_text` (
  `id` int(6) NOT NULL,
  `id_cs_web` int(6) DEFAULT NULL,
  `textcode` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `content` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `description` varchar(150) COLLATE utf8_czech_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vyprázdnit tabulku před vkládáním `cs_web_text`
--

TRUNCATE TABLE `cs_web_text`;
--
-- Vypisuji data pro tabulku `cs_web_text`
--

INSERT INTO `cs_web_text` (`id`, `id_cs_web`, `textcode`, `content`, `description`) VALUES
(1, 2, 'page.title', 'MD Production', 'název webu'),
(2, 1, 'page.title', 'CSWS', ''),
(3, 3, 'page.title', 'Námořní akademie', '');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `cs_admin_novinky`
--
ALTER TABLE `cs_admin_novinky`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `cs_banner_slot`
--
ALTER TABLE `cs_banner_slot`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `cs_banner_type`
--
ALTER TABLE `cs_banner_type`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `cs_cron`
--
ALTER TABLE `cs_cron`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `cs_longtexts`
--
ALTER TABLE `cs_longtexts`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `cs_mail`
--
ALTER TABLE `cs_mail`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `cs_templates`
--
ALTER TABLE `cs_templates`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `cs_text`
--
ALTER TABLE `cs_text`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `cs_user`
--
ALTER TABLE `cs_user`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `cs_user_current_web`
--
ALTER TABLE `cs_user_current_web`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `cs_user_login`
--
ALTER TABLE `cs_user_login`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `cs_user_renew`
--
ALTER TABLE `cs_user_renew`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `cs_user_role`
--
ALTER TABLE `cs_user_role`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `cs_user_web`
--
ALTER TABLE `cs_user_web`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `cs_web`
--
ALTER TABLE `cs_web`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `cs_web_article`
--
ALTER TABLE `cs_web_article`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `cs_web_article_web_tag`
--
ALTER TABLE `cs_web_article_web_tag`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `cs_web_banner`
--
ALTER TABLE `cs_web_banner`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `cs_web_banner_web_tag`
--
ALTER TABLE `cs_web_banner_web_tag`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `cs_web_list`
--
ALTER TABLE `cs_web_list`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `cs_web_longtext`
--
ALTER TABLE `cs_web_longtext`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `cs_web_mail`
--
ALTER TABLE `cs_web_mail`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `cs_web_navigation`
--
ALTER TABLE `cs_web_navigation`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `cs_web_tag`
--
ALTER TABLE `cs_web_tag`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `cs_web_template`
--
ALTER TABLE `cs_web_template`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `cs_web_text`
--
ALTER TABLE `cs_web_text`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `cs_admin_novinky`
--
ALTER TABLE `cs_admin_novinky`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `cs_banner_slot`
--
ALTER TABLE `cs_banner_slot`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT pro tabulku `cs_banner_type`
--
ALTER TABLE `cs_banner_type`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pro tabulku `cs_cron`
--
ALTER TABLE `cs_cron`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `cs_longtexts`
--
ALTER TABLE `cs_longtexts`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `cs_mail`
--
ALTER TABLE `cs_mail`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pro tabulku `cs_templates`
--
ALTER TABLE `cs_templates`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pro tabulku `cs_text`
--
ALTER TABLE `cs_text`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `cs_user`
--
ALTER TABLE `cs_user`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pro tabulku `cs_user_current_web`
--
ALTER TABLE `cs_user_current_web`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pro tabulku `cs_user_login`
--
ALTER TABLE `cs_user_login`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT pro tabulku `cs_user_renew`
--
ALTER TABLE `cs_user_renew`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pro tabulku `cs_user_role`
--
ALTER TABLE `cs_user_role`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pro tabulku `cs_user_web`
--
ALTER TABLE `cs_user_web`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT pro tabulku `cs_web`
--
ALTER TABLE `cs_web`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pro tabulku `cs_web_article`
--
ALTER TABLE `cs_web_article`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pro tabulku `cs_web_article_web_tag`
--
ALTER TABLE `cs_web_article_web_tag`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pro tabulku `cs_web_banner`
--
ALTER TABLE `cs_web_banner`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pro tabulku `cs_web_banner_web_tag`
--
ALTER TABLE `cs_web_banner_web_tag`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pro tabulku `cs_web_list`
--
ALTER TABLE `cs_web_list`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pro tabulku `cs_web_longtext`
--
ALTER TABLE `cs_web_longtext`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT pro tabulku `cs_web_mail`
--
ALTER TABLE `cs_web_mail`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT pro tabulku `cs_web_navigation`
--
ALTER TABLE `cs_web_navigation`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pro tabulku `cs_web_tag`
--
ALTER TABLE `cs_web_tag`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pro tabulku `cs_web_template`
--
ALTER TABLE `cs_web_template`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `cs_web_text`
--
ALTER TABLE `cs_web_text`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
