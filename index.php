<?php
session_start();
require_once "App/Config/Init.php";
WC::init();

if ($session->isSignedIn()){
	\Classes\Helpers\Functions::redirect('dashboard');
}

$presenterClass = ' page-login';
include Dir::template()."/Html/head.php";

echo '<main>';
	if (is_array($_POST) && count($_POST) > 0) {
		$login = trim($_POST['login']);
		$data = new \Classes\Db\Data('cs_user_login');
		if ($user_found = WC::component()->users()->checkLogin($login)) {
			$the_message = '';
			if ($user_found->getAttr('role') == 'admin') {
				if (!WC::component()->web()->getUserWebs($user_found->getId())) {
					$insert = ['mail' => $login, 'error' => 'web not active', 'ip' => \Classes\Formatters\Strings::getUserIP()];
					$data->insert(['column' => [$insert]]);
					WC::component()->logger()->error('LOGIN FAILED', ['user' => $login, 'data' => $insert]);
					$the_message = 'Nemáte aktivní web.';
				}
			}
			if ($the_message == '') {
				if ($password = password_verify($user_found->getAttr('salt').$_POST['password'], $user_found->getAttr('password'))) {
					$session->login($user_found);
					\Classes\Helpers\Functions::redirect('/');
				} else {
					$insert = ['mail' => $login, 'error' => 'password', 'ip' => \Classes\Formatters\Strings::getUserIP()];
					$data->insert(['column' => [$insert]]);
					WC::component()->logger()->error('LOGIN FAILED', ['user' => $login, 'data' => $insert]);
					$the_message = 'Přihlašovací údaje nejsou platné, uživatel neexistuje nebo není aktivní.<br><span><a href="/obnova-hesla">Zapomněli jste heslo?</a></span>';
				}
			}
		} else {
			$insert = ['mail' => $login, 'error' => 'not found', 'ip' => \Classes\Formatters\Strings::getUserIP()];
			$data->insert(['column' => [$insert]]);
			WC::component()->logger()->error('LOGIN FAILED', ['user' => $login, 'data' => $insert]);
			$the_message = 'Přihlašovací údaje nejsou platné, uživatel neexistuje nebo není aktivní.<br><span><a href="/obnova-hesla">Zapomněli jste heslo?</a></span>';
		}
	}
	$err = '';
	if (isset($the_message)) {
		$err = '<div class="login-error">' . $the_message . '</div>';
	}

	echo \Classes\Helpers\Template::render('User/login', ['message' => $err]);
echo '</main>';
