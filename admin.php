<?php
session_start();
require_once "App/Config/Init.php";

$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$parsed = explode('/', parse_url($actual_link)['path']);
$presenterClass = '';

if (isset($parsed[1]) && $parsed[1] == 'obnova-hesla') {

} else {
	if (!$session->isSignedIn()){
		\Classes\Helpers\Functions::redirect('/');
	}
}
if (isset($_SERVER['REDIRECT_URL'])){
	$parsed = explode('/', parse_url(rtrim($actual_link, '/'))['path']);
	$link = parse_url(rtrim($actual_link, '/'))['path'];
	if(isset($parsed[1])) {
		$processor = \Classes\Helpers\PresenterProcessor::process($parsed[1]);
		$presenter = new $processor($parsed, $session, WC::component()->users());
		$presenterClass = $presenter->class;
	}
	if (!$session->isSignedIn()){
		$presenterClass .= ' error-class';
	}
}

$time = getdate()['0'];

include Dir::template()."/Html/head.php";
include Dir::template()."/Html/body.php";
include Dir::template()."/Html/foot.php";
