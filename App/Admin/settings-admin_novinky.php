<?php
return [
	'groupUser' => 'superAdmin',
	'title' => 'Admin - Novinky',
//	'editable' => 'all',
	'table' => 'cs_admin_novinky',
	'actions' => ['edit','delete', 'create'],
	'create' => true,
	'filter' => ['setColumns' => ['cs_admin_novinky-name', 'cs_admin_novinky-id_cs_user']],
	'paging' => 100,
	'columns' => [
		'id' => [
			'title' => '#id', 'listing' => true, 'edit' => false, 'class' => 'table-th-id'
		],
		'name' => [
			'title' => 'Název', 'type' => 'input', 'listing' => true,
		],
		'content' => [
			'title' => 'Obsah', 'type' => 'paragraph', 'listing' => false,
		],
		'id_cs_user' => [
			'title' => 'Autor', 'type' => 'select', 'listing' => true, 'joined' => 'lastname|firstname', 'order' => 'username'
		],
		'published' => [
			'title' => 'Vydáno', 'type' => 'datetime', 'listing' => false,
		],
		'class' => [
			'title' => 'Styl', 'type' => 'input', 'listing' => false, 'hint' => 'zatím se nepoužívá'
		],

	],
	'order' => 'published DESC'
];
