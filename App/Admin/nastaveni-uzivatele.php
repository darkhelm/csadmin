<?php
return [
	'canShow' => ['admin','superAdmin'],
	'canEdit' => ['admin','superAdmin'],
	'canDelete' => ['admin','superAdmin'],
	'canCreate' => ['admin','superAdmin'],
	'title' => 'Administrace uživatelů',
	'table' => 'cs_user',
	'joinedTable' => 'cs_user_web',
	'cs_web' => true,
	'where' => 'code != "superAdmin"',
	'whereList' => 'cs_user_web.id_cs_web = '.$_SESSION['webId'].' AND id_cs_user_role != 1',
	'actions' => ['edit'],
	'create' => true,
	'filter' => ['setColumns' => ['cs_user-lastname', 'cs_user-id_cs_user_role']],
	'paging' => 100,
	'columns' => [
		'id' => [
			'title' => '#', 'listing' => true, 'edit' => false, 'class' => 'table-th-id', 'readonly' => true
		],
		'lastname' => [
			'title' => 'Příjmení', 'type' => 'input', 'listing' => true
		],
		'firstname' => [
			'title' => 'Jméno', 'type' => 'input', 'listing' => true
		],
		'password' => [
			'title' => 'Heslo', 'type' => 'password', 'listing' => false,
			'hint' => 'Prázdná hodnota nezmění heslo.'
		],
		'id_cs_user_role' => [
			'title' => 'Skupina', 'listing' => true, 'type' => 'select', 'where' => 'code != "superAdmin"', 'default' => 'code|editor'
		],
		'email' => [
			'title' => 'E-mail', 'type' => 'input', 'listing' => true,
			'hint' => 'Slouží jako uživatelské jméno.'
		],
		'phone' => [
			'title' => 'Telefon', 'type' => 'input', 'listing' => true
		],
		'active' => [
			'title' => 'Aktivní', 'type' => 'switcher', 'listing' => true
		],
	],
	'order' => 'active DESC, lastname, firstname'
];
