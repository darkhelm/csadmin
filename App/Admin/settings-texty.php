<?php
return [
	'canShow' => ['superAdmin'],
	'canEdit' => ['superAdmin'],
	'canDelete' => ['superAdmin'],
	'canCreate' => ['superAdmin'],
	'title' => 'Administrace textů',
	'table' => 'cs_text',
	'actions' => ['edit','delete'],
	'create' => true,
	'columns' => [
		'id' => [
			'title' => '#id', 'listing' => true, 'edit' => false, 'class' => 'table-th-id'
		],
		'textcode' => [
			'title' => 'Kód', 'type' => 'input', 'listing' => true, 'hint' => 'Neměnit!!!'
		],
		'content' => [
			'title' => 'Název', 'type' => 'input', 'listing' => true
		],
		'description' => [
			'title' => 'Popis', 'type' => 'input', 'listing' => true
		],
	],
	'order' => 'textcode'
];
