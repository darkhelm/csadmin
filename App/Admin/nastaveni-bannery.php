<?php
return [
	'canShow' => ['editor','admin','superAdmin'],
	'canEdit' => ['editor','admin','superAdmin'],
	'canDelete' => ['editor','admin','superAdmin'],
	'canCreate' => ['editor','admin','superAdmin'],
	'editable' => 'all',
	'title' => 'Bannery',
	'table' => 'cs_web_banner',
	'actions' => ['edit','delete', 'create'],
	'filter' => ['setColumns' => ['cs_web_banner-id_cs_banner_type', 'cs_web_banner-id_cs_banner_slot']],
	'cs_web' => true,
	'create' => true,
//	'sortable' => true,
	'paging' => 100,
	'processForm' => 'Banner',
	'columns' => [
		'id' => [
			'title' => '#id', 'listing' => true, 'edit' => false, 'class' => 'table-th-id', 'readonly' => true
		],
		'name' => [
			'title' => 'Název', 'type' => 'input', 'listing' => true, 'required' => true,
		],
		'id_cs_banner_type' => [
			'title' => 'Typ banneru', 'type' => 'select', 'listing' => true, 'required' => true, 'class' => 'banner_type', 'rowClass' => 'show',
			'hint' => 'Nastavte před psaním obsahu. Změna může obsah textu vymazat.',
			'options' => [
				'create' => ['where' => 'active = 1'],
			]
		],
		'id_cs_banner_slot' => [
			'title' => 'Pozice banneru', 'type' => 'select', 'listing' => true, 'required' => true,
			'options' => [
				'create' => ['where' => 'active = 1'],
			]
		],
		'content' => [
			'title' => 'Obsah', 'type' => 'paragraph', 'listing' => false, 'class' => 'banner_content'
		],
		'image' => [
			'title' => 'Obrázek', 'type' => 'image', 'listing' => false,
		],
		'visibility' => [
			'title' => 'Viditelné na stránkách', 'type' => 'paragraphSimple', 'listing' => false
		],
		'list_annotation' => [
			'title' => 'Zobrazit anotaci', 'type' => 'select', 'listing' => false,
			'values' => [
				'none' => 'Žádná',
				'annotation' => 'Anotace článku',
				'text' => 'Obsah článku',
			]
		],
		'item_order' => [
			'title' => 'Pořadí článků', 'type' => 'select', 'listing' => false,
			'values' => [
				'user' => 'Manuálně (podle pořadí z přehledu článků)',
				'random' => 'Náhodně',
				'date' => 'Od nejnovějšího',
				'name' => 'Podle názvu',
			]
		],
		'id_cs_web_list' => [
			'title' => 'Seznam', 'type' => 'select', 'listing' => false, 'rowClass' => 'banner_list'
		],
		'banner_article' => [
			'title' => 'Články', 'type' => 'bannerArticle', 'listing' => false, 'rowClass' => 'banner_article'
		],
		'article_tags' => [
			'title' => 'Štítky', 'type' => 'articleTags', 'listing' => false, 'rowClass' => 'banner_article_tag',
			'help' => 'Vybrání štítku má přednost před individuálními články. Vybrání více štítků omezuje seznam článků, které musí mít nastavené právě vybrané štítky.'
		],

		'item_count' => [
			'title' => 'Počet položek', 'type' => 'input', 'listing' => false, 'hint' => 'celé číslo<br>0 pro max.',
		],
		'valid_from' => [
			'title' => 'Platnost od', 'type' => 'datetime', 'listing' => true, 'tab' => 'advanced'
		],
		'valid_to' => [
			'title' => 'Platnost do', 'type' => 'datetime', 'listing' => true, 'tab' => 'advanced'
		],
		'class' => [
			'title' => 'Vlastní třída', 'type' => 'input', 'listing' => true, 'tab' => 'advanced'
		],
		'active' => [
			'title' => 'Aktivní', 'type' => 'switcher', 'listing' => true,
		],
	],
	'order' => 'name'
];
