<?php
return [
	'canShow' => ['superAdmin'],
	'canEdit' => ['superAdmin'],
	'canDelete' => ['superAdmin'],
	'canCreate' => ['superAdmin'],
	'title' => 'Administrace textů',
	'table' => 'cs_longtexts',
	'actions' => ['edit','delete'],
	'create' => true,
	'columns' => [
		'id' => [
			'title' => '#id', 'listing' => true, 'edit' => false, 'class' => 'table-th-id'
		],
		'name' => [
			'title' => 'Název', 'type' => 'input', 'listing' => true
		],
		'code' => [
			'title' => 'Kód', 'type' => 'input', 'listing' => true, 'hint' => 'Neměnit!!!'
		],
		'text' => [
			'title' => 'Text', 'type' => 'paragraph', 'listing' => false
		],
		'class' => [
			'title' => 'Třída', 'type' => 'input', 'listing' => true
		],

	],
	'order' => 'name'
];
