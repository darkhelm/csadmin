<?php
return [
	'canShow' => ['admin', 'superAdmin'],
	'canEdit' => ['admin', 'superAdmin'],
	'canDelete' => ['admin', 'superAdmin'],
	'canCreate' => ['admin', 'superAdmin'],
	'title' => 'Textové popisky',
	'table' => 'cs_web_text',
	'actions' => ['edit','delete'],
	'create' => true,
	'cs_web' => true,
	'paging' => 100,
	'columns' => [
		'id' => [
			'title' => '#id', 'listing' => true, 'edit' => false, 'class' => 'table-th-id', 'readonly' => true
		],
		'textcode' => [
			'title' => 'Kód', 'type' => 'input', 'listing' => true, 'hint' => 'Neměnit!!!'
		],
		'content' => [
			'title' => 'Zobrazený text', 'type' => 'input', 'listing' => true
		],
		'description' => [
			'title' => 'Popis', 'type' => 'input', 'listing' => false, 'hint' => 'Nápověda'
		],

	],
	'order' => 'textcode'
];
