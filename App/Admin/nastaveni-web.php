<?php
return [
	'defaultAction' => 'edit',
	'canShow' => ['admin','superAdmin'],
	'canEdit' => ['admin','superAdmin'],
	'canDelete' => ['admin','superAdmin'],
	'canCreate' => ['superAdmin'],
	'title' => 'Web',
	'table' => 'cs_web',
//	'cs_web' => true,
	'actions' => ['edit'],
	'create' => true,
	'columns' => [
		'id' => [
			'title' => '#', 'listing' => true, 'edit' => false, 'class' => 'table-th-id', 'readonly' => true
		],
		'name' => [
			'title' => 'Název', 'type' => 'input', 'listing' => true
		],
		'code' => [
			'title' => 'Kód', 'type' => 'input', 'listing' => true, 'auth' => ['superAdmin'],
			'hint' => 'Bez diakritiky...'
		],
		'logo' => [
			'title' => 'Logo', 'type' => 'image', 'listing' => false
		],
		'favicon' => [
			'title' => 'Favikonka', 'type' => 'image', 'listing' => false, 'help' => 'Soubor ve formátu ICO - konvertor obrázků do .ico např. zde: <a href="https://convertico.com/" target=_blank>https://convertico.com/</a>'
		],
	],
	'order' => 'name'
];
