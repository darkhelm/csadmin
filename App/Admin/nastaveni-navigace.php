<?php
return [
	'canShow' => ['editor','admin','superAdmin'],
	'canEdit' => ['editor','admin','superAdmin'],
	'canDelete' => ['editor','admin','superAdmin'],
	'canCreate' => ['editor','admin','superAdmin'],
	'title' => 'Navigace',
	'table' => 'cs_web_navigation',
	'cs_web' => true,
	'actions' => ['edit','delete'],
	'create' => true,
	'processForm' => 'NavigationItem',
//	'filter' => ['setColumns' => ['cs_user-lastname', 'cs_user-id_cs_user_role']],
	'paging' => 100,
	'columns' => [
		'id' => [
			'title' => '#', 'listing' => true, 'edit' => false, 'class' => 'table-th-id', 'readonly' => true
		],
		'name' => [
			'title' => 'Název', 'type' => 'input', 'listing' => true
		],
		'code' => [
			'title' => 'Kód', 'type' => 'input', 'listing' => true,
			'hint' => 'Bez diakritiky...'
		],
		'position' => [
			'title' => 'Pozice', 'type' => 'select', 'listing' => true,
			'values' => [
				'' => 'Ruční vložení do banneru nebo článku',
				'main-menu' => 'Hlavní navigace v hlavičce',
			]
		],
		'class' => [
			'title' => 'Vlastní třída', 'type' => 'input', 'listing' => true, 'tab' => 'advanced'
		],
		'navigation_items' => [
			'title' => 'Položky navigace', 'type' => 'navigationItem', 'listing' => false,
		],
		'active' => [
			'title' => 'Aktivní', 'type' => 'switcher', 'listing' => true
		],
	],
	'order' => 'active DESC, name'
];
