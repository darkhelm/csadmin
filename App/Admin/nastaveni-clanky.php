<?php
return [
	'canShow' => ['editor','admin','superAdmin'],
	'canEdit' => ['admin','superAdmin'],
	'canDelete' => ['admin','superAdmin'],
	'canCreate' => ['editor','admin','superAdmin'],
	'title' => 'Články',
	'table' => 'cs_web_article',
	'cs_web' => true,
	'actions' => ['edit','delete'],
	'create' => true,
	'sortable' => true,
	'processForm' => 'Article',
	'filter' => ['setColumns' => ['cs_web_article-id_cs_web_article', 'cs_web_article-id_cs_user']],
	'paging' => 100,
	'tabs' => [
		'article' => 'Tato nastavení se použijí pro vygenerovaný seznam podčlánků tohoto článku.'
	],
	'columns' => [
		'id' => [
			'title' => '#id', 'listing' => true, 'edit' => false, 'class' => 'table-th-id', 'readonly' => true
		],
		'name' => [
			'title' => 'Název', 'type' => 'input', 'listing' => true
		],
		'code' => [
			'title' => 'Odkaz', 'type' => 'input', 'listing' => true, 'tab' => 'advanced',
			'hint' => 'Pokud necháte pole prázdné, odkaz bude automaticky vygenerován.',
			'help' => 'Odkaz je složen z domény a nadřazených článků: např. <strong>www.vasweb.cz/hlavni_clanek/tento_clanek</strong>. Tento celý odkaz musí být v rámci webu unikátní.'
		],
		'id_cs_web_article' => [
			'title' => 'Nadřazený článek', 'type' => 'articleSelect', 'listing' => true
		],
		'annotation' => [
			'title' => 'Anotace', 'type' => 'paragraph', 'listing' => false
		],
		'text' => [
			'title' => 'Obsah', 'type' => 'paragraph', 'listing' => false
		],
		'list_image' => [
			'title' => 'Obrázek', 'type' => 'image', 'listing' => false
		],
		'article_tags' => [
			'title' => 'Štítky', 'type' => 'articleTags', 'listing' => false
		],

		'id_cs_user' => [
			'title' => 'Autor', 'type' => 'select', 'listing' => true, 'joined' => 'lastname|firstname', 'order' => 'lastname, firstname', 'tab' => 'advanced',
			'where' => 'id IN (SELECT id_cs_user FROM cs_user_web WHERE id_cs_web = '.$_SESSION['webId'].')', 'default' => $_SESSION['userId']
		],
		'class' => [
			'title' => 'Třída CSS', 'type' => 'input', 'listing' => true, 'tab' => 'advanced'
		],
		'heading_image' => [
			'title' => 'Obrázek v hlavičce článku', 'type' => 'image', 'listing' => false, 'tab' => 'advanced'
		],
		'created_at' => [
			'title' => 'Publikace článku', 'type' => 'datetime', 'listing' => false, 'tab' => 'advanced'
		],
		'valid_from' => [
			'title' => 'Platnost od', 'type' => 'datetime', 'listing' => true, 'tab' => 'advanced'
		],
		'valid_to' => [
			'title' => 'Platnost do', 'type' => 'datetime', 'listing' => true, 'tab' => 'advanced'
		],


		'list_annotation' => [
			'title' => 'Zobrazit anotaci', 'type' => 'select', 'listing' => false, 'tab' => 'article',
			'values' => [
				'none' => 'Žádná',
				'annotation' => 'Anotace článku',
				'text' => 'Obsah článku',
			]
		],
		'item_order' => [
			'title' => 'Pořadí článků', 'type' => 'select', 'listing' => false, 'tab' => 'article',
			'values' => [
				'user' => 'Manuálně (podle pořadí z přehledu článků)',
				'random' => 'Náhodně',
				'date' => 'Od nejnovějšího',
				'name' => 'Podle názvu',
			]
		],
		'item_count' => [
			'title' => 'Počet zobrazených článků na stránku', 'type' => 'input', 'listing' => false, 'tab' => 'article', 'hint' => '0 nebo prázdné použijte pro výpis všech'
		],
		'show_date' => [
			'title' => 'Zobrazit datum článku', 'type' => 'switcher', 'listing' => false, 'tab' => 'article'
		],
		'show_image' => [
			'title' => 'Zobrazit obrázek článku', 'type' => 'switcher', 'listing' => false, 'tab' => 'article'
		],
		'show_tag' => [
			'title' => 'Zobrazit štítky článku', 'type' => 'switcher', 'listing' => false, 'tab' => 'article'
		],

		'in_menu' => [
			'title' => 'V menu', 'type' => 'switcher', 'listing' => true
		],
		'active' => [
			'title' => 'Aktivní', 'type' => 'switcher', 'listing' => true
		],
	],
	'order' => 'sort'
];
