<?php
return [
	'canShow' => ['editor','admin','superAdmin'],
	'canEdit' => ['editor','admin','superAdmin'],
	'canDelete' => ['editor','admin','superAdmin'],
	'canCreate' => ['editor','admin','superAdmin'],
	'title' => 'Seznamy',
	'table' => 'cs_web_list',
	'cs_web' => true,
	'actions' => ['edit','delete'],
	'create' => true,
	'processForm' => 'WebList',
//	'filter' => ['setColumns' => ['cs_user-lastname', 'cs_user-id_cs_user_role']],
	'paging' => 100,
	'columns' => [
		'id' => [
			'title' => '#', 'listing' => true, 'edit' => false, 'class' => 'table-th-id', 'readonly' => true
		],
		'name' => [
			'title' => 'Název', 'type' => 'input', 'listing' => true
		],
		'code' => [
			'title' => 'Kód', 'type' => 'input', 'listing' => true,
			'hint' => 'Bez diakritiky...'
		],
		'settings' => [
			'title' => 'Nastavení', 'type' => 'webListItems', 'listing' => false,
		],
		'active' => [
			'title' => 'Aktivní', 'type' => 'switcher', 'listing' => true
		],
		'class' => [
			'title' => 'Třída CSS', 'type' => 'input', 'listing' => false, 'tab' => 'advanced'
		],
	],
	'order' => 'active DESC, name'
];
