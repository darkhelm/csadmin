<?php
return [
	'canShow' => ['superAdmin'],
	'canEdit' => ['superAdmin'],
	'canDelete' => ['superAdmin'],
	'canCreate' => ['superAdmin'],
	'title' => 'Administrace webů',
	 'table' => 'cs_web',
	'actions' => ['edit','delete'],
	'create' => true,
	'columns' => [
		'id' => [
			'title' => '#id', 'listing' => true, 'edit' => false, 'class' => 'table-th-id'
		],
		'name' => [
			'title' => 'Název', 'type' => 'input', 'listing' => true
		],
		'code' => [
			'title' => 'Kód', 'type' => 'input', 'listing' => true
		],
		'domain' => [
			'title' => 'Doména', 'type' => 'input', 'listing' => true
		],
		'theme' => [
			'title' => 'Téma CSS', 'type' => 'input', 'listing' => true
		],
		'description' => [
			'title' => 'Popis', 'type' => 'paragraph', 'listing' => false
		],
		'settings' => [
			'title' => 'Nastavení', 'type' => 'paragraphSimple', 'listing' => false, 'hint' => 'JSON'
		],
		'active' => [
			'title' => 'Aktivní', 'type' => 'switcher', 'listing' => true
		],
	],
	'order' => 'name'
];
