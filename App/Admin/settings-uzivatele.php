<?php
return [
	'canShow' => ['superAdmin'],
	'canEdit' => ['superAdmin'],
	'canDelete' => ['superAdmin'],
	'canCreate' => ['superAdmin'],
	'title' => 'Administrace uživatelů',
	'table' => 'cs_user',
	'actions' => ['edit'],
	'create' => true,
	'filter' => ['setColumns' => ['cs_user-lastname']],
	'paging' => 100,
	'columns' => [
		'id' => [
			'title' => '#id', 'listing' => true, 'edit' => false, 'class' => 'table-th-id'
		],
		'lastname' => [
			'title' => 'Příjmení', 'type' => 'input', 'listing' => true
		],
		'firstname' => [
			'title' => 'Jméno', 'type' => 'input', 'listing' => true
		],
		'password' => [
			'title' => 'Heslo', 'type' => 'password', 'listing' => false,
			'hint' => 'Prázdná hodnota nezmění heslo.'
		],
		'id_cs_user_role' => [
			'title' => 'Skupina', 'listing' => true, 'type' => 'select'
		],
		'email' => [
			'title' => 'E-mail', 'type' => 'input', 'listing' => true,
			'hint' => 'Slouží jako uživatelské jméno.'
		],
		'phone' => [
			'title' => 'Telefon', 'type' => 'input', 'listing' => true
		],
		'active' => [
			'title' => 'Aktivní', 'type' => 'switcher', 'listing' => true
		],
		'user_web' => [
			'title' => 'Weby', 'listing' => false,
			"detail" => true, "edit" => true, "type" => "userWeb", 'typeTable' => 'cs_user_web'
		],
	],
	'order' => 'active DESC, lastname, firstname'
];
