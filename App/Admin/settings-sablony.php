<?php
return [
	'canShow' => ['superAdmin'],
	'canEdit' => ['superAdmin'],
	'canDelete' => ['superAdmin'],
	'canCreate' => ['superAdmin'],
	'title' => 'Administrace šablon',
	 'table' => 'cs_templates',
	'actions' => ['edit','delete'],
	'create' => true,
//	'filter' => ['setColumns' => ['user_role-name', 'user_role-code']],
	'columns' => [
		'id' => [
			'title' => '#id', 'listing' => true, 'edit' => false, 'class' => 'table-th-id'
		],
		'name' => [
			'title' => 'Název', 'type' => 'input', 'listing' => true
		],
		'code' => [
			'title' => 'Kód šablony', 'type' => 'input', 'listing' => true
		],
		'type' => [
			'title' => 'Typ', 'type' => 'input', 'listing' => true, 'hint' => 'mail, text'
		],
		'subject' => [
			'title' => 'Předmět pro email', 'type' => 'input', 'listing' => true
		],
		'description' => [
			'title' => 'Popis', 'type' => 'paragraphSimple', 'listing' => true
		],
		'template' => [
			'title' => 'Výchozí text', 'type' => 'paragraph', 'listing' => false
		],
	],
	'order' => 'name'
];
