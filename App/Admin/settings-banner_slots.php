<?php
return [
	'canShow' => ['superAdmin'],
	'canEdit' => ['superAdmin'],
	'canDelete' => ['superAdmin'],
	'canCreate' => ['superAdmin'],
	'title' => 'Bannery: sloty',
	'table' => 'cs_banner_slot',
	'actions' => ['edit','delete'],
	'create' => true,
	'paging' => 100,
	'columns' => [
		'id' => [
			'title' => '#id', 'listing' => true, 'edit' => false, 'class' => 'table-th-id'
		],
		'name' => [
			'title' => 'Název', 'type' => 'input', 'listing' => true
		],
		'code' => [
			'title' => 'Kód', 'type' => 'input', 'listing' => true, 'hint' => 'Neměnit!!!'
		],
		'active' => [
			'title' => 'Aktivní', 'type' => 'switcher', 'listing' => true
		],
	],
	'order' => 'active DESC, name'
];
