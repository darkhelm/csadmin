<?php
return [
	'canShow' => ['superAdmin'],
	'canEdit' => ['superAdmin'],
	'canDelete' => ['superAdmin'],
	'canCreate' => ['superAdmin'],
	'title' => 'Administrace rolí',
	 'table' => 'cs_user_role',
	'actions' => ['edit','delete'],
	'create' => true,
//	'filter' => ['setColumns' => ['user_role-name', 'user_role-code']],
	'columns' => [
		'id' => [
			'title' => '#id', 'listing' => true, 'edit' => false, 'class' => 'table-th-id'
		],
		'name' => [
			'title' => 'Role', 'type' => 'input', 'listing' => true
		],

		'code' => [
			'title' => 'Kód role', 'type' => 'input', 'listing' => true
		],
		'description' => [
			'title' => 'Popis', 'type' => 'paragraph', 'listing' => true
		],
	],
	'order' => 'name'
];
