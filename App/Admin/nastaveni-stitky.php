<?php
return [
	'canShow' => ['editor','admin','superAdmin'],
	'canEdit' => ['editor','admin','superAdmin'],
	'canDelete' => ['editor','admin','superAdmin'],
	'canCreate' => ['editor','admin','superAdmin'],
	'editable' => 'all',
	'title' => 'Štítky',
	'table' => 'cs_web_tag',
	'actions' => ['edit','delete', 'create'],
	'cs_web' => true,
	'create' => true,
	'paging' => 100,
	'columns' => [
		'id' => [
			'title' => '#id', 'listing' => true, 'edit' => false, 'class' => 'table-th-id', 'readonly' => true
		],
		'name' => [
			'title' => 'Název', 'type' => 'input', 'listing' => true, 'required' => true,
		],
		'code' => [
			'title' => 'Kód', 'type' => 'input', 'listing' => false, 'hint' => 'bez diakritiky, speciálních znaků a mezer'
		],
	],
	'order' => 'name'
];
