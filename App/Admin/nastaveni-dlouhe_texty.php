<?php
return [
	'canShow' => ['superAdmin'],
	'canEdit' => ['superAdmin'],
	'canDelete' => ['superAdmin'],
	'canCreate' => ['superAdmin'],
	'title' => 'Administrace textů',
	'table' => 'cs_web_longtext',
	'actions' => ['edit','delete'],
	'create' => true,
	'cs_web' => true,
	'columns' => [
		'id' => [
			'title' => '#id', 'listing' => true, 'edit' => false, 'class' => 'table-th-id'
		],
		'name' => [
			'title' => 'Název', 'type' => 'input', 'listing' => true
		],
		'code' => [
			'title' => 'Kód', 'type' => 'input', 'listing' => true, 'hint' => 'Neměnit!!!'
		],
		'text' => [
			'title' => 'Text', 'type' => 'paragraph', 'listing' => false
		],
		'class' => [
			'title' => 'Třída', 'type' => 'input', 'listing' => true
		],
		'id_cs_web' => [
			'title' => 'Web', 'type' => 'select', 'listing' => false, 'default' => $_SESSION['webId']
		],

	],
	'order' => 'name'
];
