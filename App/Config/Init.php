<?php
ob_start();

error_reporting(E_ALL);
ini_set('display_errors', 1);

ini_set('max_execution_time', 120);
//ini_set('display_errors','off');

if(!isset($_COOKIE['csAdmin--cookies'])) {
	ob_start();
	setcookie('csAdmin--cookies', 'newbie', time()+60,'/');
	ob_end_flush();
}

if (file_exists("App/Config/Config_Local.php")){
	require_once("Config_Local.php");
} else {
	require_once("Config.php");
}
require_once("AutoLoad.php");
$db = new \Classes\Db\Database();
$session = new \Classes\Modules\User\UserSession();
