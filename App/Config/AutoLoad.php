<?php

require_once __DIR__ . "/../../vendor/autoload.php";

use Tracy\Debugger;
Tracy\Debugger::getBar()->addPanel(new JanDrabek\Tracy\GitVersionPanel());

// Create an instance for Tracy Bar Panel
$panel = new \Dzegarra\TracyMysqli\BarPanel();
// Add the panel to Tracy
\Tracy\Debugger::getBar()->addPanel($panel);
\Tracy\Debugger::getBar()->addPanel(new \Classes\Components\ComponentLoaderDebugPanel(), 'components');
// Make a connection to the DB using the \Dzegarra\TracyMysqli\Mysqli class instead of mysqli
$conn = new \Dzegarra\TracyMysqli\Mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
// Execute your queries

Debugger::enable(Debugger::DETECT);
