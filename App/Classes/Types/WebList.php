<?php

namespace Classes\Types;

use Classes\Formatters\Strings;

class WebList extends BasicType
{

	public function __construct($post)
	{
		parent::__construct($post);
	}

	public function prepareData()
	{
		if (\WC::app()->get('action') == 'create') {
			if ($newId = $this->create()) {
				\WC::component()->alert()->prepare('Seznam byl vytvořen.');
				$_SESSION['highlighted'] = $newId;
				$this->prepareSpecialInsert($newId);
			} else {
				\WC::component()->alert()->prepare('Seznam nebyl vytvořen.','error');
			}
		} elseif (\WC::app()->get('action') == 'edit') {
			if ($this->update()) {
				\WC::component()->alert()->prepare('Seznam byl aktualizován.');
				$_SESSION['highlighted'] = $this->id;
				$this->prepareSpecialInsert($this->id);
			} else {
				\WC::component()->alert()->prepare('Seznam nebyl aktualizován.','error');
			}
		}
//		\WC::app()->redirect('/nastaveni/seznamy');
	}

	private function prepareSpecialInsert($parentId)
	{
		if (count($this->specialColumns) > 0) {
			foreach ($this->specialColumns as $m=>$column) {
				$method = 'insert'.ucfirst(Strings::underscoreToCamelCase($m));
				if (method_exists($this, $method)) {
					$this->$method($m, $parentId);
				}
			}
		}
	}

	private function insertSettings($key, $id)
	{
		if (isset($this->post[$key]) && count($this->post[$key]) > 0) {
			$items = [];
			foreach ($this->post[$key] as $set) {
				if ($set['name'] != '') {
					$set['code'] = $set['code'] != '' ? Strings::toLink($set['code']) : Strings::toLink($set['name']);
					$items[] = $set;
				}
			}
			$settings['settings'] = json_encode($items, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
			$this->update($settings);
		}
	}


	/*
	public function render()
	{
		$vars = [
			'items' => [],
		];
		return (new Renderer())->render(\Dir::template() . '/../Renderers/WebList/list.latte', $vars);
	}

	public function processForm($post)
	{
		unset($post['tabs']);

		$settings = [];
		foreach ($post['settings'] as $set) {
			if ($set['name'] != '') {
				$settings[] = $set;
			}
		}
		$post['id_cs_web'] = $_SESSION['webId'];
		$post['settings'] = json_encode($settings);
		unset($post['id']);

		$data = new Data('cs_web_list');
		if ($data->insert(['column' => [$post]])) {
			\WC::component()->alert()->prepare('Seznam '.$post['name'].' byl založen.');
		} else {
			\WC::component()->alert()->prepare('Seznam se nepodařilo založit.','error');
		}
		\WC::app()->redirect('/nastaveni/seznamy');
	}
	*/
}
