<?php

namespace Classes\Types;

use Classes\Db\Data;
use Classes\Formatters\Strings;
use Renderers\Renderer;

class Banner extends BasicType
{

	public function __construct($post)
	{
		parent::__construct($post);
	}

	public function prepareData()
	{
		if (\WC::app()->get('action') == 'create') {
			if ($newId = $this->create()) {
				\WC::component()->alert()->prepare('Banner byl vytvořen.');
				$_SESSION['highlighted'] = $newId;
				$this->prepareSpecialInsert($newId);
			} else {
				\WC::component()->alert()->prepare('Banner nebyl vytvořen.','error');
			}
		} elseif (\WC::app()->get('action') == 'edit') {
			if ($this->update()) {
				\WC::component()->alert()->prepare('Banner byl aktualizován.');
				$_SESSION['highlighted'] = $this->id;
				$this->prepareSpecialInsert($this->id);
			} else {
				\WC::component()->alert()->prepare('Banner nebyl aktualizován.','error');
			}
		}
		\WC::app()->redirect('/nastaveni/bannery');
	}

	private function prepareSpecialInsert($parentId)
	{
		if (count($this->specialColumns) > 0) {
			foreach ($this->specialColumns as $m=>$column) {
				$method = 'insert'.ucfirst(Strings::underscoreToCamelCase($m));
				if (method_exists($this, $method)) {
					$this->$method($m, $parentId);
				}
			}
		}
		$this->uploadImages($this->insert['imagesToUpload'], $parentId);
	}

	private function insertArticleTags($key, $id)
	{
		if (isset($this->post[$key]) && count($this->post[$key]) > 0) {
			$insert = $existing = [];
			$data = new Data('cs_web_banner_web_tag');
			if ($tags = $data->select(['columns' => '*', 'where' => 'id_cs_web_banner = '.intval($id)])) {
				foreach ($tags as $tag) {
					$existing[$tag['id']] = $tag['id_cs_web_tag'];
				}
			}
			if (count($existing) > 0) {
				$delete = array_diff($existing, $this->post[$key]);
				if (count($delete) > 0) {
					$deleteIds = implode(',',array_keys($delete));
					$data->deleteWhere('id IN ('.$deleteIds.')');
				}
			}

			foreach ($this->post[$key] as $value) {
				if (!in_array($value, $existing)) {
					$insert[] = [
						'id_cs_web_banner' => $id,
						'id_cs_web_tag' => $value,
					];
				}
			}

			if (count($insert) > 0) {
				$success = $error = 0;
				foreach ($insert as $item) {
					if ($data->insert(['column' => [$item]])) {
						$success++;
					} else {
						$error++;
					}
				}
				if ($error > 0) {
					\WC::component()->alert()->prepare('Počet nepřidaných štítků: '.$error,'error');
				}
			}
		}
	}

	private function insertBannerArticle($key, $id)
	{
		$items = [];
		foreach ($this->post['list_articles'] as $set) {
			if ($set['id'] != '') {
				$items[] = $set;
			}
		}
		$articles['list_articles'] = json_encode($items);
		$this->update($articles);
	}



}
