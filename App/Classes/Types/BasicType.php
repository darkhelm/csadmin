<?php

namespace Classes\Types;

use Classes\Actions\DefaultAction;
use Classes\Db\Data;

class BasicType extends DefaultAction
{

	protected $post;
	protected $insert;
	protected $specialColumns;
	protected $allowedTypes = ['input','select','switcher','date','datetime','paragraph','paragraphSimple','articleSelect','image','password'];

	public function __construct($post = null)
	{
		parent::__construct();
		$this->post = $post ?: [];
		$this->preparePost();
	}

	protected function preparePost()
	{
		foreach ($this->columns as $key=>$column) {
			if (isset($column['type']) && !in_array($column['type'], $this->allowedTypes)) {
				$this->specialColumns[$key] = $column;
				unset($this->columns[$key]);
			}
		}
		$this->insert = $this->prepareSave($this->columns);
	}

	protected function create()
	{
		$data = new Data($this->table);
		return $data->insert(['column' => [$this->insert['insert']]]);
	}

	protected function update($update = false)
	{
		$data = new Data($this->table);
		return $data->update(($update ? $update : $this->insert['insert']), ['where' => 'id = '.intval($this->id).' AND id_cs_web = '.intval($_SESSION['webId'])]);
	}
}
