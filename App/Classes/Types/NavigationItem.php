<?php

namespace Classes\Types;

use Classes\Db\Data;
use Classes\Formatters\Strings;
use Renderers\Renderer;

class NavigationItem
{

	public function render()
	{
		$items = [];
		if ($id = \WC::app()->get('id')) {
			if ($navigation = \WC::component()->navigation()->getNavigationById($id)) {
				$items = $navigation->getItems();
			}
		}

		$vars = [
			'items' => $items,
			'articles' => \WC::component()->article()->getArticlesForMenu(),
		];
		return (new Renderer())->render(\Dir::template() . '/../Renderers/Navigation/items.latte', $vars);
	}

	public function processForm($post)
	{
		unset($post['tabs']);

		$items = [];
		foreach ($post['navigationItem'] as $set) {
			if ($set['id_web_article'] != '' || ($set['name'] != '' && $set['link'] != '')) {
				$items[] = $set;
			}
		}
		$post['code'] = $post['code'] == '' ? Strings::toLink($post['name']) : Strings::toLink($post['code']);
		$post['active'] = isset($post['active']) && $post['active'] == 'on';
		$post['id_cs_web'] = $_SESSION['webId'];
		$post['navigation_items'] = json_encode($items);
		unset($post['navigationItem']);

		if (\WC::app()->get('action') == 'create') {
			unset($post['id']);
			$this->create($post);
		} elseif (\WC::app()->get('action') == 'edit') {
			$this->update($post);
		}
		\WC::component()->alert()->prepare('Chyba při vytváření navigace.','error');
		\WC::app()->redirect('/nastaveni/navigace');
	}

	private function update($post)
	{
		$data = new Data('cs_web_navigation');
		$id = $post['id'];
		unset($post['id']);

		if ($data->update($post, ['where' => 'id = '.intval($id).' AND id_cs_web = '.intval($_SESSION['webId'])])) {
			\WC::component()->alert()->prepare('Navigace '.$post['name'].' byla aktualizována.');
		} else {
			\WC::component()->alert()->prepare('Navigaci se nepodařilo aktualizovat.','error');
		}
		\WC::app()->redirect('/nastaveni/navigace');
	}

	private function create($post)
	{
		$data = new Data('cs_web_navigation');
		if ($data->insert(['column' => [$post]])) {
			\WC::component()->alert()->prepare('Navigace '.$post['name'].' byla založena.');
		} else {
			\WC::component()->alert()->prepare('Navigaci se nepodařilo založit.','error');
		}
		\WC::app()->redirect('/nastaveni/navigace');
	}
}
