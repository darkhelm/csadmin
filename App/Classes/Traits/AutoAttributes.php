<?php

namespace Classes\Traits;

use Classes\Formatters\Strings;

trait AutoAttributes
{

	protected $id;
	protected $_attributes;

	public function __construct(array $attributes)
	{
		$this->_attributes = $attributes;
		foreach ($this->_attributes as $key => $val) {
			$objectKey = Strings::underscoreToCamelCase($key);
			if (property_exists($this, $objectKey)) {
				$this->$objectKey = $this->getAttr($key);
			}
		}
	}

	public function getId()
	{
		return $this->id;
	}

	public function getAttr($key)
	{
		if (isset($this->_attributes[$key])) {
			return $this->_attributes[$key];
		}
		return null;
	}

}
