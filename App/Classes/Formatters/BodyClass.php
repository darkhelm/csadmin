<?php

namespace Classes\Formatters;

class BodyClass {

	public static function render($url)
	{
		$class = '';
		if (isset($url[1]) && $url[1] == '') {
			$class .= 'page-index';
		} else {
			foreach ($url as $item) {
				if ($item != '') {
					$class .= 'page-' . $item . ' ';
				}
			}
		}

		return $class;
	}
}
