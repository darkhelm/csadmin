<?php

namespace Classes\Formatters;

class Date {

	public static function shortDate($date, $format = null)
	{
		$timestamp = \DateTime::createFromFormat('Y-m-d G:i:s', $date);
		return (isset($format) ? $timestamp->format($format) : $timestamp->format('j. n.'));
	}

	public static function fullDate($date)
	{
		$timestamp = \DateTime::createFromFormat('Y-m-d G:i:s', $date);
		return $timestamp->format('j. n. Y (G:i:s)');
	}

	public static function csDate($date)
	{
		if ($date == '0000-00-00' || $date == null) {
			return '';
		}
		$timestamp = \DateTime::createFromFormat('Y-m-d', $date);
		return $timestamp->format('j. n. Y');
	}

	public static function getConvertedFullDate($date, $fromZone = null, $toZone = null, $format = 'j. n. Y (G:i)')
	{
		$from = ($fromZone == null) ? 'Zulu' : $fromZone;
		$to = ($toZone == null) ? 'Europe/Prague' : $toZone;

		date_default_timezone_set($from);
		$timestamp = new \DateTime($date);
		$euro = new \DateTimeZone($to);
		$timestamp->setTimezone($euro);

		return $timestamp->format($format);
	}

	public static function getConvertedShortDate($date, $fromZone = null, $toZone = null)
	{
		$from = ($fromZone == null) ? 'Zulu' : $fromZone;
		$to = ($toZone == null) ? 'Europe/Prague' : $toZone;

		date_default_timezone_set($from);
		$timestamp = new \DateTime($date);
		$euro = new \DateTimeZone($to);
		$timestamp->setTimezone($euro);

		return $timestamp->format('j. n.');
	}

	public static function getMonthNameFromNumber($month)
	{
		$array = ['', 'leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'];
		return $array[$month];
	}

	public static function getDayNameFromNumber($day)
	{
		$array = ['', 'neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'];
		return $array[$day];
	}
}
