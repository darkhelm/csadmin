<?php

namespace Classes\Formatters;

class Strings
{

	public static function cleanWord($text)
	{
		$array = ['<br>'];
		foreach ($array as $item) {
			$text = str_replace($item, '', $text);
		}
		return $text;
	}

	public static function getUserIP()
	{
		// Get real visitor IP behind CloudFlare network
		if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
			$_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
			$_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
		}
		$client  = @$_SERVER['HTTP_CLIENT_IP'];
		$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
		$remote  = $_SERVER['REMOTE_ADDR'];

		if(filter_var($client, FILTER_VALIDATE_IP))
		{
			$ip = $client;
		}
		elseif(filter_var($forward, FILTER_VALIDATE_IP))
		{
			$ip = $forward;
		}
		else
		{
			$ip = $remote;
		}

		return $ip;
	}

	public static function toLink ($string)
	{
		$replace = [
			"á" => "a",
			"ä" => "a",
			"č" => "c",
			"ď" => "d",
			"é" => "e",
			"ş" => "s",
			"ŝ" => "s",
			"ś" => "s",
			"Ś" => "s",
			"Ŝ" => "s",
			"ŝ" => "s",
			"Ş" => "s",
			"ŕ" => "r",
			"Ŕ" => "r",
			"ł" => "l",
			"Ł" => "l",
			"ę" => "e",
			"Ę" => "e",
			"ć" => "c",
			"Ć" => "c",
			"Ĉ" => "c",
			"ĉ" => "c",
			"è" => "e",
			"È" => "e",
			"Ç" => "c",
			"ç" => "c",
			"Æ" => "ae",
			"æ" => "ae",
			"ß" => "ss",
			"ě" => "e",
			"í" => "i",
			"ĺ" => "l",
			"ľ" => "l",
			"ň" => "n",
			"ń" => "n",
			"ó" => "o",
			"ô" => "o",
			"ő" => "o",
			"ö" => "o",
			"ŕ" => "r",
			"š" => "s",
			"ś" => "s",
			"ť" => "t",
			"ú" => "u",
			"ů" => "u",
			"ű" => "u",
			"ü" => "u",
			"ý" => "y",
			"ř" => "r",
			"ž" => "z",
			"Á" => "a",
			"Ä" => "a",
			"Č" => "c",
			"Ď" => "d",
			"É" => "e",
			"Ě" => "e",
			"Í" => "i",
			"Ĺ" => "l",
			"Ľ" => "l",
			"Ň" => "n",
			"Ó" => "o",
			"Ô" => "o",
			"Ő" => "o",
			"Ö" => "o",
			"Ŕ" => "r",
			"Š" => "s",
			"Ś" => "s",
			"Ť" => "t",
			"Ú" => "u",
			"Ů" => "u",
			"Ű" => "u",
			"Ü" => "u",
			"Ý" => "y",
			"Ř" => "r",
			"Ž" => "z",
			"_" => "-",
			"+" => "",
			"=" => "-",
			":" => "-",
			'"' => "",
			"'" => "",
			"?" => "",
			"!" => "",
			"." => "-",
			"," => "",
			"/" => "",
			"\\" => "",
			" " => "-",
			"*" => "",
			"„" => "",
			"“" => ""
		];
		$search = array_keys($replace);
		$repl = array_values($replace);
		$string = strip_tags($string);
		$r = strtolower(str_replace($search, $repl, $string));
		return $r;
	}

	public static function slugify($text,$strict = false) {
		$text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
		// replace non letter or digits by -
		$text = preg_replace('~[^\\pL\d.]+~u', '-', $text);

		// trim
		$text = trim($text, '-');
		setlocale(LC_CTYPE, 'en_GB.utf8');
		// transliterate
		if (function_exists('iconv')) {
			$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		}

		// lowercase
		$text = strtolower($text);
		// remove unwanted characters
		$text = preg_replace('~[^-\w.]+~', '', $text);
		if (empty($text)) {
			return 'empty_$';
		}
		if ($strict) {
			$text = str_replace(".", "_", $text);
		}
		return $text;
	}

	public static function generateFileName ($key, $id, $file) {
		$clean_code = preg_replace('/[^a-zA-Z0-9_-]/', '', Files::get_name($file));
		if ($clean_code != '') {
			$time = time();
			$hash = substr(hash_hmac('md5', $time, $key),0,8);
			$filename = $id.'-'.$clean_code.'-'.$hash.Files::get_extension($file);
			return $filename;
		}
		return false;
	}

	public static function stringToNumber ($string) {
		$num = str_replace (
			array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "_"),
			array("13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39"),
			$string
		);
		return $num;
	}

	public static function stripText ($string, $length = 25)
	{
		$total = strlen($string);
		if ($total <= $length) {
			return $string;
		}
		$r = '';
		$rl = 0;
		$chunk = explode(' ', $string);
		for ($i = 0; $i < count($chunk); $i++) {
			$rl += strlen($chunk[$i]) + ($i>0?1:0);
			if ($rl <= $length) {
				$r .= ($i>0?' ':'').$chunk[$i];
			} else {
				$rl -= 1;
				break;
			}
		}
		return $r.($rl<$total?'..':'');
	}

	public static function p ($string = "")
	{
		if ($string == "" || trim(strip_tags($string)) == '') {
			return '';
		}
		if (!preg_match("/^\<[p|h|div|ul|ol]/", trim($string))) {
			return "<p>$string</p>";
		}
		return $string;
	}

	public static function dashesToCamelCase($string, $capitalizeFirstCharacter = false)
	{
		$str = str_replace('-', '', ucwords($string, '-'));
		if (!$capitalizeFirstCharacter) {
			$str = lcfirst($str);
		}
		return $str;
	}

	public static function underscoreToCamelCase($string, $capitalizeFirstCharacter = false)
	{
		$str = str_replace('_', '', ucwords($string, '_'));
		if (!$capitalizeFirstCharacter) {
			$str = lcfirst($str);
		}
		return $str;
	}

	public static function dashFullName($string)
	{
		$str = preg_replace("/[^a-zA-Z0-9-]+/", "",strtolower(str_replace(' ','-',$string)));
		return $str;
	}

}
