<?php

namespace Classes\Formatters;

class Numbers
{

	public static function validateDecimal($number)
	{
		return str_replace(',', '.', $number);
	}

}
