<?php

namespace Classes\Formatters;

class Files
{

	/**
	 * Gets file extension
	 *
	 * @param string           Filename
	 *
	 * @access    public
	 * @return    string           File extension
	 */
	public static function get_extension($file, $full = true)
	{
		$r = strtolower(strrchr($file, "."));
		if (!$full)
			$r = preg_replace("/^\./", "", $r);
		return $r;
	}

	public static function get_name($file)
	{
		return pathinfo($file, PATHINFO_FILENAME);
	}

}
