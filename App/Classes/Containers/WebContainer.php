<?php

namespace Classes\Containers;

use Classes\Components\ComponentLoader;
use Classes\Helpers\App;

class WebContainer
{

	private static $application;

	private static $theme;

	private static $componentLoader;

	private static $content;

	public static function init()
	{
		self::$componentLoader = new ComponentLoader();
	}

	/**
	 * @return ComponentLoader
	 */
	public static function component()
	{
		return self::$componentLoader;
	}

	/**
	 * @return App
	 */
	public static function app()
	{
		return self::component()->app();
	}

}
