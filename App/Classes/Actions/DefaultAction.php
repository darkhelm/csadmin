<?php

namespace Classes\Actions;

use Classes\Db\Data;
use Classes\Formatters\Strings;
use Classes\Helpers\ActionProcessor;
use Classes\Helpers\AdminTemplate;
use Classes\Modules\User\User;
use Symfony\Component\Filesystem\Filesystem;

class DefaultAction {

	protected $application;
	protected $url;
	protected $rowData = null;
	protected $template;
	protected $columns;
	protected $table;
	protected $groupUser;
	protected $id;
	/**
	 * @var User
	 */
	protected $user;
	protected $idColumn = "id";
	protected $valueList = [];
	protected $action;

	protected $canShow = false;
	protected $canEdit = false;
	protected $canDelete = false;
	protected $canCreate = false;

	private $subactions = ['userAuth', 'files', 'userWeb', 'articleSelect', 'articleTags', 'bannerArticle', 'webListItems'];
	protected $tabs = [
		'basic' => 'Základní nastavení',
		'advanced' => 'Pokročilá nastavení',
		'article' => 'Seznam článků',
	];


	public function __construct()
	{
		$this->application = \WC::app();
		$this->user = \WC::component()->users()->getUser();
		$this->url = $this->application->getFullUrl();
		$this->action = \WC::app()->get('action');
		if ($id = \WC::app()->get('id')){
			$this->id = $id;
		}
		if ($this->action){
			$templateName = str_replace('/','-', \WC::app()->get('baseLink'));
			$this->template = AdminTemplate::getTemplate($templateName);
			if ($this->template){
				$this->columns = $this->template['columns'];
				$this->table = $this->template['table'];

				$this->canShow = \WC::component()->users()->isAuthorized($this->template['canShow']);
				$this->canEdit = \WC::component()->users()->isAuthorized($this->template['canEdit']);
				$this->canDelete = \WC::component()->users()->isAuthorized($this->template['canDelete']);
				$this->canCreate = \WC::component()->users()->isAuthorized($this->template['canCreate']);

				$data = new Data($this->table);
				$this->rowData = $this->id ? $data->selectSingle(['columns' => '*', 'where' => 'id = '.$this->id]) : null;
			}
		}
	}

	protected function processForm($post)
	{
		$className = ucfirst($this->template['processForm']);
		if (class_exists('\\Classes\\Types\\'.$className)) {
			if (in_array($className,['NavigationItem'])) {
				$className = '\\Classes\\Types\\'.$className;
				return (new $className())->processForm($post);
			}
			$className = '\\Classes\\Types\\'.$className;
			return (new $className($post, $this->template))->prepareData();
		} else {
			\WC::component()->alert()->prepare('Došlo k chybě při zpracování. Kód: '.$this->template['table'], 'error');
			\WC::app()->reload();
		}
	}

	protected function format($column, $key, $value = null, $linkTable = null)
	{
		if ($key == 'id') {
			$value = $this->id;
		}
		$r = '';
		$type = 'input';
		if (isset($column['type'])) {
			$type = $column['type'];
		}
		$class = isset($column['class']) ? $column['class'] : '';
		$readonly = isset($column['readonly']) && $column['readonly'] ? ' readonly ' : '';
		$disabled = isset($column['disabled']) && $column['disabled'] ? ' disabled ' : '';
		$required = isset($column['required']) && $column['required'] ? ' required ' : '';
		if ($required != '') {
			$class .= ' required';
		}

		$name = $linkTable ? $linkTable.'|'.$key.'['.$value.']' : $key;

		$className = ucfirst($type);
		if (class_exists('\\Classes\\Types\\'.$className)) {
			$className = '\\Classes\\Types\\'.$className;
			$r = (new $className())->render();
		} elseif (in_array($type, $this->subactions)) {
			if ($process = ActionProcessor::action($className)) {
				$method = new $process();
				$r = $method->process($column, $key, $value, $linkTable, $this->table);
			}
		} else {
			preg_match("/^id_(.*)/", $key, $result);
			if (is_array($result) && count($result) == 2) {
				$table = $result[1];
				if ($o = $this->getOptions($column, $table, $value)) {
					$r .= '
						<select name=' . $name . ' class="form-control form-control-sm ' . $class . '" '.$disabled.' '.$readonly.'>
							' . $o . '
						</select>
					';
				}
			} else {
				if ($type == 'mapValues') {
					if (isset($column["typeTable"]) && $column["typeTable"] != "" && isset($column["columns"]) && is_array($column["columns"])) {
						// Map values definition
						if (isset($column["typeColumnText"]) && $column["typeColumnText"] != "")
							$dataSelect = $column["typeColumnText"] . " AS label";
						else
							$dataSelect = $this->idColumn . " AS label";

						// Filled data columns definition
						$filledDataSelect = $column["columns"];
						if (!isset($column["columns"][$this->idColumn]))
							$filledDataSelect[$this->idColumn] = "ID";
						$sourceTableLink = "id_" . $this->table;
						$typeTableLink = "id_" . $column["typeTable"];
						$filledDataSelect[$sourceTableLink] = "SourceTable";
						$filledDataSelect[$typeTableLink] = "TypeTable";
						$typeWhere = null;
						if (isset($column["typeWhere"]))
							$typeWhere = $column["typeWhere"];

						if ($select = $this->getValues($column["typeTable"], $dataSelect, $typeWhere)) {
							$selected = array();
							$filledSelect = $this->getValues($key, array_keys($filledDataSelect), $sourceTableLink . " = '" . intval($this->id) . "'");
							$r .= '<div class="mappedValues detail">';
							foreach ($select as $type) {
								$fill = null;
								$class = isset($column['tableClass']) ? $column['tableClass'] : '';
								$r .= '<div class="' . $class . '">';
								$r .= '<h3>' . $type["label"] . '</h3>';
								$r .= '<table class="table">';
								if ($filledSelect) {
									foreach ($filledSelect as $temp)
										if ($temp[$typeTableLink] == $type["id"])
											$fill = $temp;
								}
								foreach ($column["columns"] as $k => $col) {
									$ident = $key . '-' . $type["id"] . '-' . $k;
									$identId = $key . '-' . $type["id"] . '-id';
									$r .= '
										<tr>
											<th class="table-th-name small" style="width: 100px;"><label for="' . $ident . '">' . $col . '</label></th>
											<td><input type="text" maxlength="100" value="' . (is_array($fill) ? $fill[$k] : '') . '" name="' . $ident . '" id="' . $ident . '" class="form-control form-control-sm" /></td>
											<input type="hidden" hidden name="' . $identId . '" value="' . $fill['id'] . '">
										</tr>
									';
								}
								$r .= '</table>';
								$r .= '</div>';
							}
							$r .= '</div>';
						}
					}
				} elseif ($type == 'linkTable') {
//				$table = false;
//				preg_match("/^web_(.*)/", $key, $result);
//				if (is_array($result) && count($result) == 2) {
//					$table = $result[1];
//				}
//				if ($table && $column['columns']){
//					$r .= '
//						<div class="linkTable detail">
//							<table class="table">
//								<thead>
//									<tr>
//									';
//									foreach ($column['columns'] as $head){
//										$r .= '
//											<td>'.$head['title'].'</td>
//										';
//									}
//								$r .= '
//									</tr>
//								</thead>
//								<tbody>
//									<tr>
//					';
//									foreach ($column['columns'] as $k=>$column){
//										$r .= '
//											<td>' . $this->format($column, $k, '', $table) . '</td>
//										';
//									}
//					$r .= '
//									</tr>
//								</tbody>
//							</table>
//						</div>
//					';
//				}
				} elseif ($type == 'colorpicker') {
					$r = '<input type="text" name="' . $name . '" value="' . $value . '" id="' . $name . '" class="form-control form-control-sm jscolor" ' . $readonly . ' ' . $required . '>';
				} elseif ($type == 'image') {
					if ($value) {
						$r = '
						    
							<a href="" class="ui-delete-image" data-table="'.$this->table.'" data-id="'.$this->id.'" data-image="'.$key.'" data-name="'.basename($value).'" data-link="'.$value.'">
								<i class="fa fa-fw fad fa-times-circle text-danger"></i>
							</a>
							<a href="' . $value . '" data-fancybox>Soubor: '.basename($value).'<img src="' . $value . '" class="image-preview"></a>
						';
					}
					$r .= '<input type="file" name="' . $name . '" id="' . $name . '" class="form-control form-control-sm" ' . $readonly . ' ' . $required . '>';
				} elseif ($type == 'paragraph') {
					$r .= '<textarea name="' . $name . '" id="' . $name . '" class="ckeditor form-control ' . $class . '" ' . $required . '>' . $value . '</textarea>';
				} elseif ($type == 'paragraphSimple') {
					$r = '<textarea name="' . $name . '" id="' . $name . '" class="form-control form-control-sm ' . $class . '" ' . $required . '>' . $value . '</textarea>';
				} elseif ($type == 'checkbox' || $type == 'switcher') {
					$checked = '';
					if ($value != 0 || $value == 1 || $value == 'on') {
						$checked = 'checked = "checked"';
						$value = 1;
					}
//				$r = '<input type="checkbox"  name="' . $name . '" '.$checked.' id="' . $name . '">';
					$r = '
					<label class="customcheck">
						<input type="checkbox"  name="' . $name . '" ' . $checked . ' id="' . $name . '" ' . $required . '>
						<span class="checkmark"></span>
					</label>
				';

				} elseif ($type == 'password') {
					$r = '<input type="password" name="' . $name . '" value="" id="' . $name . '" class="form-control form-control-sm" ' . $required . '>';
				} elseif ($type == 'datetime') {
					$value = $value == null || $value == '0000-00-00 00:00:00' ? '' : $value;
					$r = '
					<div class="input-append date">
					    <input type="text"  name="' . $name . '" value="' . $value . '" id="' . $name . '" readonly class="form-control form-control-sm form_datetime" ' . $required . '>
					    <span class="add-on"><i class="icon-th"></i></span>
					</div>
				';
				} elseif ($type == 'date') {
					$value = $value == null || $value == '0000-00-00' ? '' : $value;
					$r = '
					<div class="input-append date">
					    <input type="text"  name="' . $name . '" value="' . $value . '" id="' . $name . '" readonly class="form-control form-control-sm form_date" ' . $required . '>
					    <span class="add-on"><i class="icon-th"></i></span>
					</div>
				';
				} elseif ($type == 'select' && $column["values"]) {
					$o = '';
					foreach ($column["values"] as $k => $v) {
						$selected = $k == $value ? 'selected = "selected"' : '';
						$o .= '<option value="' . $k . '" ' . $selected . '>' . $v . '</option>';
					}

					$r = '
					<select name=' . $name . ' class="form-control form-control-sm" ' . $required . '>
						' . $o . '
					</select>
				';
				} elseif ($type == 'price') {
					$r = '<input type="text" name="' . $name . '" value="' . $value . '" id="' . $name . '" class="form-control form-control-sm mask-price">';
				} elseif ($type == 'datetime') {
					$r = '
					<div class="input-append date form_datetime">
					    <input type="text"  name="' . $name . '" value="' . $value . '" id="' . $name . '" readonly class="form-control form-control-sm">
					    <span class="add-on"><i class="icon-th"></i></span>
					</div>
				';
				} elseif ($type == 'jobNumber') {
					$value = str_replace('.', '-', $value);
					$r = '<input type="text" name="' . $name . '" value="' . $value . '" id="' . $name . '" class="form-control form-control-sm">';
				} else {
					$r = '<input type="text" name="' . $name . '" value="' . $value . '" id="' . $name . '" class="form-control form-control-sm" ' . $readonly . ' ' . $required . '>';
				}
			}
		}

		return $r;
	}

	protected function getValues ($table, $columns, $where = null, $customIdent = null, $orderColumn = null) {
		$ident = $table.md5($where).$customIdent;
//		if (key_exists($ident, $this->valueList))
//			return $this->valueList[$ident];
		if (!is_array($columns))
			$columns = array ($columns);
		$columns[] = $this->idColumn." AS id";
		$data = new Data($table);
		$this->valueList[$ident] = $data->select ([
			"columns" => $columns,
			"where" => $where,
			"order" => $orderColumn
		]);
		return $this->valueList[$ident];
	}

	private function getOptions($column, $table, $value = null)
	{
		$option = false;
		$whereArray = [];
		$order = isset($column['order']) ? $column['order'] : '';
		$default = isset($column['default']) ? $column['default'] : '';

		if (isset($column['where'])) {
			$whereArray[] = $column['where'];
		}
		if (isset($column['options'][$this->action]['where'])) {
			$whereArray[] = $column['options'][$this->action]['where'];
		}
		$where = implode(' AND ', $whereArray);

		$data = new Data($table);
		if ($select = $data->select([
			'columns' => '*',
			'order' => $order,
			'where' => $where
		])){
			$dataAttribute = '';
			if (!\WC::component()->users()->isAuthorized('admin') && $table == 'user'){
				$option .= '<option value="' . $this->user->getId() . '">' . $this->user->getAttr('lastname') . ', ' .$this->user->getAttr('firstname'). '</option>';
			} else {
				$option .= '<option value="">--- vybrat ---</option>';
				foreach ($select as $sel) {
					if (isset($column['data'])) {
						$dataAttribute = $sel[$column['data']];
					}
					$selected = '';
					if (isset($column['default'])) {
						if (is_numeric($column['default']) && $sel['id'] == $column['default']) {
							$selected = 'selected';
						} elseif (!is_numeric($column['default'])) {
							$parts = explode('|', $column['default']);
							if ($sel[$parts[0]] == $parts[1]) {
								$selected = 'selected';
							}
						}
					} else {
						if ($value && $sel['id'] == $value) {
							$selected = 'selected';
						} elseif (!$value && $table == 'user') {
							if ($this->user->getId() == $sel['id']){
								$selected = 'selected';
							}
						}
					}
					if (isset($column['joined'])) {
						$joined = explode('|', $column['joined']);
						$name = [];
						foreach ($joined as $item) {
							$name[] = $sel[$item];
						}
						$name = implode(', ', $name);
					} else {
						$name = $sel['name'];
					}
					$option .= '<option value="' . $sel['id'] . '" ' . $selected . ' class="'.$dataAttribute.'">' . $name . '</option>';
				}
			}
		}
		return $option;
	}

	protected function prepareSave($columns = null)
	{
		$r = $filesToUpload = $insertUserWeb = $imagesToUpload = [];
		if (isset($columns['tabs'])) {
			unset($columns['tabs']);
		}
		$columns = $columns ? $columns : $this->template['columns'];
		foreach ($columns as $key=>$column) {
			if (isset($column['type']) && $column['type'] == 'userWeb') {
				if (isset($columns['cs_user_web'])) {
					foreach ($columns['cs_user_web'] as $auth => $val) {
						if ($val) {
							$insertUserWeb[$auth] = $val;
						}
					}
				}
				unset($columns['cs_user_web']);
			}

			if (isset($column['type']) && $column['type'] == 'files') {
				$filesToUpload = [
					'files' => $_FILES,
					'code' => isset($column['fileCode']) ? $column['fileCode'] : '',
				];
				unset($columns[$key]);
			}
			if (isset($column['type']) && $column['type'] == 'image') {
				$imagesToUpload = [
					'files' => $_FILES,
				];
				unset($columns[$key]);
			}
		}

		$r['insert'] = $this->prepareColumnsToInsert($columns);
		$r['insertUserWeb'] = $insertUserWeb;
		$r['filesToUpload'] = $filesToUpload;
		$r['imagesToUpload'] = $imagesToUpload;

		return $r;
	}

	protected function prepareColumnsToInsert($columns)
	{
		$insert = [];
		foreach ($columns as $key=>$column) {
			if (isset($this->template['columns'][$key]['type']) && $this->template['columns'][$key]['type'] == 'datetime'){
				$insert[$key] = '';
				if ($_POST[$key] != '' && $_POST[$key] != '0000-00-00 00:00:00'){
					$insert[$key] = date('Y-m-d H:i:s');
				}
			} elseif (isset($this->template['columns'][$key]['type']) && $this->template['columns'][$key]['type'] == 'date'){
				if ($_POST[$key] != '' || $_POST[$key] != '0000-00-00'){
					$insert[$key] = $_POST[$key];
				}
			} elseif (isset($this->template['columns'][$key]['type']) && $this->template['columns'][$key]['type'] == 'checkbox' || isset($this->template['columns'][$key]['type']) && $this->template['columns'][$key]['type'] == 'switcher'){
				$insert[$key] = 0;
				if (isset($_POST[$key]) && $_POST[$key] == 'on'){
					$insert[$key] = 1;
				}
			} elseif (isset($this->template['columns'][$key]['type']) && $this->template['columns'][$key]['type'] == 'password') {
				if ($_POST[$key] != '') {
					$insert['salt'] = 'clpa2019';
					$insert[$key] = password_hash($insert['salt'] . $_POST[$key], PASSWORD_DEFAULT);
				}
			} elseif (isset($this->template['columns'][$key]['type']) && $this->template['columns'][$key]['type'] == 'price') {
				$insert[$key] = str_replace(' ', '', $_POST[$key]);
			} elseif (isset($this->template['columns'][$key]['type']) && $this->template['columns'][$key]['type'] == 'sort') {
				$insert[$key] = '';
			} else {
				if ($key != 'id') {
					if ($key == 'target' || $key == 'active') {
						$insert[$key] = 0;
						if (isset($_POST[$key]) && $_POST[$key] == 'on'){
							$insert[$key] = 1;
						}
					} elseif ($key == 'code' || $key == 'link') {
						$insert[$key] = $_POST[$key] == '' && isset($_POST['name']) && $_POST['name'] != '' ? Strings::toLink($_POST['name']) : Strings::toLink($_POST[$key]);
					} else {
						$insert[$key] = $_POST[$key];
					}
				} else {
					if ($this->template['columns'][$key]['edit']) {
						$insert[$key] = $_POST[$key];
					}
				}
			}
		}

		if (isset($this->template['cs_web']) && $this->template['cs_web'] == true) {
			$insert['id_cs_web'] = $_SESSION['webId'];
		}

		return $insert;
	}

	protected function uploadImages($imagesToUpload, $newId)
	{
		if (count($imagesToUpload['files']) > 0) {
			$webDir = str_replace('cs_','',$this->table).'/'.$_SESSION['webId'].'/';
			$fileSystem = new Filesystem();
			if (!$fileSystem->exists(\Dir::resources().'/'.$webDir)) {
				$fileSystem->mkdir(\Dir::resources().'/'.$webDir);
			}
			foreach ($imagesToUpload['files'] as $dbName => $files) {
				if ($files['name'] != '') {
					$name = $files['name'];
					$tmpname = $files['tmp_name'];
					$type = $files['type'];
					$error = $files['error'];
					$size = $files['size'];

					if ($name != '' && $size > 0 && $error == 0) {
						$insFile = [];
						$fileName = strtolower(basename($name));
						$newFileName = Strings::generateFileName($fileName,$newId,$fileName);
						$target_file = \Dir::resources().'/'.$webDir.$newFileName;
						if (move_uploaded_file($tmpname, $target_file)) {
							$insFile = [
								$dbName => '/resources/'.$webDir.$newFileName,
							];
							$data = new Data($this->table);
							if ($data->update($insFile,['where' => 'id = '.intval($newId)])) {
								\WC::component()->alert()->prepare('Obrázek '.$name.' byl úspěšně uložen pod názvem '.$newFileName.'.');
							}
						}
					}
				}
			}
		}
	}

	protected function uploadFiles($filesToUpload, $newId)
	{
		foreach ($filesToUpload['files'] as $fileTable=>$files) {
			$dataFile = new Data($fileTable);
			$code = $filesToUpload['code'] != '' ? $filesToUpload['code'].'_' : '';
			$webDir = str_replace('cs_','',$fileTable).'/';
			$fileSystem = new Filesystem();
			if (!$fileSystem->exists(\Dir::resources().'/'.$webDir)) {
				$fileSystem->mkdir(\Dir::resources().'/'.$webDir);
			}
			$keys = [];
			foreach ($files['name'] as $fk=>$file) {
				if ($file != '') {
					$keys[] = $fk;
				}
			}
			if ($keys) {
				foreach ($keys as $key) {
					$name = $files['name'][$key];
					$tmpname = $files['tmp_name'][$key];
					$type = $files['type'][$key];
					$error = $files['error'][$key];
					$size = $files['size'][$key];
					$fileInfo = $filesToUpload['filenames'][$fileTable][$key];

					if ($name != '' && $size > 0 && $error == 0) {
						$insFile = [];
						$fileName = $code.strtolower(basename($name));
						$newFileName = Strings::generateFileName($fileName,$newId,$fileName);
						$target_file = \Dir::resources().'/'.$webDir.$newFileName;
//						$fileSystem->copy($tmpname,$target_file,true);
						if (move_uploaded_file($tmpname, $target_file)) {
							$insFile = [
								'id_' . $this->table => intval($newId),
								'filename' => '/resources/'.$webDir.$newFileName,
								'name' => $fileInfo,
							];
							if ($newIdFile = $dataFile->insert(['column' => [$insFile]])) {
								\WC::component()->logger()->info('FILE: ', ['page' => $this->template['title'], 'table' => $fileTable, 'user' => $this->user->user['email'], 'data' => $insFile]);
							}
						}
					}
				}
			}
		}
	}

}
