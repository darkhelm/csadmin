<?php

namespace Classes\Actions;

use Renderers\Renderer;

class WebListItems
{

	public function process($column, $key, $value = null, $linkTable = null, $table = null)
	{
		$items = [];
		if ($id = \WC::app()->get('id')) {
			if ($list = \WC::component()->webList()->getListById($id)) {
				$items = $list->getSettings();
			}
		}

		$vars = [
			'items' => $items,
		];
		return (new Renderer())->render(\Dir::template() . '/../Renderers/WebList/list.latte', $vars);
	}

}
