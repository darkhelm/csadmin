<?php

namespace Classes\Actions;

use Renderers\Renderer;

class BannerArticle
{

	public function process($column, $key, $value = null, $linkTable = null, $table = null)
	{
		$articles = \WC::component()->article()->getArticles();

		$items = [];
		if ($id = \WC::app()->get('id')) {
			if ($banner = \WC::component()->banner()->getBannerById($id)) {
				$items = $banner->getArticles();
			}
		}

		$vars = [
			'articles' => $articles,
			'items' => $items,
		];
		return (new Renderer())->render(\Dir::template() . '/../Renderers/Banner/articles.latte', $vars);
	}



}
