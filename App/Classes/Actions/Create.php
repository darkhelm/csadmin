<?php

namespace Classes\Actions;

use Classes\Db\Data;

class Create extends DefaultAction {

	protected $data;

	public function __construct()
	{
		parent::__construct();
		if (is_array($_POST) && count($_POST) > 0){
			if (isset($this->template['processForm'])) {
				$this->processForm($_POST);
			} else {
				$s = $this->prepareSave($_POST);
				return $this->save($s);
			}
		}
	}

	public function create()
	{
		if ($this->canCreate) {
			$tabs = '<div class="tabs">';
			$i = 0;
			foreach ($this->prepareForm() as $tab=>$items) {
				$tr = '';
				$tabHelp = isset($this->template['tabs'][$tab]) ? $this->template['tabs'][$tab] : '';
				foreach ($items as $item) {
					$tr .= $item;
				}
				$i++;
				$tabs .= '
					<input type="radio" name="tabs" id="tab'.$i.'" '.($i == 1 ? "checked" : "").' />
					<label for="tab'.$i.'" role="tab" aria-selected="true" aria-controls="panel'.$i.'" tabindex="0">'.$this->tabs[$tab].'</label>
					<div class="tab">
						'.$tabHelp.'
						<table class="table table-bordered table-admin">
							<tbody>
								'.$tr.'
							</tbody>
						</table>
					</div>
				';
			}
			$tabs .= '</div>';
			return '
				<form method="post" class="form-admin" enctype="multipart/form-data">
					'.$tabs.'
					<input type="submit" value="Uložit" class="btn btn-primary">
				</form>
			';
		}
		\WC::app()->redirectWithAlert('/dashboard', 'Pro tuto akci nemáte dostatečná oprávnění.');
	}

	protected function prepareForm()
	{
		$r = [];
		foreach ($this->columns as $key=>$column){
			$tab = isset($column['tab']) ? $column['tab'] : 'basic';
			$rowClass = '';
			if (isset($column['edit']) && $column['edit'] == false) {
				$rowClass = 'is-hidden';
			}
//			if (!isset($column['edit']) || $column['edit'] == true) {
			$hint = '';
			if (isset($column['hint'])) {
				$hint = '
				 <div class="hint">' . $column['hint'] . '</div>
			';
			}
			$r[$tab][] = '
				<tr class="'.$rowClass.'">
					<th class="table-th-name">
                        <label for="' . $key . '">' . $column['title'] . '</label>
                        ' . $hint . '
                    </th>
					<td>
						' . $this->format($column, $key) . '
						'.(isset($column['help']) && $column['help'] != '' ? '<div class="alert alert-warning mt-2" role="alert">'.$column['help'].'</div>' : '').'
					</td>
				</tr>
			';
//			}
		}

		return $r;
	}

	private function getExisting($table, $where)
	{
		$d = new Data($table);
		$existing = [];
		if ($selected = $d->select(['columns' => '*', 'where' => $where])) {
			foreach ($selected as $ut) {
				$existing[$ut['id']] = $ut;
			}
		}
		return $existing;
	}

	private function save($dataSet)
	{

		$insert = $dataSet['insert'];
		$success = [];
		if (is_array($insert) && count($insert) > 0) {
			$data = new Data($this->table);
			if ($newId = $data->insert(['column' => [$insert]])){
				if (isset($insert['sort']) && $insert['sort'] == '') {
					$upd['sort'] = $newId;
					$data->update($upd, ['where' => 'id = '.intval($newId)]);
				}
				$name = isset($insert['name']) ? $insert['name'] : $this->id;
				$success[] = "Byla vytvořena položka ".$name.".";
				\WC::component()->logger()->info('CREATE: ', ['page' => $this->template['title'], 'table' => $this->table, '#id' => $newId, 'user' => $this->user->getAttr('email')]);

				if ($this->table == 'cs_user') {
					$d = new Data('cs_user_web');
					if ($this->template['cs_web']) {
						$ins = [
							'id_cs_user' => intval($newId),
							'id_cs_web' => intval($_SESSION['webId']),
						];
						if ($newId = $d->insert(['column' => [$ins]])) {
							\WC::component()->logger()->info('UPDATE: ', [
								'page' => $this->template['title'],
								'table' => 'cs_user_auth',
								'#id_user' => $newId,
								'user' => $this->user->getAttr('email'),
								'data' => $ins
							]);
						}
					} else {
						foreach ($dataSet['insertUserWeb'] as $auth => $on) {
							$ins = [
								'id_cs_user' => intval($newId),
								'id_cs_web' => intval($auth),
							];
							if ($newJoinId = $d->insert(['column' => [$ins]])) {
								\WC::component()->logger()->info('UPDATE: ', [
									'page' => $this->template['title'],
									'table' => 'cs_user_auth',
									'#id_user' => $newId,
									'user' => $this->user->getAttr('email'),
									'data' => $ins
								]);
							}
						}
					}
				}
				if ($this->table == 'cs_web') {
					$data = new \Classes\Db\Data('cs_templates');
					if ($templates = $data->select(['columns' => '*'])) {
						$d = new \Classes\Db\Data('cs_web_template');
						foreach ($templates as $template) {
							$insert = [
								'id_cs_web' => $newId,
								'code' => $template['code'],
								'template' => $template['template'],
								'name' => $template['name'],
								'description' => $template['description'],
								'subject' => $template['name'],
								'type' => $template['type'],
							];
							if ($d->insert(['column' => [$insert]])) {
								$success[] = 'Šablony byly zkopírovány.';
								\WC::component()->logger()->info('CREATE: ', ['page' => $this->template['title'], 'table' => 'cs_web_template', '#id_web' => $newId, 'user' => $this->user->getAttr('email'), 'data' => $insert]);
							}
						}
					}
					$data = new \Classes\Db\Data('cs_longtexts');
					if ($templates = $data->select(['columns' => '*'])) {
						$d = new \Classes\Db\Data('cs_web_longtext');
						foreach ($templates as $template) {
							$insert = [
								'id_cs_web' => $newId,
								'code' => $template['code'],
								'name' => $template['name'],
								'text' => $template['text'],
								'class' => $template['class'],
							];
							if ($d->insert(['column' => [$insert]])) {
								$success[] = 'Longtexty byly zkopírovány.';
								\WC::component()->logger()->info('CREATE: ', ['page' => $this->template['title'], 'table' => 'cs_web_longtext', '#id_web' => $newId, 'user' => $this->user->getAttr('email'), 'data' => $insert]);
							}
						}
					}
					$data = new \Classes\Db\Data('cs_text');
					if ($templates = $data->select(['columns' => '*'])) {
						$d = new \Classes\Db\Data('cs_web_text');
						foreach ($templates as $template) {
							$insert = [
								'id_cs_web' => $newId,
								'code' => $template['code'],
								'content' => $template['content'],
								'description' => $template['description'],
							];
							if ($d->insert(['column' => [$insert]])) {
								$success[] = 'Obecné texty byly zkopírovány.';
								\WC::component()->logger()->info('CREATE: ', ['page' => $this->template['title'], 'table' => 'cs_web_text', '#id_web' => $newId, 'user' => $this->user->getAttr('email'), 'data' => $insert]);
							}
						}
					}
				}

				$imagesToUpload = $dataSet['imagesToUpload'];
				if (isset($imagesToUpload['files']) && is_array($imagesToUpload['files']) && count($imagesToUpload['files']) > 0) {
					$this->uploadImages($imagesToUpload, $this->id);
				}
			}
		}
		if ($success) {
			\WC::component()->alert()->success($success);
		}
		\WC::app()->redirect('/'.\WC::app()->get('baseLink'));
	}

}
