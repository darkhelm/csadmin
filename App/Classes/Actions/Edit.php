<?php

namespace Classes\Actions;

use Classes\Db\Data;
use Classes\Helpers\AdminTemplate;
use Classes\Helpers\Functions;

class Edit extends DefaultAction
{

	protected $data;
	private $specialTypes = ['mapValues', 'userAuth', 'files', 'userWeb', 'articleTags', 'bannerArticle', 'webListItems'];

	public function __construct()
	{
		parent::__construct();
		if (!$this->rowData || ($this->table == 'cs_web' && $this->id != $_SESSION['webId']) || (isset($this->template['cs_web']) && $this->template['cs_web'] && $this->rowData['id_cs_web'] != $_SESSION['webId']) || (isset($this->template['editable']) && $this->template['editable'] != 'all') && !AdminTemplate::isAuthorized($this->rowData)) {
			\WC::app()->redirect(\WC::app()->getLink(), false);
		}
		if (is_array($_POST) && count($_POST) > 0) {
			if (isset($this->template['processForm'])) {
				$this->processForm($_POST);
			} else {
				$s = $this->prepareSave('', $_POST);
				return $this->save($s);
			}
		}
	}

	public function edit()
	{
		if ($this->canEdit) {
			$tabs = '<div class="tabs">';
			$i = 0;
			foreach ($this->prepareForm() as $tab=>$items) {
				$tr = '';
				$tabHelp = isset($this->template['tabs'][$tab]) ? '<div class="alert alert-warning" role="alert">'.$this->template['tabs'][$tab].'</div>' : '';
				foreach ($items as $item) {
					$tr .= $item;
				}
				$i++;
				$saveTr = $i == 1 ? '<tr class="save"><td colspan="2"><input type="submit" value="Uložit" class="btn btn-primary"></td></tr>': '';
				$tabs .= '
					<input type="radio" name="tabs" id="tab'.$i.'" '.($i == 1 ? "checked" : "").' />
					<label for="tab'.$i.'" role="tab" aria-selected="true" aria-controls="panel'.$i.'" tabindex="0">'.$this->tabs[$tab].'</label>
					<div class="tab">
						'.$tabHelp.'
						<table class="table table-bordered table-admin">
							<tbody>
								'.$tr.'
							</tbody>
							'.$saveTr.'
						</table>
					</div>
				';
			}
			$tabs .= '</div>';
			return '
				<form method="post" class="form-admin" enctype="multipart/form-data">
					'.$tabs.'
				</form>
			';
		}
		\WC::app()->redirectWithAlert('/dashboard', 'Pro tuto akci nemáte dostatečná oprávnění.');
	}

	protected function prepareForm()
	{
		$values = [];
		$r = [];
		foreach ($this->columns as $key => $column) {
			if (!isset($column['auth']) || \WC::component()->users()->isAuthorized($column['auth'])) {
				$tab = isset($column['tab']) ? $column['tab'] : 'basic';
				if (isset($column['type']) && !in_array($column['type'], $this->specialTypes)) {
					$values = $this->getValues($this->template['table'], $key, 'id = ' . $this->id)[0];
				}
				$hint = '';
				if (isset($column['hint'])) {
					$hint = '
						<div class="hint">' . $column['hint'] . '</div>
					';
				}
				$class = isset($column['edit']) && $column['edit'] == false ? 'hidden ' : '';
				if (isset($column['rowClass'])) {
					$class .= $column['rowClass'];
				}
				$r[$tab][] = '
					<tr class="' . $class . '">
						<th class="table-th-name">
							<label for="' . $key . '">' . $column['title'] . '</label>
							' . $hint . '
						</th>
						<td>
							' . $this->format($column, $key, isset($values[$key]) ? $values[$key] : '') . '
							'.(isset($column['help']) && $column['help'] != '' ? '<div class="alert alert-warning mt-2" role="alert">'.$column['help'].'</div>' : '').'
						</td>
					</tr>
				';
			}
		}

		return $r;
	}

	protected function prepareSave($table = null, $columns = null)
	{
		$r = $userWeb = $mapValues = $linkTable = $filesToUpload = $imagesToUpload = [];
		$insertMapped = $insertUserWeb = [];
		$table = $table ? $table : $this->template['table'];
		$columns = $columns ? $columns : $this->template['columns'];
		if (isset($columns['tabs'])) {
			unset($columns['tabs']);
		}
		foreach ($this->template['columns'] as $ktc=>$tc) {
			if (isset($tc['type']) && $tc['type'] == 'mapValues') {
				$mapValues[$ktc] = [
					'typeTable' => $tc['typeTable'],
				];
			}
			if (isset($tc['type']) && $tc['type'] == 'linkedTable') {
				$linkTable[$ktc] = [
					'typeTable' => $tc['joinedTable'],
				];
			}
			if (isset($tc['type']) && $tc['type'] == 'userWeb') {
				$userWeb[$ktc] = [
					'typeTable' => $tc['typeTable'],
				];
			}
			if (isset($tc['type']) && $tc['type'] == 'files') {
				$names[$ktc] = $_POST['filename_'.$ktc];
				$filesToUpload = [
					'filenames' => $names,
					'files' => $_FILES,
					'code' => isset($tc['fileCode']) ? $tc['fileCode'] : '',
				];
				unset($columns['filename_'.$ktc]);
				unset($columns[$ktc]);
			}

			if (isset($tc['type']) && $tc['type'] == 'image') {
				$names[$ktc] = $_POST[$ktc];
				$imagesToUpload = [
					'files' => $_FILES,
				];
				unset($columns[$ktc]);
			}
		}
		foreach ($columns as $key=>$column) {
			if ($userWeb) {
				if (isset($columns['cs_user_web'])) {
					foreach ($columns['cs_user_web'] as $auth => $val) {
						if ($val) {
							$insertUserWeb[$auth] = $val;
						}
					}
				}
				unset($columns['cs_user_web']);
			}
			if ($mapValues) {
				foreach ($mapValues as $keyMap => $mapTable) {
					preg_match("/^{$keyMap}(.*)/", $key, $result);
					if ($result) {
						$parts = explode('-', $result[0]);
						$insertMapped[$mapTable['typeTable']][$parts[0]][$parts[1]][$parts[2]] = $column;
						unset($columns[$key]);
					}
				}
			}

//			if ($linkTable) {
//				foreach ($linkTable as $keyMap => $mapTable) {
//					preg_match("/^{$keyMap}(.*)/", $key, $result);
//					if ($result) {
//						$mk = explode('-', $result[0]);
//						$insertTeamSeason[$mk[1]] = $column;
//						unset($columns[$key]);
//					}
//				}
//			}
		}

		$r['insert'] = $this->prepareColumnsToInsert($columns);
		$r['insertMapped'] = $insertMapped;
		$r['insertUserWeb'] = $insertUserWeb;
		$r['filesToUpload'] = $filesToUpload;
		$r['imagesToUpload'] = $imagesToUpload;

		return $r;
	}

	private function getExisting($table, $where)
	{
		$d = new Data($table);
		$existing = [];
		if ($selected = $d->select(['columns' => '*', 'where' => $where])) {
			foreach ($selected as $ut) {
				$existing[$ut['id']] = $ut;
			}
		}
		return $existing;
	}

	private function save($dataSet)
	{
		$insert = $dataSet['insert'];
		$success = [];
		$data = new Data($this->table);
		if ($data->update($insert, ['where' => 'id = ' . intval($this->id)])) {
			$name = isset($insert['name']) ? $insert['name'] : $this->id;
			$success[] = "Položka " . $name . " byla aktualizována.";
			\WC::component()->logger()->info('UPDATE: ', ['page' => $this->template['title'], 'table' => $this->table, '#id' => $this->id, 'user' => $this->user->getAttr('email')]);

			if ($this->table == 'cs_user') {
				$d = new Data('cs_user_web');
				$existing = $this->getExisting('cs_user_web', 'id_cs_user = '.intval($this->id));
				foreach ($dataSet['insertUserWeb'] as $auth=>$on) {
					if (count($existing) > 0 && in_array($auth, $existing)) {
						unset($existing[array_search($auth, $existing)]);
					} else {
						$ins = [
							'id_cs_user' => intval($this->id),
							'id_cs_web' => intval($auth),
						];
						if ($newId = $d->insert(['column' => [$ins]])){
							\WC::component()->logger()->info('UPDATE: ', ['page' => $this->template['title'], 'table' => 'cs_user_web', '#id_user' => $this->id, 'user' => $this->user->getAttr('email'), 'data' => $ins]);
						}
					}
				}
				if (count($existing) > 0) {
					foreach ($existing as $k=>$v) {
						if ($d->delete($k)) {
							\WC::component()->logger()->info('DELETE: ', ['page' => $this->template['title'], 'table' => 'cs_user_web', '#id_user' => $this->id, 'user' => $this->user->getAttr('email'), 'data' => $v]);
						}
					}
				}
			}

			$insertMapped = $dataSet['insertMapped'];
			if (is_array($insertMapped) && count($insertMapped) > 0) {
				$idParent = 'id_' . $this->table;
				$mapInsert[$idParent] = $this->id;
				foreach ($insertMapped as $typeTable => $table) {
					$idType = 'id_' . $typeTable;
					foreach ($table as $mapTable => $values) {
						$dataMap = new Data($mapTable);
						foreach ($values as $key => $v) {
							$mapInsert[$idType] = $key;
							foreach ($v as $k=>$item) {
								$mapInsert[$k] = $item;
							}
							if (isset($mapInsert['id']) && $mapInsert['id'] != '') {
								$updateId = $mapInsert['id'];
								unset($mapInsert['id']);
								if ($newMapId = $dataMap->update($mapInsert, ['where' => 'id = ' . intval($updateId)])) {
									$success[] = "Položka " . $mapTable . " byla aktualizována.";
									\WC::component()->logger()->info('UPDATE: ', ['page' => $this->template['title'], 'table' => $mapTable, '#id' => $updateId, 'user' => $this->user->getAttr('email')]);
								} else {
									\WC::component()->logger()->error('UPDATE: ', ['page' => $this->template['title'], 'table' => $mapTable, '#id' => $updateId, 'user' => $this->user->getAttr('email')]);
								}
							} else {
								if ($newMapId = $dataMap->insert(['column' => [$mapInsert]])) {
									$success[] = "Byla vytvořena nová položka " . $mapTable . ".";
									\WC::component()->logger()->info('CREATE: ', ['page' => $this->template['title'], 'table' => $mapTable, '#id' => $newMapId, 'user' => $this->user->getAttr('email')]);
								} else {
									\WC::component()->logger()->error('CREATE: ', ['page' => $this->template['title'], 'table' => $mapTable, 'user' => $this->user->getAttr('email')]);
								}
							}
						}
					}
				}
			}

			$filesToUpload = $dataSet['filesToUpload'];
			if (isset($filesToUpload['files']) && is_array($filesToUpload['files']) && count($filesToUpload['files']) > 0) {
				$this->uploadFiles($filesToUpload, $this->id);
			}
			$imagesToUpload = $dataSet['imagesToUpload'];
			if (isset($imagesToUpload['files']) && is_array($imagesToUpload['files']) && count($imagesToUpload['files']) > 0) {
				$this->uploadImages($imagesToUpload, $this->id);
			}
		}
		if ($success) {
			\WC::component()->alert()->success(implode('<br>', $success));
		}
		\WC::app()->redirect('/' . \WC::app()->get('baseLink'));
	}

}
