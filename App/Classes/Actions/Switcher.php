<?php

namespace Classes\Actions;

use Classes\Db\Data;
use Classes\Helpers\AdminTemplate;

class Switcher extends DefaultAction {

	protected $data;

	public function __construct()
	{
		parent::__construct();
		if (!$this->rowData || (isset($this->template['editable']) && $this->template['editable'] != 'all' || !AdminTemplate::isAuthorized($this->rowData))){
			\WC::app()->redirect(\WC::app()->getLink(), false);
		}
	}

	public function switcher()
	{
		if (\WC::app()->get('column') && \WC::app()->get('id')) {
			$column = \WC::app()->get('column');
			$id = \WC::app()->get('id');

			$data = new Data($this->table);
			$check = $data->selectSingle([
				'columns' => $column,
				'where' => 'id = '.intval($id)
			]);
			$switchTo = ($check[$column] == 1) ? 0:1;
			$cols[$column] = $switchTo;

			if ($update = $data->update($cols, ['where' => 'id = '.intval($id)])){
				\WC::component()->alert()->success("Položka {$id} změněna.");
				\WC::component()->logger()->info('UPDATE: ', ['page' => $this->template['title'], 'table' => $this->table, '#id' => $id, 'user' => $this->user->user['email'], 'data' => $cols]);
			} else {
				\WC::component()->alert()->error("Položku {$id} se nepodařilo aktualizovat.");
				\WC::component()->logger()->error('UPDATE: ', ['page' => $this->template['title'], 'table' => $this->table, '#id' => $id, 'user' => $this->user->user['email']]);
			}
			\WC::app()->redirect(\WC::app()->getLink(), false);
		}
	}

}
