<?php

namespace Classes\Actions;

use Classes\Formatters\Date;
use Classes\Modules\Web\Web;

class UserWeb
{

	public function process($column, $key, $value = null, $linkTable = null)
	{
		$return = $tr = '';
		$webArray = [];
		if ($webs = \WC::component()->web()->getWebs()) {
			if ($userWebs = \WC::component()->web()->getUserWebs(\WC::app()->get('id'))) {
				foreach ($userWebs as $userWeb) {
					$webArray[] = $userWeb['id'];
				}
			}
			/** @var Web $web */
			foreach ($webs as $web) {
				$checked = in_array($web->getId(), $webArray) ? 'checked' : '';
				$tr .= '
					<tr>
						<td class="checkbox"><input type="checkbox" name="cs_user_web['.$web->getId().']" '.$checked.'></td>
						<td>'.$web->getName().'</td>
						<td class="short text-center">'.$web->isActive(true).'</td>
						<td class="short text-center">'.$web->isPublic(true).'</td>
					</tr>
				';
			}
		}

		if ($tr) {
			$return = '
				<table class="table table-sm table-condensed table-bordered">
					<thead class="thead-brand">
						<tr>
							<th></th>
							<th>Název</th>
							<th class="short text-center">Aktivní</th>
							<th class="short text-center">Veřejný</th>
						</tr>
					</thead>
					<tbody>
						'.$tr.'
					</tbody>
				</table>
			';

		}

		return $return;
	}

}
