<?php

namespace Classes\Actions;

use Classes\Db\Data;

class Files
{

	public function process($column, $key, $value = null, $linkTable = null, $table = null)
	{
		$listFiles = $this->getFiles($key, $table);
		return '
			<div class="files-list">'.$listFiles.'</div>
			<table class="table-files table-admin table-sm">
				<thead class="thead-brand">
					<tr><th>Soubor</th><th>Popis</th></tr>
				</thead>
				<tbody>
					<tr>
						<td><input type="file" class="form-control" name="'.$key.'[]"></td>
						<td colspan="49"><input type="text" class="form-control" name="filename_'.$key.'[]"></td>
					</tr>
					<tr class="ui-widget-header">
						<td colspan="50"><input type="button" value="další soubor"></td>
					</tr>
				</tbody>
			</table>
		';
	}

	private function getFiles($key, $table)
	{
		$dataFile = new Data($key);
		$listFiles = '';
		if ($id = \WC::app()->get('id')) {
			if ($files = $dataFile->select(['columns' => '*', 'where' => 'id_' . $table . ' = ' . $id])) {
				$listFiles .= '<table class="table-files table-admin table-sm">';
				foreach ($files as $file) {
					$path_parts = pathinfo($file['filename']);
					$icon = '';
					if ($path_parts['extension'] == 'pdf') {
						$icon = 'file-pdf text-danger';
					} elseif ($path_parts['extension'] == 'doc' || $path_parts['extension'] == 'docx') {
						$icon = 'file-word';
					} elseif ($path_parts['extension'] == 'msg') {
						$icon = 'envelope text-info';
					} elseif ($path_parts['extension'] == 'xls' || $path_parts['extension'] == 'xlsx') {
						$icon = 'file-excel text-success';
					} elseif ($path_parts['extension'] == 'jpg' || $path_parts['extension'] == 'jpeg' || $path_parts['extension'] == 'png' || $path_parts['extension'] == 'gif') {
						$icon = 'file-image text-secondary';
					}
					$listFiles .= '
							<tr class="files">
								<td style="width: 25px" class="text-center"><a href="' . $file['filename'] . '" title="' . $file['filename'] . '" target="_blank"><i class="fa fa-fw fa-2x fad fa-' . $icon . '"></i></a></td>
								<td class="filename">'.$file['name'].'</td>
								<td class="filename">' . $path_parts['basename'] . '</td>
								<td style="width: 25px" class="text-center"><a href="" class="delete_link_file" data-fileurl="' .$file['filename'] . '" data-filename="' . $path_parts['basename'] . '" data-table="' . $key . '" data-file="' . $file["id"] . '"><i class="fa far fa-times-circle text-danger"></i> </a></td>
							</tr>
						';
				}
				$listFiles .= '</table>';
			}
		}
		return $listFiles;
	}

}
