<?php

namespace Classes\Actions;

use Classes\Db\Data;

class ArticleTags
{

	private $tags = [];
	private $articleTags = [];
	private $id;

	public function process($column, $key, $value = null, $linkTable = null, $table = null)
	{
		$this->loadArticleTags();
		$this->id = \WC::app()->get('id');
		$type = \WC::app()->parseUrl(\WC::app()->getLink());
		if (end($type) == 'clanky') {
			$this->getArticleTags();
		} elseif (end($type) == 'bannery') {
			$this->getBannerArticleTags();
		}

		$r = [];
		if ($this->tags) {
			foreach ($this->tags as $tag) {
				$checked = in_array($tag['id'], $this->articleTags) ? 'checked' : '';
				$r[] = '
					<div class="badge badge-light p-2 mr-2">
						<label><input type="checkbox" value="'.$tag['id'].'" name="article_tags[]" class="mr-2" '.$checked.'>'.$tag['name'].'</label>
					</div>
				';
			}
		}

		if (count($r) > 0) {
			return implode('',$r);
		}
		return false;
	}

	private function getBannerArticleTags()
	{
		if ($this->id) {
			$data = new Data('cs_web_banner_web_tag');
			if ($tags = $data->select(['columns' => '*', 'where' => 'id_cs_web_banner IN (SELECT id FROM cs_web_banner WHERE id = '.intval($this->id).' AND id_cs_web = '.intval($_SESSION['webId']).')'])) {
				foreach ($tags as $tag) {
					$this->articleTags[] = $tag['id_cs_web_tag'];
				}
			}
		}
	}

	private function getArticleTags()
	{
		if ($this->id) {
			$data = new Data('cs_web_article_web_tag');
			if ($tags = $data->select(['columns' => '*', 'where' => 'id_cs_web_article IN (SELECT id FROM cs_web_article WHERE id = '.intval($this->id).' AND id_cs_web = '.intval($_SESSION['webId']).')'])) {
				foreach ($tags as $tag) {
					$this->articleTags[] = $tag['id_cs_web_tag'];
				}
			}
		}
	}

	private function loadArticleTags()
	{
		if (!$this->tags) {
			$data = new Data('cs_web_tag');
			if ($tags = $data->select(['columns' => '*', 'where' => 'id_cs_web = '.intval($_SESSION['webId'])])) {
				$this->tags = $tags;
			}
		}
	}

}
