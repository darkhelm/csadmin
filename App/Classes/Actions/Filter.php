<?php

namespace Classes\Actions;

use Classes\Db\Data;

class Filter
{

	private $searchColumns = [];
	private $availableSearchColumns;
	private $columns;
	private $query;
	private $queryData;
	private $sessionName;

	public function __construct($template)
	{
		$this->availableSearchColumns = $template['filter']['setColumns'];
		$this->columns = $template['columns'];
		$this->sessionName = $template['table'];
		$this->addColumn($this->columns);
	}

	public function searchQuery($data)
	{
		$wh = $this->queryData = [];
		if (isset($data['filterList'])){
			$jTable[] = $this->sessionName;
			foreach ($data as $key=>$value){
				if ($key != 'filterList') {
					$key = str_replace('filter-','', $key);
					$e = explode('-', $key);
					if (count($e) > 1){
						$this->queryData[$e[0].'.'.$e[1]] = $value;
						if ($e[0] != $this->sessionName){
							$jTable[] = $e[0].' ON '.$this->sessionName.'.id = '.$e[0].'.id_'.$this->sessionName;
						}
					} else {
						$this->queryData[$this->sessionName.'.'.$e[1]] = $value;
					}
					preg_match("/^id_(.*)/", $e[1], $result);
					if (is_array($result) && count($result) > 0){
						if ($value != ''){
							$wh[] = $key . ' =' . $value;
						}
					} else {
						$wh[] = $key . ' LIKE "%' . $value . '%"';
					}
					$_SESSION['filter-' . $this->sessionName][] = [$key => $value];
				}
			}
			$this->query['where'] = implode(' AND ', $wh);
			$this->query['tables'] = implode(' LEFT JOIN ', $jTable);
			$_SESSION['filter-'.$this->sessionName]['query'] = $this->query;
		}
		return $this->query;
	}

	public function getQuery()
	{
		$r = null;
		if (isset($_SESSION['filter-'.$this->sessionName])){
			foreach ($_SESSION['filter-'.$this->sessionName] as $key=>$items){
				if ($key !== 'query'){
					foreach ($items as $k=>$v){
						if ($k != 'query') {
							$r[$k] = $v;
						}
					}

				}
			}
		}
		return $r;
	}

	public function generateForm()
	{
		$query = $this->getQuery();

		if (is_array($this->searchColumns)) {
			$r = '
				<form action="'.\WC::app()->getFullUrl().'" method="post" class="filter">
					<h2>Filtr</h2>
					<p>
			';
			foreach ($this->searchColumns as $v=>$c) {
				$c['searchPreformat'] = true;
				$r .= '<span><strong>' . $c["title"] . ': </strong>' . $this->formatEdit($c, $v, (isset($query[$v]) ? $query[$v] : null)) . '</span>'; // (isset($query[$c["key"]]) ? $query[$c["key"]] : null)
			}

			$r .= '
						<input name="filterList" value="1" type="hidden" />
						<span><input type="submit" value="Filtrovat" class="btn btn-xsm btn-secondary" /></span>
					</p>
				</form>
			';
			return $r;
		}
	}

	public function formatEdit ($column, $key, $value = NULL, $id = NULL) {
		if ($column['type'] == 'input' || $column['type'] == 'paragraph' || $column['type'] == 'paragraphSimple'){
			return '<input name="filter-'.$column['name'].'" value="'.$value.'">';
		} elseif ($column['type'] == 'select'){
			$keyMatch = explode('-', $key);
			preg_match("/^id_(.*)/", $keyMatch[1], $result);
			if (is_array($result) && count($result) == 2){
				$table = $result[1];
				if ($o = $this->getOptions($column, $table, $value)) {
					return '
						<select name=' . $key . ' class="">
							' . $o . '
						</select>
					';
				}
			}
		} elseif ($column['type'] == 'checkbox' || $column['type'] == 'switcher' || $column['type'] == 'trueFalse'){
			$o = '<option value="" selected>--- vše ---</option>';
			$o .= '<option value="0" '.($value == '0' ? 'selected' : '').'>Ne</option>';
			$o .= '<option value="1" '.($value == '1' ? 'selected' : '').'>Ano</option>';

			return '
				<select name=' . $key . ' class="">
				'.$o.'
				</select>
			';
		} else {
			$class = '\\Classes\\Actions\\'.ucfirst($column['type']);
			if (class_exists($class)) {
				return (new $class())->prepareFilter($key, $value);
			}
		}
	}

	private function getOptions($column, $table, $value = null)
	{
		$option = false;
		$where = isset($column['where']) ? $column['where'] : '';
		$order = isset($column['order']) ? $column['order'] : '';

		$data = new Data($table);
		if ($select = $data->select([
			'columns' => '*',
			'order' => $order,
			'where' => $where
		])){
			$option .= '<option value="">--- vybrat ---</option>';
			foreach ($select as $sel) {
				$names = [];
				$selected = '';
				if ($value && $sel['id'] == $value) {
					$selected = 'selected';
				}
				if (isset($column['joined'])) {
					$joined = explode('|', $column['joined']);
					foreach ($joined as $item) {
						$names[] = $sel[$item];
					}
					$name = implode(', ', $names);
				} else {
					$name = $sel['name'];
				}
				$option .= '<option value="' . $sel['id'] . '" ' . $selected . '>' . $name . '</option>';
			}
		}
		return $option;
	}

	public function setGroups ($columns) {
		if (is_array($columns)) {
			foreach ($columns as $line) {
				if (is_array($line)) {
					foreach ($line as $columnCode) {
						$this->addColumn($columnCode);
					}
				}
			}
			$this->searchGroups = $columns;
		}
	}

	protected function addColumn($columns)
	{
		$names = [];
		foreach ($columns as $v=>$c) {
			if (isset($c['type']) && $c['type'] == 'mapValues') {
				foreach ($c['columns'] as $mapKey => $mapColumn) {
					$names[$v . '-' . $mapKey] = $c['columns'];
					$names[$v . '-' . $mapKey]['title'] = $mapColumn;
				}
			} else {
				$names[$this->sessionName . '-' . $v] = $c;
			}
		}
		foreach ($names as $k=>$c) {
			if (in_array($k, $this->availableSearchColumns)) {
				$column["name"] = $k;
				$column["title"] = $c["title"];
				$column["type"] = isset($c["type"]) ? $c['type'] : 'input';
				$column["joined"] = isset($c["joined"]) ? $c['joined'] : 'name';
				$column["order"] = isset($c["order"]) ? $c['order'] : 'name';
				$column["where"] = isset($c["where"]) ? $c['where'] : '';
				$this->searchColumns[$k] = $column;
			}
		}
	}

}
