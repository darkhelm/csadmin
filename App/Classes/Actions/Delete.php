<?php

namespace Classes\Actions;

use Classes\Db\Data;
use Classes\Helpers\AdminTemplate;

class Delete extends DefaultAction {

	public function __construct()
	{
		parent::__construct();
		if (!AdminTemplate::isAuthorized($this->rowData)){
			\WC::app()->redirect(\WC::app()->getLink(), false);
		}
	}

	public function icon()
	{
//		\WC::component()->user()->canDelete();
//		return '<a href="" class="delete_link" page="'.$this->url.'" rel="'.$this->rowData["id"].'"><i class="fa far fa-times-circle text-danger"></i> </a>';
	}

	public function delete()
	{
		$data = new Data($this->table);

		if ($data->delete(\WC::app()->get('id'))) {
			\WC::component()->logger()->info('DELETE: ', ['page' => $this->template['title'], 'table' => $this->table, '#id' => $this->id, 'user' => $this->user->user['email']]);
			$info[] = 'Položka '.$this->id.' smazána.';
			\WC::component()->alert()->success(implode('<br>', $info));
			\WC::app()->redirect(\WC::app()->getLink(), false);
		}
	}

}
