<?php

namespace Classes\Actions;

use Classes\Db\Data;

class ArticleSelect
{

	private $articles = [];

	public function prepareFilter($key, $value = NULL)
	{
		$this->loadArticles();
		$r = [];
		if ($this->articles) {
			foreach ($this->articles as $article) {
				$selected = $article['id'] == $value ? 'selected' : '';
				$r[] = '<option value="'.$article['id'].'" '.$selected.'>'.$this->getArticleFullName($article['id']).'</option>';
			}
		}

		if (count($r) > 0) {
			return '
				<select name="'.$key.'" class="">
					<option value="">---</option>
					'.implode('',$r).'
				</select>
			';
		}
		return false;
	}

	public function process($column, $key, $value = null, $linkTable = null, $table = null)
	{
		$this->loadArticles();

		$r = [];
		if ($this->articles) {
			foreach ($this->articles as $article) {
				if ($article['in_menu'] == 1 && (\WC::app()->get('id') != $article['id'])) {
					$selected = $article['id'] == $value ? 'selected' : '';
					$r[] = '<option value="'.$article['id'].'" '.$selected.'>'.$this->getArticleFullName($article['id']).'</option>';
				}
			}
		}

		if (count($r) > 0) {
			return '
				<select name="id_cs_web_article" class="form-control form-control-sm">
					<option value="">---</option>
					'.implode('',$r).'
				</select>
			';
		}
		return false;
	}

	private function loadArticles()
	{
		if (!$this->articles) {
			$data = new Data('cs_web_article');
			if ($articles = $data->select(['columns' => '*', 'where' => 'id_cs_web = '.intval($_SESSION['webId'])])) {
				$this->articles = $articles;
			}
		}
	}

	public function getArticleFullLink ($id, $link = '') {
		$this->loadArticles();
		if ($article = $this->getArticleById($id)) {
			$link = $article["code"].($link!=""?'/':'').$link;
			if ($article["id_cs_web_article"] != "")
				$link = $this->getArticleFullLink ($article["id_cs_web_article"], $link);
		}
		return $link;
	}

	public function getArticleFullName ($id, $link = '') {
		$this->loadArticles();
		if ($article = $this->getArticleById($id)) {
			$link = $article["name"].($link!=""?' >> ':'').$link;
			if ($article["id_cs_web_article"] != "")
				$link = $this->getArticleFullName ($article["id_cs_web_article"], $link);
		}
		return $link;
	}

	public function getArticleById ( $id ) {
		$this->loadArticles();
		if ($id != "" && $this->articles) {
			foreach ($this->articles as $a)
				if ($a["id"] == $id)
					return $a;
		}
		return false;
	}

//	public function getSubArticles ($parentId, $inMenu = true) {
//		$this->loadArticles();
//		if ($this->articles) {
//			$r = [];
//			foreach ($this->articles as $a) {
//				if (($inMenu && $a["in_menu"] == '1' || !$inMenu) && $a["id_cs_web_article"] == $parentId) {
//					$r[] = $a;
//				}
//			}
//			return $r;
//		}
//		return false;
//	}

}
