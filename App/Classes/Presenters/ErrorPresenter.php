<?php

namespace Classes\Presenters;

use Classes\Helpers\Functions;
use Classes\Helpers\Presenter;

class ErrorPresenter extends Presenter {

	protected $folder = 'Error/';
	public $class = 'error-class';

	public function __construct($link, $session = null, $user = null)
	{
		parent::__construct($link, $session, $user);
	}

	public function process()
	{
		Functions::redirect('/');
	}

}
