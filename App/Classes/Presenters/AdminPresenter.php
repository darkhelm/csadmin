<?php

namespace Classes\Presenters;

use Classes\Helpers\Presenter;
use Classes\Helpers\Template;

class AdminPresenter extends Presenter {

	protected $folder = 'Admin/';
	protected $action = 'default';

	private $allowedPages = ['web'];
	private $vars = [];

//	public function process()
//	{
//		if (isset($this->link) && is_array($this->link) && count($this->link) > 2) {
//			$link = $this->link[2];
//			if (in_array($link, $this->allowedPages)) {
//				$this->action = $link;
//				return $this->$link();
//			} else {
//				return Template::render('/Error/default');
//			}
//		}
//
//		return Template::render($this->folder.$this->action, $this->vars);
//	}
//
//	private function web()
//	{
//		if ($this->user->isSuperAdmin()) {
//			return Template::render($this->folder . $this->action, $this->vars);
//		}
//		return Template::render('/Error/default');
//	}

}
