<?php

namespace Classes\Helpers;

use Classes\Formatters\Date;
use Classes\Formatters\Strings;
use Classes\Modules\Web\Web;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Shared\Html;
use PhpOffice\PhpWord\Style\Font;
use PhpOffice\PhpWord\Style\Language;
use Symfony\Component\Filesystem\Filesystem;

class WordHelper
{

	public static function create(Web $web, $filename = 'tipovacka')
	{
		$phpWord = new PhpWord();

		$properties = $phpWord->getDocInfo();
		$properties->setCreator('Alex Potesil');
		$properties->setCompany('Alex Potesil');
		$properties->setTitle($web->getName());
		$properties->setCreated(time());
		$phpWord->getSettings()->setHideGrammaticalErrors(true);
		$phpWord->getSettings()->setHideSpellingErrors(true);

		$phpWord->setDefaultParagraphStyle(
			array(
				'alignment'  => \PhpOffice\PhpWord\SimpleType\Jc::START,
				'spaceAfter' => \PhpOffice\PhpWord\Shared\Converter::pointToTwip(2)
			)
		);

		self::prehled($web, $phpWord);

		$fileName = 'tipovacka_'.$web->getAttr('code').'.docx';
		$fileSystem = new Filesystem();
		if (!$fileSystem->exists(\Dir::resources().'/export/'.$web->getId().'/')) {
			$fileSystem->mkdir(\Dir::resources().'/export/'.$web->getId().'/');
		}
		$file = \Dir::resources().'/export/'.$web->getId().'/'.$fileName;
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save($file);

		return '/resources/export/'.$web->getId().'/'.$fileName;
	}

	private static function prehled(Web $web, PhpWord $phpWord)
	{
		$section = $phpWord->addSection([
				'orientation' => 'portrait',
				'marginTop' => '700',
				'marginBottom' => '700',
				'marginLeft' => '800',
				'marginRight' => '800',
				'pageNumberingStart' => 1
			]
		);
		$header = [
			'size' => 26,
			'bold' => true,
			'spaceAfter' => '200'
		];
		$fSection = [
			'size' => 13,
			'bold' => true,
		];
		$pSection = [
			'spaceAfter' => '150',
			'spaceBefore' => '250',
		];
		$fDescription = [
			'size' => 10,
			'bold' => false,
			'color' => '#777777'
		];
		$pDescription = [
			'indent' => .6,
		];
		$fText = [
			'size' => 11,
			'bold' => false,
		];
		$fTip = [
			'size' => 10,
			'bold' => false,
			'color' => '#333333',
		];
		$pText = [
			'indent' => .2,
			'spaceBefore' => '50',
		];
		$pTip = [
			'indent' => .5,
			'spaceBefore' => '20',
		];

		$section->addText($web->getName(), $header);
		$section->addText('');
		Html::addHtml($section, Strings::cleanWord($web->getAttr('description')));
		$section->addText('');
		Html::addHtml($section, Strings::cleanWord($web->getAttr('rules')));
		$section->addPageBreak();

		$sections = \WC::component()->otazka()->getSortedQuestions();
		foreach ($sections as $sec=>$questions) {
			$section->addText($sec,$fSection,$pSection);
			$i = 0;
			foreach ($questions as $question) {
				$i++;
				if ($question->getAttr('color') != '') {
					$fText['color'] = '#'.$question->getAttr('color');
				}
//				$textrun = $section->addTextRun($pText);
				$section->addText($i.'. '.$question->getAttr('name'), $fText, $pText);
				if ($question->getAttr('answers_count') == 1) {
					if ($question->getAttr('description')) { $section->addText($question->getAttr('description'), $fDescription, $pDescription); }
					$section->addText(' [['.$web->getId().'#'.$question->getId().'#TIPZDE]]', $fTip, $pTip);
				} else {
					if ($question->getAttr('description')) { $section->addText($question->getAttr('description'), $fDescription, $pDescription); }
					if ($question->getAttr('answers_count') > 1) {
						for ($i = 1; $i <= $question->getAttr('answers_count'); $i++) {
							if ($question->getAttr('answer_type') == 'opened') {
								$section->addText('[['.$question->getAttr('answer_type').'#'.$web->getId().'#'.$question->getId().'#'.$i.'#JMENO,DISCIPLINA#TIPPORADI]]', $fTip,$pTip);
							} elseif ($question->getAttr('answer_type') == 'opened-name') {
								$section->addText('[['.$question->getAttr('answer_type').'#'.$web->getId().'#'.$question->getId().'#'.$i.'#JMENO#DISCIPLINA]]', $fTip,$pTip);
							} else {
								$section->addText(' [['.$web->getId().'#'.$question->getId().'#TIPZDE]]', $fTip,$pTip);
							}
						}
					} else {

					}
				}
//				$section->addText($i.'. '.$question->getAttr('name').' [['.$web->getId().'=>'.$question->getId().'=>{TIPZDE}]]', $fText, $pText);
			}
		}

		$section->addText("");
		$section->addLine(['weight' => 1, 'width' => 1440, 'height' => 0]);
		$section->addText("Vygenerováno systémem www.csAdmin.cz: ".date("j. n. Y")." v ".date("H:i:s"));

		return $section;
	}

}
