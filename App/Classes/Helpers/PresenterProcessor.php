<?php

namespace Classes\Helpers;

class PresenterProcessor {

	protected $class = null;

	public static function process($link)
	{

		$presenterClassName = '\\Classes\\Presenters\\'.ucfirst($link).'Presenter';

		if (!class_exists($presenterClassName)) {
			$presenterClassName = '\\Classes\\Presenters\\ErrorPresenter';
		}

		return $presenterClassName;

	}

}
