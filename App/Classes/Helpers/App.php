<?php

namespace Classes\Helpers;

use Classes\Handlers\User;
use Classes\Handlers\UserSession;

class App {

	private static $instance = null;
	private $link;

	public static function getInstance() {
		if (self::$instance === NULL) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	public function redirect ($link, $full = true) {
		header ("Location: ".($full?$this->getBaseUrl():'').$link);
		exit;
	}
	public function redirectWithAlert ($link, $msg = 'Pro vstup na stránku nemáte dostatečná oprávnění!', $full = true) {
		\WC::component()->alert()->prepare($msg, 'error');
		header ("Location: ".($full?$this->getBaseUrl():'').$link);
		exit;
	}
	public function reload () {
		header ("Location: ".$this->getFullUrl());
		exit;
	}
	public function getFullUrl()
	{
		if (isset($_SERVER['HTTP_HOST'])) {
			return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		}
		return false;
	}
	public function getBaseUrl()
	{
		return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http")."://$_SERVER[HTTP_HOST]";
	}
	public function getLink()
	{
		return $this->getBaseUrl().'/'.$this->get('baseLink');
//		return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http")."://$_SERVER[HTTP_HOST]$_SERVER[REDIRECT_URL]";
	}

	public function parseUrl($url)
	{
		$data = explode('/', $url);
		$base = $d = [];
		foreach ($data as $item){
			$parts = explode('=',$item);
			if (count($parts) > 1){
				$d[$parts[0]] = $parts[1];
			} else {
				$d[] = $parts[0];
				$base[] = $parts[0];
			}
		}
		$this->link = $d;
		$this->link['baseLink'] = implode('/', $base);
		return $d;
	}

	public function get($key)
	{
		if (isset($this->link[$key])){
			return $this->link[$key];
		} elseif (!isset($_GET['page']) || $_GET['page']  == null) {
			return 'homepage';
		}
		return false;
	}

}