<?php

namespace Classes\Helpers;

class Session {

	public static function setSession($name, $value)
	{
		if (isset($name) && isset($value)) {
			if ($_SESSION[$name] = $value)
				return true;
		}
		return false;
	}

	public static function getSession($name)
	{
		if (isset($name)) {
			return (isset($_SESSION[$name]) ? $_SESSION[$name] : false);
		}
		return false;
	}

}
