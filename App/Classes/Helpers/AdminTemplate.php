<?php

namespace Classes\Helpers;

use Classes\Actions\Filter;
use Classes\Db\Data;

class AdminTemplate {

	private static $columns = [];
	private static $actions = [];
	private static $template;
	private static $canShow = false;
	private static $canEdit = false;
	private static $canDelete = false;
	private static $canCreate = false;

	public static function getPage($page)
	{
		if (is_array($page) && count($page) > 0) {
			$templateName = self::createTemplate($page);
			if ($template = self::getTemplate($templateName)) {
				self::$canShow = \WC::component()->users()->isAuthorized($template['canShow']);
				self::$canEdit = \WC::component()->users()->isAuthorized($template['canEdit']);
				self::$canDelete = \WC::component()->users()->isAuthorized($template['canDelete']);
				self::$canCreate = \WC::component()->users()->isAuthorized($template['canCreate']);
				if (isset($template['defaultAction']) && $template['defaultAction'] == 'edit' && self::$canEdit) {
					\WC::app()->redirect(\WC::app()->getFullUrl().'/action=edit/id='.$_SESSION['webId'],false);
				}
				return self::preparePage($template, $page);
			}
		}
		return false;
	}

	private static function preparePage($template, $page)
	{
		self::$template = $template;
		$table = $template['table'];
		$joinedTable = '';
		$where = [];
		if (self::$canShow) {
			$r['title'] = $template['title'];
			if (!$_POST && isset($_SESSION['filter-'.$table]['query']) && $_SESSION['filter-'.$table]['query']['where'] != ''){
				$where[] = str_replace('-','.', $_SESSION['filter-'.$table]['query']['where']);
			}
			if (isset($template['filter'])){
				$f = new Filter($template);
				if (is_array($_POST) && count($_POST) > 0) {
					$filter = $f->searchQuery($_POST);
					if (isset($filter['where']) && $filter['where']){
						$where[] = str_replace('-','.', $filter['where']);
					}
					if (isset($filter['tables']) && $filter['tables']){
						$table = $filter['tables'];
					}
				}

				$r['filter'] = $f->generateForm();
			}

			if (isset($table) && isset($template['columns']) && count($template['columns'])) {
				$limit = isset($template['paging']) ? $template['paging'] : 10;
				$offset = \WC::app()->get('page') && \WC::app()->get('page') > 1 ? (\WC::app()->get('page') - 1) * $limit : 0;

				if (isset($template['cs_web']) && !isset($template['joinedTable']) && $template['cs_web'] == true) {
					$where[] = 'id_cs_web = '.intval($_SESSION['webId']);
				}

				if (isset($template['whereList'])) {
					$where[] = $template['whereList'];
				}

				if (isset($template['joinedTable'])) {
					$t = $template['joinedTable'];
					$joinedTable = ' LEFT JOIN '.$t.' ON '.$t.'.id_'.$template['table'].' = '.$template['table'].'.id';
				}

				$order = isset($template['order']) ? $template['order'] : '';
				$data = new Data($table.$joinedTable);
				$vars['limit'] = $limit;
				$vars['offset'] = $offset;
				$vars['total'] = $data->maxRows([
					'columns' => '*',
					'order' => $order,
					'where' => implode(' AND ', $where),
				]);
				$vars['selected'] = $data->select([
					'columns' => '*',
					'order' => $order,
					'where' => implode(' AND ', $where),
					'limit' => $limit,
					'offset' => $offset
				]);
				$r['template'] = $template;
				$r['page'] = self::prepareTable($template, $vars);
			}
			return $r;
		}
		$processor = new \Classes\Presenters\ErrorPresenter($page);
		echo $processor->process();
	}

	private static function prepareTable($template, $data)
	{
		$drag = isset($template['sortable']) && $template['sortable'] == true ? ' table-draggable' : '';
		$r = '
			'.self::getPagination($data).'	
			<table class="table table-bordered table-hover table-sm '.$drag.'" id="'.$template['table'].'">
				<thead class="thead-brand">
					<tr>
						'.self::getHeader($template['columns'], $template).'
						'.self::getHeadActions($template['actions']).'
					</tr>
				</thead>
				<tbody>
					'.self::getBody($data['selected'], $template).'
				</tbody>
			</table>
		';
		return $r;
	}

	private static function getPagination($data)
	{
		$paging = [];
		$info[] = 'Celkem záznamů: <span>'.$data['total'].'</span>';
		if ($data['total'] > $data['limit']){
			$info[] = 'Vypsány záznamy';
			$maxPages = ceil($data['total']/$data['limit']);
			$currentPage = (\WC::app()->get('page')) ? \WC::app()->get('page') : 1;
			for ($iPage = 1; $iPage <= $maxPages; $iPage++){
				$selected = $currentPage == $iPage ? 'selected' : '';
				$start = $iPage*$data['limit']+1-$data['limit'];
				$end = $start+$data['limit']-1;
				$paging[] = '<a href="'.\WC::app()->getLink().'/page='.$iPage.'"><span class="paging '.$selected.'">'.$start.'-'.$end.'</span></a>';
			}
		}
		return '
			<div class="alert alert-info small alert-filter" data-total="'.$data['total'].'">
				'.implode(' | ', $info).' '.implode('', $paging).'
			</div>	
		';
	}

	private static function getHeader($columns, $template)
	{
		$r = '';
		if (isset($template['sortable']) && $template['sortable'] == true) {
			$r .= '<th style="width: 35px">Pořadí</td>';
		}
		foreach ($columns as $key=>$column){
			if (isset($column['listing']) && $column['listing']){
				self::$columns[] = $key;
				$width = '';
				if ($key == 'id' || (isset($column['type']) && $column['type'] == 'checkbox') || (isset($column['type']) && $column['type'] == 'switcher') || (isset($column['display']) && $column['display'] == 'small')){
					$width = 'style="width: 35px"';
				}
				if (isset($column['display']) && $column['display'] == 'medium') {
					$width = 'style="width: 100px"';
				}

				$r .= '<th '.$width.'>'.$column['title'].'</th>';
			}
		}
		return $r;
	}

	private static function getHeadActions($actions)
	{
		$r = '';
		$width = 'style="width: 25px"';
		foreach ($actions as $action){
			if ($action != 'create') {
				self::$actions[] = $action;
				$r .= '<th ' . $width . '></th>';
			}
		}
		return $r;
	}

	private static function getBody($data, $template)
	{
		$r = '';
		$switcher = [];
		if (is_array($data) && count($data) > 0){
			foreach ($data as $web){
				$td = $tdItems = [];
				$id = 0;
				foreach (self::$columns as $column) {
					if ($column == 'id'){
						$id = $web[$column];
					}
					$td[$column]['data'] = self::format($template['columns'][$column], $column, $web[$column], $id);
					$td[$column]['class'] = isset($template['columns'][$column]['class']) ? $template['columns'][$column]['class'] : '';
				}
				if (count($td) > 0){
					if (isset($template['sortable']) && $template['sortable'] == true) {
						$tdItems[] = '
							<td class="text-center">
								<i class="fa fa-fw fad fa-sort text-success" style="cursor: move;"></i>
								<input type="hidden" value="'.$id.'" id="item" name="item" data-name="'.$id.'">
							</td>
						';
					}
					foreach ($td as $row){
						$class = isset($row['class']) ? $row['class'] : '';
						$tdItems[] = "<td class=".$class.">".$row['data']."</td>";
					}
					$rowClass = '';
					if (isset($_SESSION['highlighted']) && $_SESSION['highlighted'] == $id) {
						$rowClass .= 'highlighted';
						unset($_SESSION['highlighted']);
					}
					$r .= '
						<tr id="'.$id.'" class="'.$rowClass.'">
							'.implode('', $tdItems).'
							'.self::getActions(self::$actions, $web, $switcher).'
						</tr>
					';
				}
			}
		} else {
			$r .= '
				<tr>
					<td colspan="100">Neobsahuje žádná data...</td>
				</tr>
			';
		}
		return $r;
	}

	private static function format($column, $key, $value = null, $id = null)
	{
		$return = $value;
		preg_match("/^id_(.*)/", $key, $result);
		if (is_array($result) && count($result) == 2) {
			$table = $result[1];
			$data = new Data($table);
			$select = $data->selectSingle([
				'columns' => '*',
				'where' => 'id = ' . intval($value)
			]);
			if (isset($column['joined'])) {
				$joined = explode('|', $column['joined']);
				foreach ($joined as $item) {
					$name[] = $select[$item];
				}
				$ret = implode(', ', $name);
			} else {
				$ret = $select['name'];
			}

			$return = $ret;
		} elseif (isset($column['type']) && $column['type'] == 'date'){
			$return = '';
			if ($value && $value != '0000-00-00') {
				$return = date('j. n. Y', strtotime($value));
			}
		} elseif (isset($column['type']) && $column['type'] == 'datetime'){
			$return = '';
			if ($value && $value != '0000-00-00 00:00:00') {
				$return = date('j. n. Y', strtotime($value)).'<br><span class="time-preview">('.date('H:i', strtotime($value)).')</span>';
			}
		}  elseif (isset($column['type']) && $column['type'] == 'colorpicker'){
			$return = '<span class="color-preview" style="background: #'.$value.'"></span>';
		} elseif (isset($column['type']) && $column['type'] == 'trueFalse'){
			$switch = $value == 1 ? 'home text-success' : 'home text-notactive';
			$return = '
				<i class="fa fas fa-'.$switch.'">
			';
		}  elseif (isset($column['type']) && $column['type'] == 'switcher'){
			$web = '';
			if (isset(self::$template['cs_web']) && self::$template['cs_web']) {
				$web = '/web=true';
			}
			$switch = $value == 1 ? 'toggle-on text-success' : 'toggle-off text-notactive';
			$return = '
				<a href="" class="switcher '.$switch.'" rel="'.$id.'" page="' .\WC::app()->getLink().'/action=switcher/column='.$key. '/item=csws_'.self::$template['table'].'/id='.$id.$web. '"></a>
			';
		} elseif (isset($column['type']) && $column['type'] == 'image'){
			$d = new Data(self::$template['table']);
			$s = $d->selectSingle(['columns' => '*', 'where' => 'id = '.intval($id)]);
			$caption = isset($s['popis']) ? $s['popis'] : $s['name'];
			$return = '<a href="'.$value.'" data-fancybox="gallery" data-caption="'.$caption.'"><img src="'.$value.'" class="image-preview"></a>';
		} elseif (isset($column['type']) && $column['type'] == 'sort'){
			$return = '
					<a href="" class="moveDown" rel="'.$id.'" page="' . \WC::app()->getFullUrl() . '/id='.$id.'/item='.self::$template['table'].'"><i class="fa fas fa-arrow-square-up text-success"></i></a>
					<a href="" class="moveUp" rel="'.$id.'" page="' . \WC::app()->getFullUrl() . '/id='.$id.'/item='.self::$template['table'].'"><i class="fa fas fa-arrow-square-down text-success"></i></a>
				';
		}
		return $return;
	}

	private static function getActions($actions, $data = null, $switcher = null)
	{
		$r = '';
		foreach ($actions as $action){
			if ($action != 'create') {
				if (is_array($data) && count($data) > 0) {
					if ($action == 'edit'){
						if (self::$canEdit) {
							$r .= '<td class="text-center"><a href="' . \WC::app()->getLink() . '/action=edit/id=' . $data["id"] . '"><i class="fa far fa-edit text-info"></i> </a></td>';
						} else {
							$r .= '<td class="text-center"><i class="fa far fa-edit text-notactive"></i></td>';
						}
					}
					if ($action == 'delete') {
						if (self::$canDelete) {
							$r .= '<td class="text-center"><a href="" class="delete_link" page="' . \WC::app()->getLink() . '" rel="' . $data["id"] . '"><i class="fa far fa-times-circle text-danger"></i> </a></td>';
						} else {
							$r .= '<td class="text-center"><i class="fa far fa-times-circle text-notactive"></i></td>';
						}
					}
				} else {
					$r .= '<td></td>';
				}
			}
		}
		return $r;
	}

	public static function isAuthorized($data)
	{
		if ($data == null || \WC::component()->users()->isAuthorized('superAdmin')) {
			return true;
		} elseif (isset($data[' id_cs_user'])) {
			$userId = $_SESSION['userId'];
			$author = $data[' id_cs_user'];
			return $userId == $author ? true : false;
		} else {
//			if (isset($data['id_web']) && $data['id_web'] == self::$webId){
//				return true;
//			}
		}
		return false;
	}

	public static function getTemplate($templateName)
	{
		$fileName = \Dir::adminTemplate().'/'.$templateName.'.php';
		if (is_file($fileName)) {
			return include $fileName;
		}
		return false;
	}

	private static function createTemplate($page)
	{
		return implode('-', $page);
	}


}
