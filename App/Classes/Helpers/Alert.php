<?php

namespace Classes\Helpers;

class Alert
{
	public function error($message)
	{
		if (is_array($message)) {
			foreach ($message as $msg) {
				$_SESSION['msg']['error'][] = $msg;
			}
		} else {
			$_SESSION['msg']['error'][] = $message;
		}
	}

	public function success($message)
	{
		if (is_array($message)) {
			foreach ($message as $msg) {
				$_SESSION['msg']['success'][] = $msg;
			}
		} else {
			$_SESSION['msg']['success'][] = $message;
		}
	}

	public function successCookie($message)
	{
		setcookie("csAdmin--alert-success", json_encode([$message]), time()+120,'/');
	}

	public function prepare($message, $type = 'success')
	{
		if (is_array($message)) {
			foreach ($message as $msg) {
				$_SESSION['msg'][$type][] = $msg;
			}
		} else {
			$_SESSION['msg'][$type][] = $message;
		}
	}

	public static function render()
	{
		if (isset($_SESSION['msg'])) {
			$r = '';
			$types = [
				'success',
				'error',
				'warning',
				'info'
			];
			foreach ($types as $type) {
				if (isset($_SESSION['msg'][$type]) && count($_SESSION['msg'][$type]) > 0) {
					foreach ($_SESSION['msg'][$type] as $msg) {
						$r .= '
							var msg = "'.$msg.'";
							var type = "'.$type.'";
							$.toast({
								icon: type,
								text: msg,
								hideAfter: 10000,
								showHideTransition: "fade",
								position: "top-center",
								allowToastClose: true,
							});
						';
					}
				}
			}
			unset($_SESSION['msg']);
			if ($r != '') {
				return '
					<script type="text/JavaScript">
						'.$r.'
					</script>
				';
			}
		}
		return '';
	}

}
