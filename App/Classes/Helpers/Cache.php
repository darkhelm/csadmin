<?php

namespace Classes\Helpers;

class Cache {

	public static function cache ($filename, $expiration, $exit = false)
	{
		// define the path and name of cached file
		$cachefile = 'temp/' . $filename . '.php';
		// define how long we want to keep the file in seconds. I set mine to 5 hours.
		$cachetime = $expiration;
		// Check if the cached file is still fresh. If it is, serve it up and exit.
		if (file_exists($cachefile) && time() - $cachetime < filemtime($cachefile)) {
			include($cachefile);
			if ($exit){
				exit;
			}
			return false;
		}
		return true;
	}

	public static function saveCache($filename)
	{
		$cachefile = 'temp/' . $filename . '.php';
		$fp = fopen($cachefile, 'w');
		fwrite($fp, ob_get_contents());
		fclose($fp);
		// finally send browser output
		ob_end_flush();
	}
}
