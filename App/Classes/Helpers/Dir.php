<?php
class Dir {
	public static function root () {
		return realpath(__DIR__ . '/../../..');
	}

	public static function app () {
		return self::root() . '/App';
	}

	public static function www () {
		return self::root() . '/public';
	}

	public static function resources () {
		return self::root() . '/resources';
	}

	public static function vendor () {
		return self::root() . '/vendor';
	}

	public static function temp () {
		return self::root() . '/temp';
	}

	public static function logs () {
		return self::root() . '/logs';
	}

	public static function config () {
		return self::app() . '/Config';
	}

	public static function statsapi () {
		return self::app() . '/Data/statsapi';
	}

	public static function log () {
		return self::root() . '/log';
	}

	public static function template () {
		return self::app() . '/Templates';
	}

	public static function adminTemplate () {
		return self::app() . '/Admin';
	}

}
