<?php

namespace Classes\Helpers;

use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

class MyLogger
{

	/**
	 * @var Logger
	 */
	private $logger;

	public function __construct()
	{
		$this->logger = new Logger('CSAdmin');
		$stream = new RotatingFileHandler(\Dir::logs().'/csadmin.log', 0,Logger::INFO);
		$stream->setFormatter(new JsonFormatter());
		$this->logger->pushHandler($stream);

	}

	public function info($message, $variables = [])
	{
		$this->logger->info($message, $variables);
	}

	public function error($message, $variables = [])
	{
		$this->logger->error($message, $variables);
	}

	public function warning($message, $variables = [])
	{
		$this->logger->warning($message, $variables);
	}

}
