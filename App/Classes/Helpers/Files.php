<?php

namespace Classes\Helpers;

use Classes\Db\Data;
use Classes\Formatters\Strings;
use Symfony\Component\Filesystem\Filesystem;

class Files extends \Classes\Actions\Files
{

	public function upload($files, $id, $post = null, $parent = null)
	{
		$fileSystem = new Filesystem();
		foreach ($files as $table=>$file) {
			if ($parent) {
				$dataFile = new Data($table);
			}
			if (is_array($file['name'])) {
				foreach ($file['name'] as $key=>$filename) {
					if ($filename != '') {
						$name = $file['name'][$key];
						$tmpname = $file['tmp_name'][$key];
						$type = $file['type'][$key];
						$error = $file['error'][$key];
						$size = $file['size'][$key];
						$fileInfo = $post ? $post['filename-'.$table][$key] : '';

						if ($name != '' && $size > 0 && $error == 0) {
							$insFile = [];
							$fileName = $id.'_'.strtolower(basename($name));
							$newFileName = Strings::generateFileName($fileName,$id,$fileName);
							$target_file = \Dir::resources().'/'.$table.'/'.$newFileName;
							if (!$fileSystem->exists(\Dir::resources().'/'.$table)) {
								$fileSystem->mkdir(\Dir::resources().'/'.$table);
							}
							if (move_uploaded_file($tmpname, $target_file)) {
								if ($parent) {
									$insFile = [
										'id_'.$parent => intval($id),
										'filename' => '/resources/'.$table.'/'.$newFileName,
										'name' => $fileInfo,
										'id_cs_user' => \WC::component()->userSession()->userId,
									];
									if ($newIdFile = $dataFile->insert(['column' => [$insFile]])) {
										\WC::component()->logger()->info('FILE: ', [
											'page' => \WC::app()->get('baseLink'),
											'table' => $table,
											'user' => \WC::component()->userSession()->user->user['email'],
											'data' => $insFile
										]);
									}
								}
								return $target_file;
							}
						}
					}
				}
			} else {
				$name = $file['name'];
				$tmpname = $file['tmp_name'];
				$type = $file['type'];
				$error = $file['error'];
				$size = $file['size'];
				$fileInfo = $post ? $post['filename-'.$table] : '';

				if ($name != '' && $size > 0 && $error == 0) {
					$insFile = [];
					$fileName = strtolower(basename($name));
					$newFileName = Strings::generateFileName($fileName,$id,$fileName);
					$target_file = \Dir::resources().'/'.$table.'/'.$newFileName;
					if (!$fileSystem->exists(\Dir::resources().'/'.$table)) {
						$fileSystem->mkdir(\Dir::resources().'/'.$table);
					}
					if (move_uploaded_file($tmpname, $target_file)) {
						if ($parent) {
							$insFile = [
								'id_'.$parent => intval($id),
								'filename' => '/resources/'.$table.'/'.$newFileName,
								'name' => $fileInfo,
								'id_cs_user' => \WC::component()->userSession()->userId,
							];
							if ($newIdFile = $dataFile->insert(['column' => [$insFile]])) {
								\WC::component()->logger()->info('FILE: ', [
									'page' => \WC::app()->get('baseLink'),
									'table' => $table,
									'user' => \WC::component()->userSession()->user->user['email'],
									'data' => $insFile
								]);
							}
						}
						return [
							'link' => 'resources/'.$table.'/'.$newFileName,
							'newName' => $newFileName,
							'name' => $name,
							'size' => $size,
						];
					}
				}
			}
		}
		return false;
	}

	public function getFiles($table, $parentTable, $idParent)
	{
		$dataFile = new Data($table);
		$listFiles = '<div class="alert alert-warning alert-icon-info" role="alert">Žádné soubory...</div>';
		if ($id = $idParent) {
			if ($files = $dataFile->select(['columns' => '*', 'where' => 'id_' . $parentTable . ' = ' . $id])) {
				$listFiles = '<table class="table-files table-admin table-sm">';
				foreach ($files as $file) {
					$path_parts = pathinfo($file['filename']);
					$icon = '';
					if ($path_parts['extension'] == 'pdf') {
						$icon = 'file-pdf text-danger';
					} elseif ($path_parts['extension'] == 'doc' || $path_parts['extension'] == 'docx') {
						$icon = 'file-word';
					} elseif ($path_parts['extension'] == 'msg') {
						$icon = 'envelope text-info';
					} elseif ($path_parts['extension'] == 'xls' || $path_parts['extension'] == 'xlsx' || $path_parts['extension'] == 'csv') {
						$icon = 'file-excel text-success';
					} elseif ($path_parts['extension'] == 'jpg' || $path_parts['extension'] == 'jpeg' || $path_parts['extension'] == 'png' || $path_parts['extension'] == 'gif') {
						$icon = 'file-image text-secondary';
					}
					$listFiles .= '
							<tr class="files">
								<td style="width: 25px" class="text-center"><a href="' . $file['filename'] . '" title="' . $file['filename'] . '" target="_blank"><i class="fa fa-fw fa-2x fad fa-' . $icon . '"></i></a></td>
								<td class="filename">'.$file['name'].'</td>
								<td class="filename">' . $path_parts['basename'] . '</td>
								<td style="width: 25px" class="text-center"><a href="" class="delete_link_file" data-fileurl="' .$file['filename'] . '" data-filename="' . $path_parts['basename'] . '" data-table="' . $table . '" data-file="' . $file["id"] . '"><i class="fa far fa-times-circle text-danger"></i> </a></td>
							</tr>
						';
				}
				$listFiles .= '</table>';
			}
		}
		return $listFiles;
	}

}
