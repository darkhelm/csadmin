<?php

namespace Classes\Helpers;

use Joseki\Application\Responses\PdfResponse;
use Symfony\Component\Filesystem\Filesystem;

class PdfHelper
{

	public static function save($folder, $fileName, $content, $footer = [], $header = [], $title = 'PDF', $settings = [])
	{
		$pdf = new PdfResponse($content);
		$pdf->setSaveMode(PdfResponse::DOWNLOAD);
		$pdf->pageFormat = "A4-P";
		$pdf->setDocumentTitle($title);
		if (isset($settings['margins'])) {
			$pdf->setPageMargins($settings['margins']);
		}
		$mpdf = $pdf->getMPDF();
		$mpdf->setFooter($footer,''); // footer
		$mpdf->setHeader($header,''); // header
		$mpdf->WriteHTML(file_get_contents(\Dir::www().'/css/screen.min.css'), 1);

		$fileSystem = new Filesystem();
		if (!$fileSystem->exists(\Dir::resources().'/'.$folder.'/')) {
			$fileSystem->mkdir(\Dir::resources().'/'.$folder);
		}
		return $pdf->save(\Dir::resources().'/'.$folder,$fileName);
	}
}
