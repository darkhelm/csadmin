<?php

namespace Classes\Helpers;

class ActionProcessor {

	protected $class = null;

	public static function process($link)
	{
		$actionClassName = '\\Classes\\Actions\\'.ucfirst($link);
		if (!class_exists($actionClassName)) {
			$actionClassName = '\\Classes\\Actions\\DefaultAction';
		}

		return $actionClassName;
	}

	public static function action($link)
	{
		$actionClassName = '\\Classes\\Actions\\'.ucfirst($link);
		if (!class_exists($actionClassName)) {
			$actionClassName = false;
		}

		return $actionClassName;
	}

}
