<?php

namespace Classes\Helpers;

class Template {

	public static function render($templateName, $variables = [])
	{
		if ($template = self::getTemplate($templateName)) {
			if ($variables != null && is_array($variables)) {
				foreach ($variables as $key => $value) {
					${$key} = $value;
				}
			}
			ob_start();
			include $template;
			return ob_get_clean();
		}
		return null;
	}

	public static function getTemplate($templateName)
	{
		$fileName = \Dir::template().'/'.$templateName.'.php';
		if (is_file($fileName)) {
			return $fileName;
		}
		return false;
	}


}
