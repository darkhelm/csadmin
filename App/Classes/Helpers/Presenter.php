<?php

namespace Classes\Helpers;

use Classes\Handlers\User;

class Presenter {

	protected $folder = null;
	protected $link;
	protected $session = null;

	/**
	 * @var User
	 */
	protected $user = null;
	protected $seasonActual;
	protected $limit = 25;

	public $class;

	public function __construct($link, $session = null, $user = null)
	{
		$this->link = $link;
		$this->session = $session;
		$this->user = $user;
	}

	public function process()
	{
		return Template::render($this->folder.'default');
	}

	protected function pagination($target, $count, $limit, $category = null, $currentPage = null)
	{
		if ($category != '') { $category = '/'.$category; } else { $category = '';}
		$pages = ceil($count/$limit);
		$selected = '';
		if ($currentPage == '') { $currentPage = 1;}
		$pp = $currentPage - 1;
		$np = $currentPage + 1;
		$previous = '<li class="previous"><a href="/'.$target.$category.'/page='.$pp.'"><i class="fas fa-chevron-double-left"></i></a></li>';
		$next = '<li class="previous"><a href="/'.$target.$category.'/page='.$np.'"><i class="fas fa-chevron-double-right"></i></a></li>';
		if ($currentPage == 1) {
			$selected = 'selected';
			$previous = '';
		}
		$r = '
			<div class="pagination-wrapper">
				<ul class="article-pagination">
				    '.$previous.'
				    <li class="first '.$selected.'"><a href="/'.$target.$category.'">1</a></li>
	    ';

		for ($page = $currentPage-3; $page < $currentPage; $page++){
			if ($page > 1) {
				$r .= '
					<li><a href="/'.$target.$category . '/page=' . $page . '">' . $page . '</a></li>
				';
			}
		}
		if ($currentPage > 1 && $currentPage < $pages) {
			$r .= '
				<li class="selected"><a href="/'.$target.$category . '/page=' . $currentPage . '">' . $currentPage . '</a></li>
			';
		}
		for ($page = $currentPage+1; $page <= $currentPage+3; $page++){
			if ($page < $pages) {
				$r .= '
					<li><a href="/'.$target.$category . '/page=' . $page . '">' . $page . '</a></li>
				';
			}
		}

		if ($pages > 1) {
			$selected = '';
			if ($currentPage == $pages) {
				$selected = 'selected';
				$next = '';
			}
			$r .= '
					<li class="last '.$selected.'"><a href="/'.$target.$category . '/page=' . $pages . '">' . $pages . '</a></li>
					'.$next.'
				</ul>
			</div>
		';
		}
		return $r;
	}
}
