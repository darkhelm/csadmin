<?php

namespace Classes\Helpers;

class Functions {

	public static function redirect ($link) {
		header ("Location: ".$link);
		exit;
	}

	public static function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}

	public static function sortBySubValue($array, $value, $asc = true, $preserveKeys = false)
	{
		if ($preserveKeys) {
			$c = array();
			if (is_object(reset($array))) {
				foreach ($array as $k => $v) {
					$b[$k] = strtolower($v->$value);
				}
			} else {
				foreach ($array as $k => $v) {
					$b[$k] = strtolower($v[$value]);
				}
			}
			$asc ? asort($b) : arsort($b);
			foreach ($b as $k => $v) {
				$c[$k] = $array[$k];
			}
			$array = $c;
		} else {
			if (is_object(reset($array))) {
				usort($array, function ($a, $b) use ($value, $asc) {
					return $a->{$value} == $b->{$value} ? 0 : ($a->{$value} - $b->{$value}) * ($asc ? 1 : -1);
				});
			} else {
				usort($array, function ($a, $b) use ($value, $asc) {
					return $a[$value] == $b[$value] ? 0 : ($a[$value] - $b[$value]) * ($asc ? 1 : -1);
				});
			}
		}

		return $array;
	}

	public static function compareByLastName($a, $b) {
		return strcmp($a["lastName"], $b["lastName"]);
	}


	public static function reload()
	{
		$r = "<meta http-equiv='refresh' content='0'>";
		return $r;
	}

//	public static function redirect($URL)
//	{
//		$r = "<script type='text/javascript'>document.location.href='{$URL}';</script>";
//		$r .= '<META HTTP-EQUIV="refresh" content="0;URL=' . $URL . '">';
//		return $r;
//	}

	public static function redirectHeader($location)
	{
		return header("Location: {$location}");
	}

}
