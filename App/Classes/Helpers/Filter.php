<?php

namespace Classes\Helpers;

use Classes\Db\Data;

class Filter
{

	private $queryData = [];
	private $query = [];
	private $sessionName = '';

	public function render($data)
	{
		$r = '';
		if ($data){
			$this->sessionName = $data['session'];
			$query = $this->getQuery($this->sessionName);
			$r = '
				<form action="'.\WC::app()->getFullUrl().'" method="post" class="filter">
					<h2>Filtr</h2>
					<p>
			';
				foreach ($data['setColumns'] as $v=>$c) {
					$v = $this->sessionName.'-'.$v;
					$r .= '<span><strong>' . $c["name"] . ': </strong>' . $this->formatEdit($c, $v, (isset($query[$v]) ? $query[$v] : null)) . '</span>'; // (isset($query[$c["key"]]) ? $query[$c["key"]] : null)
				}

				$r .= '
						<input name="filterList" value="1" type="hidden" />
						<span><input type="submit" value="Filtrovat" class="btn btn-xsm btn-filter" /></span>
					</p>
				</form>
			';
			}

		return $r;
	}

	public function setFilter($session, $data)
	{
		$return = [];
		if (isset($data['filterList'])){
			unset($_SESSION['filterRenderer-'.$session]);
			foreach ($data as $key=>$value){
				if ($key != 'filterList' && $value != '') {
					$key = str_replace('filter-','', $key);
					$parts = explode('|', $key);
					$_SESSION['filterRenderer-' . $session][] = [$parts[0] => $value];
					if ($parts[1] == '=') {
						$return[] = $parts[0] . ' = ' . intval($value);
					} elseif ($parts[1] == 'like') {
						$return[] = $parts[0] . ' LIKE "%' . $value . '%"';
					}
				}
			}
			$_SESSION['filterRenderer-'.$session]['query'] = implode(' AND ', $return);
		}
	}

	public function prepareWhere($session, $where = '')
	{
		$return = [];
		if ($where) {
			$return[] = $where;
		}
		if ($query = $this->getQueryString($session)) {
			$return[] = $query;
		}
//		if ($data) {
//			foreach ($data as $key => $value) {
//				if ($value != '') {
//					$p = explode('-', $key);
//					if (isset($p[1])) {
//						if ($key == 'id') {
//							$return[] = $p[1] . ' = ' . intval($value);
//						} else {
//							$return[] = $p[1] . ' LIKE "%' . $value . '%"';
//						}
//					} else {
//						if ($key == 'id') {
//							$return[] = $key . ' = ' . intval($value);
//						} else {
//							$return[] = $key . ' LIKE "%' . $value . '%"';
//						}
//					}
//				}
//			}
//		}
		if (count($return) > 0) {
			return implode(' AND ', $return);
		}
		return null;
	}

	public function getPagination($dataSet, $where = null, $order = null)
	{
		$paging = [];
		$data = new Data($dataSet['table']);
		$total = $data->maxRows([
			'columns' => '*',
			'where' => $where,
		]);

		$info[] = 'Celkem záznamů: <span>'.$total.'</span>';
		if ($total > $dataSet['limit']){
			$info[] = 'Vypsány záznamy';
			$maxPages = ceil($total/$dataSet['limit']);
			$currentPage = (\WC::app()->get('page')) ? \WC::app()->get('page') : 1;
			for ($iPage = 1; $iPage <= $maxPages; $iPage++){
				$selected = $currentPage == $iPage ? 'selected' : '';
				$start = $iPage*$dataSet['limit']+1-$dataSet['limit'];
				$end = $start+$dataSet['limit']-1;
				$paging[] = '<a href="'.\WC::app()->getLink().'/page='.$iPage.'"><span class="paging '.$selected.'">'.$start.'-'.$end.'</span></a>';
			}
		}
		return '
			<div class="alert alert-info small alert-filter" data-total="'.$total.'">
				'.implode(' | ', $info).' '.implode('', $paging).'
			</div>	
		';
	}

	private function formatEdit ($column, $key, $value = NULL, $id = NULL) {
		$search = '|like';
		if (isset($column['search'])) {
			$search = '|'.$column['search'];
		}
		if ($column['type'] == 'input' || $column['type'] == 'paragraph' || $column['type'] == 'paragraphSimple'){
			return '<input name="filter-'.$key.$search.'" value="'.$value.'">';
		} elseif ($column['type'] == 'select'){
			if (isset($column['table'])) {
				$table = $column['table'];
			} else {
				$keyMatch = explode('-', $key);
				preg_match("/^id_(.*)/", $keyMatch[1], $result);
				if (is_array($result) && count($result) == 2) {
					$table = $result[1];
				}
			}
			if ($o = $this->getOptions($column, $table, $value)) {
				return '
					<select name="filter-' . $key .$search. '" class="">
						' . $o . '
					</select>
				';
			}
		} elseif ($column['type'] == 'checkbox' || $column['type'] == 'switcher' || $column['type'] == 'trueFalse'){
			$o = '<option value="" '.($value == '' || !$value ? 'selected' : '').'>--- vše ---</option>';
			$o .= '<option value="0" '.($value == '0' || ($value == '0' && $column['selected'] == 0) ? 'selected' : '').'>Ne</option>';
			$o .= '<option value="1" '.($value == '1' || ($value == '1' && $column['selected'] == 1) ? 'selected' : '').'>Ano</option>';

			return '
				<select name=' . $key .$search. ' class="">
				'.$o.'
				</select>
			';
		}
	}

	private function getQuery($session)
	{
		$r = null;
		if (isset($_SESSION['filterRenderer-'.$session])){
			foreach ($_SESSION['filterRenderer-'.$session] as $key=>$items){
				if ($key !== 'query'){
					foreach ($items as $k=>$v){
						if ($k != 'query') {
							$r[$k] = $v;
						}
					}

				}
			}
		}
		return $r;
	}

	private function getQueryString($session)
	{
		if (isset($_SESSION['filterRenderer-'.$session]['query'])){
			return str_replace($session.'-','', $_SESSION['filterRenderer-'.$session]['query']);
		}
		return null;
	}


	private function getOptions($column, $table, $value = null)
	{
		$option = false;
		$where = isset($column['where']) ? $column['where'] : '';
		$order = isset($column['order']) ? $column['order'] : '';

		$data = new Data($table);
		if ($select = $data->select([
			'columns' => '*',
			'order' => $order,
			'where' => $where
		])){
			$option .= '<option value="">--- vybrat ---</option>';
			foreach ($select as $sel) {
				$names = [];
				$selected = '';
				if ($value && $sel['id'] == $value) {
					$selected = 'selected';
				}
				if (isset($column['joined'])) {
					$joined = explode('|', $column['joined']);
					foreach ($joined as $item) {
						$names[] = $sel[$item];
					}
					$name = implode(', ', $names);
				} else {
					$name = $sel['name'];
				}
				$option .= '<option value="' . $sel['id'] . '" ' . $selected . '>' . $name . '</option>';
			}
		}
		return $option;
	}

}
