<?php

namespace Classes\Db;

class Database
{
	public $connection;

	function __construct()
	{
		$this->open_db_connection();
	}

	public function open_db_connection()
	{
//		$this->connection = new \mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		$this->connection = new \Dzegarra\TracyMysqli\Mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		if ($this->connection->connect_errno) {
			die("Nepodařilo se spojit s databází... " . $this->connection->connect_error);
		}

	}

	public function query($sql)
	{
		$result = $this->connection->query($sql);
		$this->confirm_query($result);

		return $result;
	}

	public function confirm_query($result)
	{
		if (!$result) {
			die("DB Query se nepodařilo... " . $this->connection->connect_error);
		}
	}

	public function escape_string($string)
	{
		$escaped_string = $this->connection->real_escape_string($string);

		return $escaped_string;
	}

	public function the_insert_id()
	{
		return mysqli_insert_id($this->connection);
	}

	public function escape($string)
	{
		return mysqli_real_escape_string($this->connection, $string);
	}

	public function fetch ($result) {
		return mysqli_fetch_array($result);
	}


}


