<?php

namespace Classes\Db;

class Data
{

	private $table = null;
	private $db;
	private $id = "*";

	function __construct ($var=null)
	{
		global $db;
		$this->db = $db;
		if (!is_array($var)) {
			if (isset($var) && $var != "")
				$this->table = $var;
			else
				$this->error('Proměnná předávaná třídě Data musí být pole!');
		} else {
			if (!isset($var["table"]) || $var["table"] == '') $this->error('Tabulka musí být definována!');
			$this->table = $var["table"];
		}
	}

	public function selectSingle ($var=null) {
		$var["limit"] = 1;
		if ($select = $this->select($var))
			return $select[0];
		else
			return false;
	}

	public function countRows ($var=null) {
		if ($select = $this->select($var))
			return count($select);
		else
			return false;
	}

	public function select ($var=null) {
		global $db;

		$where = $types = $order = $group = $columns = $return = $limit = $offset = null;
		$setnames = 'utf8';
		if (isset($var["setnames"]) && is_string($var["setnames"]) && $var["setnames"] !='') $setnames = $var["setnames"];
		if (isset($var["where"]) && is_string($var["where"]) && $var["where"] !='') $where = "WHERE ".$var["where"];
		if (isset($var["order"]) && is_string($var["order"]) && $var["order"] !='') $order = "ORDER BY ".$var["order"];
		if (isset($var["group"]) && is_string($var["group"]) && $var["group"] !='') $group = "GROUP BY ".$var["group"];
		if (isset($var["limit"]) && $var["limit"]!='') $limit = "LIMIT ".$var["limit"];
		if (isset($var["offset"]) && $var["offset"]!='') $offset = "OFFSET ".$var["offset"];
		if (is_array($var["columns"]))
			foreach ($var["columns"] as $columnName) {
				if ($columnName != "")
					$columns .= ($columns != "" ? ", " : "").$columnName;
			}
		elseif ($var["columns"] != "")
			$columns = $var["columns"];
		else
			$columns = $this->id;

		if ($setnames != '') {
			$db->query('SET CHARACTER SET '.$setnames);
			$db->query('SET NAMES '.$setnames);
		}
		$query = $db->query("
			SELECT
				".$columns."
			FROM
				".$this->table."
			".$where."
            ".$group."
			".$order."
			".$limit."
			".$offset."
		");

		while ($data = $db->fetch($query)) {
			$return[] = $data;
		}
		return $return;
	}

	public function insert ($var=null) {
		global $db;

		$val = $col = $returnId = null;
		$setnames = 'utf8';
		if (!is_array($var)) $this->error('Proměnná předávaná funkci x_data.insert musí být pole!');
		if ((!isset($var["column"]) && !is_array($var["column"]) || count($var["column"]) == 0) && (!isset($var["columns"]) && !is_array($var["columns"]) || count($var["columns"]) == 0)) $this->error('Nebyly zadány žádné sloupce k vykonání x_data.insert!');
		if ((!isset($var['column']) || $var["column"] == null) && is_array($var["columns"]))
			$var["column"] = $var["columns"];

		foreach ($var["column"][0] as $key => $name) {
			if ($col != "") { $col .= ", "; $val .= ", ";}
			$col .= $key;
			if ($name === null)
				$val .= "null";
			else
				$val .= "'".$name."'";

			if ($key == 'id') {
				$returnId = intval($name);
			}
		}
		if ($setnames != '') {
			$db->query('SET CHARACTER SET '.$setnames);
			$db->query('SET NAMES '.$setnames);
		}
		if ($col != null) {
			if ($db->query("
				INSERT INTO
					".$this->table." (".$col.")
				VALUES
					(".$val.")
			")) {
				if ($returnId) {
					return $returnId;
				}
				return $db->the_insert_id();
			}
		}
		return false;
	}

	public function update($set, $var)
	{
		global $db;

		$where = null;
		$setnames = 'utf8';
		if (isset($var["setnames"]) && is_string($var["setnames"]) && $var["setnames"] !='') $setnames = $var["setnames"];
		if (isset($var["where"]) && is_string($var["where"]) && $var["where"] !='') $where = $var["where"];

		if (is_string($set)){
			$updates = $set;
		} elseif (is_array($set)){
			$variables = [];
			foreach ($set as $dbKey=>$value){
				$variables[] = $dbKey.'="'.$db->escape($value).'" ';
			}
			$updates = implode(',', $variables);
		} else {
			return false;
		}

		if ($setnames != '') {
			$db->query('SET CHARACTER SET '.$setnames);
			$db->query('SET NAMES '.$setnames);
		}
		return $db->query("
			UPDATE ".$this->table."
			SET ".$updates."
			WHERE ".$where."
		");

	}

	public function delete($var)
	{
		global $db;

		$where = 'id = '.intval($var);

		return $db->query("
			DELETE FROM ".$this->table."
			WHERE ".$where."
		");
	}

	public function deleteWhere($where)
	{
		global $db;

		return $db->query("
			DELETE FROM ".$this->table."
			WHERE ".$where."
		");
	}

	public function truncate()
	{
		global $db;
		return $db->query("TRUNCATE TABLE {$this->table}");
	}

	public function maxRows ($var=null) {
		return mysqli_num_rows($this->selectQuery($var));
	}

	private function selectQuery($var=null)
	{
		global $db;

		$where = $types = $order = $group = $columns = $return = $limit = $offset = null;
		$setnames = '';
		if (isset($var["setnames"]) && is_string($var["setnames"]) && $var["setnames"] !='') $setnames = $var["setnames"];
		if (isset($var["where"]) && is_string($var["where"]) && $var["where"] !='') $where = "WHERE ".$var["where"];
		if (isset($var["order"]) && is_string($var["order"]) && $var["order"] !='') $order = "ORDER BY ".$var["order"];
		if (isset($var["group"]) && is_string($var["group"]) && $var["group"] !='') $group = "GROUP BY ".$var["group"];
		if (isset($var["limit"]) && $var["limit"]!='') $limit = "LIMIT ".$var["limit"];
		if (isset($var["offset"]) && $var["offset"]!='') $offset = "OFFSET ".$var["offset"];
		if (is_array($var["columns"]))
			foreach ($var["columns"] as $columnName) {
				if ($columnName != "")
					$columns .= ($columns != "" ? ", " : "").$columnName;
			}
		elseif ($var["columns"] != "")
			$columns = $var["columns"];
		else
			$columns = $this->id;

		if ($setnames != ''){
			$db->query('SET CHARACTER SET '.$setnames);
			$db->query('SET NAMES '.$setnames);
		}

		$query = $db->query("
			SELECT
				".$columns."
			FROM
				".$this->table."
			".$where."
            ".$group."
			".$order."
			".$limit."
			".$offset."
		");

		return $query;
	}


	private function error ($string) {
		trigger_error('<span style="text-decoration:underline">Data</span>: '.$string, 256);
	}

	public function escape($string)
	{
		return mysqli_real_escape_string($this->db->connection, $string);
	}
}
