<?php

namespace Classes\Modules\User;

use Classes\Db\Data;
use Classes\Formatters\Strings;
use WC;

class UserSession
{
	private $signedIn = false;
	public $userId;
	public $webId;
	public $user = null;

	function __construct()
	{
		WC::init();
		$this->checkLogin();
	}

	public function isSignedIn()
	{
		return $this->signedIn;
	}

	public function getUser()
	{
		if ($this->user) {
			return $this->user;
		}
		if (isset($_SESSION['userId'])) {
			return WC::component()->users()->getUser($_SESSION['userId']);
		}
		return false;
	}

	public function login(\Classes\Modules\User\User $user)
	{
		if ($user) {
			$this->webId = $_SESSION['webId'] = WC::component()->web()->getUserCurrentWeb($user->getId())->getId();
			$this->userId = $_SESSION['userId'] = $user->getId();
			$this->signedIn = true;
			$this->user = $user;

			$data = new Data('cs_user_login');
			$insert = [
				'id_cs_user' => $user->getId(),
				'ip' => Strings::getUserIP(),
				'id_cs_web' => $this->webId,
			];
			$data->insert(['column' => [$insert]]);
			WC::component()->logger()->info('LOGIN', ['user' => $user->getAttr('email'), 'data' => $insert]);
		}
	}

	public function logout()
	{
		unset($this->userId);
		unset($this->$_SESSION['userId']);
		unset($this->$_SESSION['webId']);
		$this->signedIn = false;
	}

	private function checkLogin()
	{
		if(isset($_SESSION['userId']) && isset($_SESSION['webId'])) {
			$this->webId = $_SESSION['webId'];
			$this->userId = $_SESSION['userId'];
			$this->signedIn = true;
		}
	}

}
