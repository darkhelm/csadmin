<?php

namespace Classes\Modules\User;

use Classes\Traits\AutoAttributes;

class User
{

	use AutoAttributes;

	private $name;

	/** @return mixed */
	public function getName()
	{
		return $this->getAttr('lastname').' '.$this->getAttr('firstname');
	}

}
