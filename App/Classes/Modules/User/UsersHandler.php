<?php

namespace Classes\Modules\User;

use Classes\Db\Data;

class UsersHandler
{

	private $users = [];
	/**
	 * @var User
	 */
	private $user;

	public function __construct()
	{
		if (!$this->user) {
			$this->getUser();
		}
	}

	public function getLastLogin()
	{
		$data = new Data('cs_user_login');
		if ($sel = $data->selectSingle(['columns' => '*', 'where' => 'id_cs_user = '.intval($_SESSION['userId']), 'order' => 'login DESC', 'offset' => 1])) {
			return $sel['login'];
		}
		return false;
	}

	public function getUsersLogins()
	{
		$data = new Data('cs_user_login');
		if ($requests = $data->select(['columns' => ['*', '(SELECT lastname FROM cs_user WHERE id = id_cs_user) AS lastname', '(SELECT firstname FROM cs_user WHERE id = id_cs_user) AS firstname'], 'order' => 'login DESC'])) {
			return $requests;
		}
		return false;
	}

	public function update($update, $where)
	{
		$wh = [];
		foreach ($where as $k=>$v) {
			$wh[] = $k.' '.$v;
		}
		if (count($wh) > 0) {
			$date = new Data('cs_user');
			return $date->update($update, ['where' => (implode(' AND ', $wh))]);
		}
		return false;
	}

	public function getUser($id = null)
	{
		$where = null;
		if ($id) {
			$where = $id;
		} elseif (isset($_SESSION['userId'])) {
			$where = $_SESSION['userId'];
		}
		if ($where) {
			$this->loadUsers('id = '.intval($where));

			if ($this->users) {
				foreach ($this->users as $user) {
					if ($user->getId() == $where) {
						$this->user = $user;
						return $user;
					}
				}
			}
		}
		return false;
	}

	public function checkLogin($login)
	{
		$data = new Data('cs_user');
		if ($user = $data->selectSingle([
			'columns' => $this->getColumns(),
			'where' => 'email = "'.$login.'" AND active = 1'
		])) {
			return new User($user);
		}
		return false;
	}

	public function getUserRoleCodes()
	{
		if (!$this->user) {
			$this->getUser();
		}
		return $this->user->getAttr('role');
	}

	public function isAuthorized($auth)
	{
		if (is_array($auth) && array_intersect($auth, [$this->getUserRoleCodes()])) {
			return true;
		}
		if ($this->getUserRoleCodes() && $auth == $this->getUserRoleCodes()){
			return true;
		}
		return false;
	}

	private function loadUsers($where = null)
	{
		$data = new Data('cs_user');
		if ($users = $data->select(['columns' => $this->getColumns(), 'where' => $where])) {
			foreach ($users as $user) {
				$this->users[] = new User($user);
			}
		}
	}

	private function getColumns()
	{
		return [
			'*',
			'(SELECT code FROM cs_user_role WHERE id = cs_user.id_cs_user_role) as role',
		];
	}

}
