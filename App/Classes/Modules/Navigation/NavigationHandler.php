<?php

namespace Classes\Modules\Navigation;

use Classes\Db\Data;

class NavigationHandler
{

	private $data;
	private $navigations = [];

	public function __construct()
	{
		$this->data = new Data('cs_web_navigation');
	}

	public function getNavigationById($id)
	{
		if (!$this->navigations) {
			$this->load();
		}
		if ($this->navigations) {
			foreach ($this->navigations as $navigation) {
				if ($navigation->getId() == $id) {
					return $navigation;
				}
			}
		}
		return false;
	}


	private function load($where = [], $order = null)
	{
		$where[] = 'id_cs_web = '.intval($_SESSION['webId']);
		if ($navigations = $this->data->select(['columns' => $this->getColumns(), 'where' => implode(' AND ', $where), 'order' => $order])) {
			foreach ($navigations as $navigation) {
				$this->navigations[] = new Navigation($navigation);
			}
		}
	}

	private function getColumns()
	{
		return [
			'*',
		];
	}


}
