<?php

namespace Classes\Modules\Navigation;

use Classes\Traits\AutoAttributes;

class Navigation
{

	use AutoAttributes;

	public function getItems()
	{
		if ($items = $this->getAttr('navigation_items')) {
			return json_decode($items,true);
		}

		return [];
	}

}