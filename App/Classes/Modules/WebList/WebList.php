<?php

namespace Classes\Modules\WebList;

use Classes\Traits\AutoAttributes;

class WebList
{

	use AutoAttributes;

	public function getSettings()
	{
		if ($items = $this->getAttr('settings')) {
			return json_decode($items,true);
		}

		return [];
	}

}
