<?php

namespace Classes\Modules\WebList;

use Classes\Db\Data;

class WebListHandler
{

	private $lists;

	public function __construct()
	{
		$this->loadLists();
	}

	public function getListById($id)
	{
		if ($this->lists) {
			foreach ($this->lists as $list) {
				if ($list->getId() == $id) {
					return $list;
				}
			}
		}
		return false;
	}

	private function loadLists()
	{
		if (!$this->lists) {
			$data = new Data('cs_web_list');
			if ($lists = $data->select(['columns' => '*', 'where' => 'id_cs_web = '.intval($_SESSION['webId'])])) {
				foreach ($lists as $list) {
					$this->lists[] = new WebList($list);
				}
			}
		}
	}

}
