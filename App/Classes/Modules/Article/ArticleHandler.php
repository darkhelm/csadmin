<?php

namespace Classes\Modules\Article;

use Classes\Db\Data;

class ArticleHandler
{

	private $articles = [];

	public function __construct()
	{
		$this->loadArticles();
	}

	public function getArticles()
	{
		return $this->articles;
	}

	public function getArticlesForMenu()
	{
		$r = [];
		if ($this->articles) {
			foreach ($this->articles as $article) {
				if ($article->getAttr('in_menu') == 1) {
					$r[] = $article;
				}
			}
		}
		return $r;
	}

	private function loadArticles()
	{
		if (!$this->articles) {
			$data = new Data('cs_web_article');
			if ($articles = $data->select(['columns' => '*', 'where' => 'id_cs_web = '.intval($_SESSION['webId'])])) {
				foreach ($articles as $article) {
					$this->articles[] = new Article($article);
				}
			}
		}
	}


}
