<?php

namespace Classes\Modules\Cron;

use Classes\Db\Data;

class CronHandler
{

	private $data;

	public function __construct()
	{
		$this->data = new Data('cs_cron');
	}

	public function create($post)
	{
		return $this->data->insert(['column' => [$post]]);
	}

}
