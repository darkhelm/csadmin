<?php

namespace Classes\Modules\Banner;

use Classes\Traits\AutoAttributes;

class Banner
{

	use AutoAttributes;

	public function getArticles()
	{
		if ($items = $this->getAttr('list_articles')) {
			return json_decode($items,true);
		}

		return [];
	}

}
