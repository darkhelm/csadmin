<?php

namespace Classes\Modules\Banner;

use Classes\Db\Data;

class BannerHandler
{

	private $banners = [];

	public function __construct()
	{
		$this->loadBanners();
	}

	public function getBannerById($id)
	{
		if ($this->banners) {
			foreach ($this->banners as $banner) {
				if ($banner->getId() == $id) {
					return $banner;
				}
			}
		}
		return false;
	}

	private function loadBanners()
	{
		if (!$this->banners) {
			$data = new Data('cs_web_banner');
			if ($banners = $data->select(['columns' => '*', 'where' => 'id_cs_web = '.intval($_SESSION['webId'])])) {
				foreach ($banners as $banner) {
					$this->banners[] = new Banner($banner);
				}
			}
		}
	}


}
