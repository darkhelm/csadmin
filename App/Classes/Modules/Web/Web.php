<?php

namespace Classes\Modules\Web;

use Classes\Db\Data;
use Classes\Traits\AutoAttributes;

class Web
{

	use AutoAttributes;

	private $name;

	/** @return mixed */
	public function getName()
	{
		return $this->name;
	}

	public function isActive($icon = false)
	{
		if ($this->getAttr('active') == 1) {
			if ($icon) {
				return '<i class="fa fa-fw fad fa-check text-success"></i>';
			}
			return true;
		}
		if ($icon) {
			return '<i class="fa fa-fw fad fa-times text-danger"></i>';
		}
		return false;
	}

	public function isPublic($icon = false)
	{
		if ($this->getAttr('visible') == 1) {
			if ($icon) {
				return '<i class="fa fa-fw fad fa-check text-success"></i>';
			}
			return true;
		}
		if ($icon) {
			return '<i class="fa fa-fw fad fa-times text-danger"></i>';
		}
		return false;
	}

	public function getSettings($scope)
	{
		if ($settings = json_decode($this->getAttr('settings'), true)) {
			foreach ($settings as $key=>$setting) {
				if ($key == $scope) {
					return $setting;
				}
			}
		}
		return false;
	}

	public function getPayments()
	{
		$data = new Data('cs_web_payment');
		if ($sel = $data->select(['columns' => '*', 'where' => 'id_cs_web = '.intval($this->getId())])) {
			return $sel;
		}
		return false;
	}

}
