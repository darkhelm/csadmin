<?php

namespace Classes\Modules\Web;

use Classes\Db\Data;

class WebHandler
{

	private $webs = [];

	private $data;

	public function __construct()
	{
		$this->data = new Data('cs_web');
	}

	public function getUserCurrentWeb($id)
	{
		$data = new Data('cs_user_current_web LEFT JOIN cs_web ON cs_user_current_web.id_cs_web = cs_web.id');
		if ($web = $data->selectSingle(['columns' => 'cs_web.*', 'where' => 'cs_web.active = 1 AND id_cs_user = '.intval($id), 'limit = 1'])) {
			$_SESSION['webId'] = $web['id'];
			return new Web($web);
		}
		if ($webs = $this->getUserWebs($id)) {
			$web = $webs[0];
			$_SESSION['webId'] = $web['id'];
			$insert = [
				'id_cs_user' => $id,
				'id_cs_web' => $web['id'],
			];
			$data = new Data('cs_user_current_web');
			$data->insert(['column' => [$insert]]);
			return new Web($web);
		}

		return false;
	}

	public function changeUsersCurrentWeb($user, $web)
	{
		$data = new Data('cs_user_current_web');
		$update = [
			'id_cs_web' => $web,
		];
		if ($exist = $data->selectSingle(['columns' => '*', 'where' => 'id_cs_user = '.intval($user)])) {
			return $data->update($update, ['where' => 'id_cs_user = '.intval($user)]);
		}
		$update['id_cs_user'] = intval($user);
		return $data->insert(['columns' => [$update]]);
	}

	public function getWebByLink($link)
	{
		$this->loadWebs('active = 1 AND code LIKE "'.$link.'"');
		if ($this->webs) {
			return new Web($this->webs[0]);
		}
		return false;
	}

	public function getWebById($id)
	{
		$this->loadWebs('active = 1 AND id = '.intval($id));
		if ($this->webs) {
			return new Web($this->webs[0]);
		}
		return false;
	}

	public function getUserWebs($id)
	{
		$return = [];
		$data = new Data('cs_user_web LEFT JOIN cs_web ON cs_user_web.id_cs_web = cs_web.id');
		if ($webs = $data->select(['columns' => 'cs_web.*', 'where' => 'cs_web.active = 1 AND cs_user_web.id_cs_user = '.intval($id)])) {
			foreach ($webs as $web) {
				$return[] = $web;
			}
		}
		return $return;
	}

	public function getWebs($order = null)
	{
		$return = [];
		$this->loadWebs('active = 1', $order);
		if ($this->webs) {
			foreach ($this->webs as $web) {
				$return[] = new Web($web);
			}
		}
		return $return;
	}

	public function update($update, $id)
	{
		return $this->data->update($this->prepareSave($update),['where' => 'id = '.intval($id)]);
	}

	private function prepareSave($post)
	{
		if (isset($post['settings']) && count($post['settings']) > 0) {
			$settings = json_encode($post['settings']);
			$post['settings'] = $settings;
		} else {
			$post['settings'] = '';
		}
		return $post;
	}

	public function updateSettings($update, $id, $scope, $webSettings)
	{
		$upd = $paymentInsert = $paymentUpdate = [];
		if (isset($update['payment_insert'])) {
			$paymentInsert = $update['payment_insert'];
			$paymentUpdate = $update['payment_update'];

			unset($update['payment_insert']);
			unset($update['payment_update']);
		}
		if ($settings = json_decode($webSettings, true)) {
			foreach ($settings as $key => $values) {
				if ($key == $scope) {
					$upd['settings'][$key] = $update;
				} else {
					$upd['settings'][$key] = $values;
				}
			}
			if (!key_exists($scope, $settings)) {
				$upd['settings'][$scope] = $update;
			}
		} else {
			$upd['settings'][$scope] = $update;
		}
		if ($upd) {
			if ($this->data->update(['settings' => json_encode($upd['settings'], JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE)], ['where' => 'id = '.intval($id)])) {
				$data = new Data('cs_web_payment');
				if ($paymentInsert) {
					foreach ($paymentInsert as $insert) {
						$data->insert(['column' => [$insert]]);
					}
				}
				if ($paymentUpdate) {
					foreach ($paymentUpdate as $k=>$u) {
						$data->update($u, ['where' => 'id = '.intval($k)]);
					}
				}
				return true;
			}
		}
		return false;
	}

	private function loadWebs($where = null, $order = null)
	{
		$this->webs = $this->data->select(['columns' => $this->getColumns(), 'where' => $where, 'order' => $order]);
	}

	private function getColumns()
	{
		return [
			'*',
		];
	}

}
