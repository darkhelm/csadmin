<?php

namespace Classes\Modules\Mail;

use Classes\Db\Data;
use Classes\Modules\Web\Web;

class MailHandler
{

	private $mail = [];
	private $data;

	public function __construct()
	{
		$this->data = new Data('cs_web_mail');
	}

	public function update($post, $id,  Web $web)
	{
		return $this->data->update($post, ['where' => 'id = '.intval($id).' AND id_cs_web = '.intval($web->getId())]);
	}

	public function getMails(Web $web = null, $where = null, $order = null)
	{
		$this->mail = [];
		$this->load($web, $where, $order);
		return $this->mail;
	}
	public function getMailById($id)
	{
		$this->mail = [];
		$this->load(null, 'id = '.intval($id));
		if ($this->mail) {
			return $this->mail[0];
		}
		return false;
	}

	public function send()
	{
		include \Dir::app().'/../CLI/mail.php';
	}

	private function load(Web $web = null, $where = null, $order = null)
	{
		$wh = $web ? 'id_cs_web = '.$web->getId() : 'id_cs_web = '.\WC::component()->userSession()->webId;
		$wh .= $where ? ' AND '.$where : '';
		if ($mails = $this->data->select(['columns' => '*', 'where' => $wh, 'order' => $order])) {
			foreach ($mails as $mail) {
				$this->mail[] = new Mail($mail);
			}
		}
	}

}
