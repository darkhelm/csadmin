<?php

namespace Classes\Modules\Mail;

use Classes\Db\Data;
use Classes\Modules\Web\Web;
use Renderers\Renderer;

class MailHelper
{

	public static function save(array $mail, Web $web)
	{
		$data = new Data('cs_web_mail');
		$insert = [
			'id_cs_web' => $web->getId(),
			'recipient' => $mail['recipient'],
			'content' => $mail['content'],
			'subject' => $mail['subject'],
			'code' => $mail['code'],
		];
		return $data->insert(['column' => [$insert]]);
	}

	public static function saveGeneral(array $mail)
	{
		$data = new Data('cs_mail');
		$insert = [
			'recipient' => $mail['recipient'],
			'content' => $mail['content'],
			'subject' => $mail['subject'],
		];
		return $data->insert(['column' => [$insert]]);
	}

	public static function sendGeneralById($id)
	{
		$data = new Data('cs_mail');
		if ($mail = $data->selectSingle(['columns' => '*', 'where' => 'id = '.$id])) {
			$recipient = $mail['recipient'];
			$subject = $mail['subject'];

			$vars = [
				'subject' => $subject,
				'content' => $mail['content'],
			];

			$html = (new Renderer())->render(\Dir::template().'/../Renderers/Mail/mail.latte', $vars);
			if (self::send([
				'sender' => 'info@tipovacka.org',
				'recipient' => $recipient,
				'subject' => $subject,
				'html' => $html,
				'attachments' => isset($mail['attachments']) ? $mail['attachments'] : '',
			])) {
				$update = [
					'sent' => date('Y-m-d H:i:s')
				];
				$data->update($update, ['where' => 'id = '.$mail['id']]);
				return true;
			}
		}
		return false;
	}

	public static function sendById($id, Web $web)
	{
		if ($mail = \WC::component()->mail()->getMailById($id)) {
			$recipient = $mail->getAttr('recipient');
			$subject = $mail->getAttr('subject');

			$vars = [
				'subject' => $subject,
				'content' => $mail->getAttr('content'),
				'code' => $web->getAttr('code'),
				'logo' => $web->getAttr('image')
			];

			if (file_exists(\Dir::root().'/../tipovacka/public/themes/'.$web->getAttr('theme').'/templates/mail.latte')) {
				$html = (new Renderer())->render(\Dir::root().'/../tipovacka/public/themes/'.$web->getAttr('theme').'/templates/mail.latte', $vars);
			} else {
				$html = (new Renderer())->render(\Dir::root().'/../admin/App/Renderers/Mail/mail.latte', $vars);
			}
			if (self::send([
				'sender' => $mail->getAttr('sender'),
				'recipient' => $recipient,
				'subject' => $subject,
				'html' => $html,
				'attachments' => $mail->getAttr('attachments'),
			],$web)) {
				$update = [
					'sent' => date('Y-m-d H:i:s')
				];
				if (\WC::component()->mail()->update($update, $mail->getId(), $web)) {
					return \WC::component()->mail()->getMailById($mail->getId());
				}
			}
		}
		return false;
	}

	public static function send(array $mail, Web $web = null)
	{
		$settings = self::prepareMailgun($web);
		$mg = \Mailgun\Mailgun::create($settings['api'], $settings['endpoint']);
		if (is_array($mail['recipient'])) {
			\WC::component()->alert()->error('TODO Multiple recipients!');
		} else {
			if ($mg->messages()->send($settings['domain'], [
				'o:tracking' => true,
				'from' => $settings['name'].' <'.$settings['from'].'>',
				'h:Reply-To' => $settings['reply'],
				'to' => $mail['recipient'],
				'subject' => $mail['subject'],
				'html' => $mail['html'],
			])) {
				\WC::component()->logger()->info('MAIL: ', ['page' => 'sent mail', 'to' => $mail['recipient'], 'data' => date('Y-m-d H:i:s')]);
				return true;
			} else {
				\WC::component()->logger()->error('MAIL ERROR: ', ['page' => 'sent mail', 'to' => $mail['recipient'], 'data' => date('Y-m-d H:i:s')]);
			}
		}
		return false;
	}

	private static function prepareMailgun(Web $web = null)
	{
		if (!$web && isset($_SESSION['tipovacka-web'])) {
			$web = $_SESSION['tipovacka-web'];
		}
		if ($web) {
			$mail = $web->getSettings('mailgun');
			if ($mail['api'] != '' && $mail['domain'] != '' && $mail['from'] != '') {
				if ($mail['endpoint'] == '') {
					$mail['endpoint'] = 'https://api.mailgun.net';
				}
				return $mail;
			}
		}
		return [
			'api' => 'fc7f47bec388c74d69977f9476749ca7-ee13fadb-3664eab7',
			'endpoint' => 'https://api.eu.mailgun.net/v3/mg.tipovacka.org',
			'domain' => 'mg.tipovacka.org',
			'from' => 'info@tipovacka.org',
			'name' => 'Tipovačka.org',
			'reply' => 'info@tipovacka.org',
			'cc' => '',
		];

	}



}
