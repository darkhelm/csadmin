<?php

namespace Classes\Modules\Template;

use Classes\Db\Data;
use Classes\Modules\Web\Web;

class TemplateHandler
{

	private $templates = [];
	private $data;

	public function __construct()
	{
		$this->data = new Data('cs_web_template');
	}

	public function update($post)
	{
		$i = $ok = 0;
		foreach ($post['template'] as $id=>$values) {
			$update = [];
			foreach ($values as $k=>$v) {
				$update[$k] = $v;
			}
			if ($this->data->update($update, ['where' => 'id = '.intval($id).' AND id_cs_web = '.intval(\WC::component()->userSession()->webId)])) {
				$ok++;
			}
			$i++;
		}
		if ($i == $ok) {
			\WC::component()->alert()->success('Šablony byly aktualizovány.');
			\WC::app()->reload();
		}
		\WC::component()->alert()->error('Některé šablony se nepodařilo aktualizovat.');
		\WC::app()->reload();
	}

	public function getTemplates(Web $web = null)
	{
		$where = 'id_cs_web = '.\WC::component()->userSession()->webId;
		if ($web) {
			$where = 'id_cs_web = '.$web->getId();
		}
		if (!$this->templates) {
			$this->load($where);
		}
		if ($this->templates) {
			return $this->templates;
		}
		return [];
	}

	private function load($where = null)
	{
		if ($templates = $this->data->select(['columns' => '*', 'where' => $where])) {
			foreach ($templates as $template) {
				$this->templates[] = new Template($template);
			}
		}
	}

}