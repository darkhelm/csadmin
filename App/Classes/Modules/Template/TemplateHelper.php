<?php

namespace Classes\Modules\Template;

use Classes\Db\Data;
use Classes\Modules\Tiket\Tiket;
use Classes\Modules\Web\Web;

class TemplateHelper
{

	public static function getTemplate($code, Web $web = null)
	{
		$data = new Data('cs_web_template');
		if ($templates = $data->selectSingle(['columns' => '*', 'where' => 'id_cs_web = '.$web->getId().' AND code LIKE "'.$code.'"'])) {
			return $templates;
		}
		return false;
	}

	public static function prepareVariables(Web $web, Tiket $tiket, array $additional = [])
	{
		$banka = $web->getSettings('banka');
		$qrUrl = 'https://www.tipovacka.org/public/images/qr/'.$web->getId().'/'.$tiket->getAttr('vs').'.png';
		$return = [
			'{$banka}' => $banka['banka'],
			'{$banka_ucet}' => $banka['account'],
			'{$banka_cena}' => $banka['price'],
			'{$vs}' => $tiket->getAttr('vs'),
			'{$tipovacka}' => $web->getName(),
			'{$uzaverka}' => date('j. n. Y G:i', strtotime($web->getAttr('date_stop'))),
			'{$name}' => $tiket->getAttr('name'),
			'{$tiket_url}' => 'https://www.tipovacka.org/'.$web->getAttr('code').'/tiket/tiket='.$tiket->getId().'-'.$tiket->getUrlCode(),
			'{$tiket_kod}' => $tiket->getAttr('code'),
			'{$tipovacka_url}' => 'https://www.tipovacka.org/'.$web->getAttr('code'),
			'{$qr}' => '<img src="'.$qrUrl.'" style="width: 150px;" title="QR kód pro platbu">',
		];
		return array_merge($return, $additional);
	}

}
