<?php

namespace Classes\Components;

use Classes\Helpers\Alert;
use Classes\Helpers\App;
use Classes\Helpers\Files;
use Classes\Helpers\Filter;
use Classes\Helpers\MyLogger;
use Classes\Modules\Article\ArticleHandler;
use Classes\Modules\Banner\BannerHandler;
use Classes\Modules\Cron\CronHandler;
use Classes\Modules\Mail\MailHandler;
use Classes\Modules\Navigation\NavigationHandler;
use Classes\Modules\Template\TemplateHandler;
use Classes\Modules\User\UserSession;
use Classes\Modules\User\UsersHandler;
use Classes\Modules\Web\WebHandler;
use Classes\Modules\WebList\WebListHandler;
use Renderers\Renderer;
use Tracy\Debugger;

class ComponentLoader {

	private $components = [];

	/** @return App */
	public function app()
	{
		$this->log(__FUNCTION__);
		if (!$this->componentExists(__FUNCTION__)) {
			$this->addComponent(__FUNCTION__, App::getInstance());
		}
		return $this->components[__FUNCTION__];
	}

	/** @return MyLogger */
	public function logger()
	{
		$this->log(__FUNCTION__);
		return $this->loadComponent(__FUNCTION__, '\\Classes\\Helpers\\MyLogger');
	}


	/** @return NavigationHandler */
	public function navigation()
	{
		$this->log(__FUNCTION__);
		return $this->loadComponent(__FUNCTION__, '\\Classes\\Modules\\Navigation\\NavigationHandler');
	}

	/** @return ArticleHandler */
	public function article()
	{
		$this->log(__FUNCTION__);
		return $this->loadComponent(__FUNCTION__, '\\Classes\\Modules\\Article\\ArticleHandler');
	}

	/** @return BannerHandler */
	public function banner()
	{
		$this->log(__FUNCTION__);
		return $this->loadComponent(__FUNCTION__, '\\Classes\\Modules\\Banner\\BannerHandler');
	}

	/** @return WebListHandler */
	public function webList()
	{
		$this->log(__FUNCTION__);
		return $this->loadComponent(__FUNCTION__, '\\Classes\\Modules\\WebList\\WebListHandler');
	}

	/** @return UserSession */
	public function userSession()
	{
		$this->log(__FUNCTION__);
		return $this->loadComponent(__FUNCTION__, '\\Classes\\Modules\\User\\UserSession');
	}

	/** @return WebHandler */
	public function web()
	{
		$this->log(__FUNCTION__);
		return $this->loadComponent(__FUNCTION__, '\\Classes\\Modules\\Web\\WebHandler');
	}

	/** @return Alert */
	public function alert()
	{
		$this->log(__FUNCTION__);
		return $this->loadComponent(__FUNCTION__, '\\Classes\\Helpers\\Alert');
	}

	/** @return Renderer */
	public function renderer()
	{
		$this->log(__FUNCTION__);
		return $this->loadComponent(__FUNCTION__, '\\Renderers\\Renderer');
	}

	/** @return Filter  */
	public function filter()
	{
		$this->log(__FUNCTION__);
		return $this->loadComponent(__FUNCTION__, '\\Classes\\Helpers\\Filter');
	}

	/** @return Files  */
	public function files()
	{
		$this->log(__FUNCTION__);
		return $this->loadComponent(__FUNCTION__, '\\Classes\\Helpers\\Files');
	}

	/** @return UsersHandler */
	public function users()
	{
		$this->log(__FUNCTION__);
		return $this->loadComponent(__FUNCTION__, '\\Classes\\Modules\\User\\UsersHandler');
	}

	/** @return TemplateHandler */
	public function template()
	{
		$this->log(__FUNCTION__);
		return $this->loadComponent(__FUNCTION__, '\\Classes\\Modules\\Template\\TemplateHandler');
	}

	/** @return CronHandler */
	public function cron()
	{
		$this->log(__FUNCTION__);
		return $this->loadComponent(__FUNCTION__, '\\Classes\\Modules\\Cron\\CronHandler');
	}

	/** @return MailHandler */
	public function mail()
	{
		$this->log(__FUNCTION__);
		return $this->loadComponent(__FUNCTION__, '\\Classes\\Modules\\Mail\\MailHandler');
	}




	private function loadComponent($code, $class, $dependencies = []) {
		if ($this->componentExists($code)) {
			return $this->components[$code];
		} else {
			try {
				$reflectionClass = new \ReflectionClass($class);
				$this->addComponent($code, $reflectionClass->newInstanceArgs($this->getDependencies($dependencies)));
				return $this->components[$code];
			} catch(\Exception $e) {
				$this->addComponent($code, false);
			}
		}
		return false;
	}

	public function addComponent($code, $instance)
	{
		$this->components[$code] = $instance;
	}

	public function componentExists($code)
	{
		return isset($this->components[$code]);
	}

	private function getDependencies($dependencies)
	{
		$r = [];
		foreach ($dependencies as $dependency){
			if (method_exists($this, $dependency)){
				$r[] = $this->$dependency();
			}
		}
		return count($r) > 0 ? $r : [];
	}

	private function log($code)
	{
		if (Debugger::isEnabled() && $panel = Debugger::getBar()->getPanel('components')) {
			$panel->add($code);
		}
	}

}
