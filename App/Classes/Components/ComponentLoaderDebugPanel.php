<?php

namespace Classes\Components;

use Tracy\IBarPanel;

class ComponentLoaderDebugPanel implements IBarPanel
{

	private $components = [];
	private $basetime;

	public function __construct()
	{
		$this->basetime = microtime(true);
	}

	public function getTab()
	{
		return '<span title="Components"><span class="tracy-label">'.count($this->components).'</span></span>';
	}

	public function getPanel()
	{
		$table = '';
		foreach ($this->components as $key => $val) {
			$table .= '<tr><th>'.$key.'</th><td style="text-align: center;">'.$val['count'].'&times;</td><td style="text-align: right;">'.round(($val['first'] - $this->basetime)*1000, 3).'ms</td></tr>';
		}
		return '
			<h1>Loaded components</h1>
			<div class="nette-inner">
				<table><tbody>
				'.$table.'
				</tbody></table>
			</div>
		';
	}

	public function add($key)
	{
		if (isset($this->components[$key])) {
			$this->components[$key]['count']++;
		} else {
			$this->components[$key] = [
				'first' => microtime(true),
				'count' => 1,
			];
		}
	}

}
