<?php

namespace Renderers\User;

use Classes\Db\Data;
use Renderers\Renderer;

class User extends Renderer
{

	protected $templateDir = __DIR__;
	private $updatable = ['firstname', 'lastname', 'username', 'email', 'phone', 'password', 'salt'];

	public function prepareVariables()
	{

		if (is_array($_POST) && count($_POST) > 0) {
			$this->processForm($_POST);
		}

		$user = \WC::component()->users()->getUser();

		return $this->render($this->templateDir.'/user.latte', ['user' => $user,'pageTitle' => 'Nastavení uživatele']);
	}

	private function processForm($post)
	{
		$data = new Data('cs_user');
		$insert = [];
		foreach ($post as $key=>$value) {
			if ($key == 'password' && $value != '' && isset($post['password2']) && ($value == $post['password2'])) {
				$insert['salt'] = 'clpa2019';
				$insert['password'] = password_hash($insert['salt'] . $post['password'], PASSWORD_DEFAULT);
			} else {
				if ($key == 'password' && $value == '') {
				} else {
					$insert[$key] = $value;
				}
			}
		}
		if (count($insert) > 0) {
			foreach ($insert as $k => $v) {
				if (!in_array($k, $this->updatable)) {
					unset($insert[$k]);
				}
			}
			$updateId = \WC::component()->userSession()->userId;
			/**
			 * @var $user \Classes\Modules\User\User
			 */
			$user = \WC::component()->userSession()->getUser();
			if ($data->update($insert, ['where' => 'id = '.intval($updateId)])) {
				\WC::component()->logger()->info('UPDATE: ', ['page' => 'uzivatel-nastaveni', '#id' => $updateId, 'user' => $user->getAttr('email'), 'data' => date('Y-m-d H:i:s')]);
				\WC::component()->alert()->success("Nastavení uživatele ".$user->getAttr('firstname').' '.$user->getAttr('lastname')." bylo změněno.");
				\WC::app()->reload();
			} else {
				\WC::component()->logger()->info('ERROR: ', ['page' => 'uzivatel-nastaveni', '#id' => $updateId, 'user' => $user->getAttr('email'), 'data' => date('Y-m-d H:i:s')]);
				\WC::component()->alert()->error("Nastavení uživatele ".$user->getAttr('firstname').' '.$user->getAttr('lastname')." se nepodařilo uložit.");
			}
		}
	}

}
