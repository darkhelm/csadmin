<?php

namespace Renderers\Processors;

use Classes\Formatters\Strings;
use Classes\Helpers\PdfHelper;
use Classes\Modules\Invoicing\Invoice;
use Renderers\Renderer;

class Fakturace extends Processor
{

	private $subpages = ['zakaznici', 'nastaveni'];

	public function render()
	{
		if ($this->web && \WC::component()->users()->isAuthorized(['superAdmin'])) {
			$post = isset($_POST['save']) ? $_POST : [];
			if (is_array($post) && count($post) > 0) {
				unset($post['save']);
			}

			if (isset($this->parsed[1]) && in_array($this->parsed[1], $this->subpages)) {
				$name = 'render'.ucfirst(Strings::dashesToCamelCase($this->parsed[1]));
				if (method_exists($this,$name)) {
					return $this->$name($post);
				}
			} elseif (isset($this->parsed[1])) {
				return (new Renderer())->render(\Dir::template() . '/../Renderers/Error/error.latte', ['messages' => ['danger' => 'Pro vstup na tuto stránku nemáte oprávnění.']]);
			}

			return $this->renderInvoicing($post);
		}
		return (new Renderer())->render(\Dir::template() . '/../Renderers/Error/error.latte', ['messages' => ['danger' => 'Pro vstup na tuto stránku nemáte oprávnění.']]);
	}

	private function renderInvoicing(array $post = [])
	{
		$create = \WC::app()->get('create') == 'new' ? true : false;
		$invoice = $rows = null;
		if (isset($_POST['save']) || isset($_POST['savePdf'])) {
			unset($_POST['save']);
			$createPdf = isset($_POST['savePdf']) ? $_POST['savePdf'] : false;
			unset($_POST['savePdf']);
			if ($newId = \WC::component()->invoice()->save($_POST)) {
				$invoice = \WC::component()->invoice()->getInvoiceById($newId);
				\WC::component()->alert()->success('Faktura byla uložena.');
				if ($createPdf) {
					PdfHelper::save('invoicing/'.date('Y'), $invoice->getAttr('invoice_number').'.pdf', $this->getTemplate($invoice), '', '', 'Faktura '.$invoice->getAttr('invoice_number'), '');
				}
			}

			\WC::app()->redirect('/fakturace');
		}

		if ($id = \WC::app()->get('edit')) {
			$invoice = \WC::component()->invoice()->getInvoiceById($id);
			$create = true;
		}
		$vars = [
			'pageTitle' => 'Vydané faktury',
			'topButtons' => [
				'new' => ['name' => 'Nová', 'class' => 'plan-add-column btn-primary', 'link' => '/fakturace/create=new', 'authorized' => true]
			],
			'create' => $create,
			'selected' => $invoice,
			'invoices' => \WC::component()->invoice()->getInvoices(),
			'companies' => \WC::component()->invoice()->getCompanies(),
		];
		return (new Renderer())->render(\Dir::template() . '/../Renderers/Fakturace/prehled.latte', $vars);
	}

	private function getTemplate(Invoice $invoice)
	{
		$vars = [
			'invoice' => $invoice,
			'settings' => \WC::component()->invoice()->getSettings(),
		];
		return (new Renderer())->render(\Dir::template().'/../Renderers/Fakturace/pdf.latte', $vars);
	}

	private function renderNastaveni(array $post = [])
	{
		if (count($post) > 0) {
			if (\WC::component()->invoice()->updateSettings($post)) {
				$this->success[] = 'Nastavení bylo změněno.';
				\WC::component()->logger()->info('UPDATE',['page' => 'Nastavení fakturace', 'user' => $this->user->getAttr('email')]);
			} else {
				$this->error[] = 'Nastavení se nepodařilo změnit.';
				\WC::component()->logger()->info('UPDATE ERROR',['page' => 'Nastavení fakturace', 'user' => $this->user->getAttr('email')]);
			}
			\WC::component()->alert()->success($this->success);
			\WC::component()->alert()->error($this->error);
			\WC::app()->reload();
		}
		$vars = [
			'pageTitle' => 'Nastavení fakturace',
			'settings' => \WC::component()->invoice()->getSettings(),
		];
		return (new Renderer())->render(\Dir::template() . '/../Renderers/Fakturace/nastaveni.latte', $vars);
	}

}
