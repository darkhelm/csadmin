<?php

namespace Renderers\Processors;

use Renderers\Renderer;
use WC;

class Processors extends Renderer
{

	protected $templateDir = __DIR__;

	public function prepareVariables()
	{
		$parsed = WC::app()->parseUrl($_GET['page']);
		if ($parsed[0] == 404) {
			return (new Renderer())->render(\Dir::template() . '/../Renderers/Error/error.latte', ['messages' => '']);
		} else {
			$className = '\\Renderers\\Processors\\'.ucfirst($parsed[0]);
			if (class_exists($className)) {
				$processor = new $className($parsed);
				echo $processor->render();
			} else {
				echo (new Renderer())->render(\Dir::template() . '/../Renderers/Error/error.latte', ['messages' => '']);
			}
		}
	}

}
