<?php

namespace Renderers\Processors;

use Classes\Formatters\Date;
use Classes\Formatters\Strings;
use Classes\Helpers\TipCheckMailHelper;
use Classes\Modules\Mail\MailHelper;
use Classes\Modules\Tiket\TiketHandler;
use Renderers\Renderer;

class Mail extends Processor
{

	private $subpages = [
		'odeslat-zmeny','odeslane',
	];

	public function render()
	{
		if ($this->web && \WC::component()->users()->isAuthorized([
				'admin',
				'superAdmin'
			])) {
			$post = isset($_POST['save']) ? $_POST : [];
			if (is_array($post) && count($post) > 0) {
				unset($post['save']);
			}

			if (isset($this->parsed[1]) && in_array($this->parsed[1], $this->subpages)) {
				$name = 'render'.ucfirst(Strings::dashesToCamelCase($this->parsed[1]));
				if (method_exists($this, $name)) {
					return $this->$name($post);
				}
			} elseif (isset($this->parsed[1])) {
				return (new Renderer())->render(\Dir::template().'/../Renderers/Error/error.latte', ['messages' => ['danger' => 'Pro vstup na tuto stránku nemáte oprávnění.']]);
			}
		}
		return (new Renderer())->render(\Dir::template().'/../Renderers/Error/error.latte', ['messages' => ['danger' => 'Pro vstup na tuto stránku nemáte oprávnění.']]);
	}

	private function renderOdeslatZmeny()
	{
		if ($tiketId = \WC::app()->get('tiket')) {
			return $this->sendMail($tiketId);
		}
		$changes = TipCheckMailHelper::prepareMail();
		$vars = [
			'pageTitle' => 'Změny provedené administrátory čekající na odeslání',
			'web' => $this->web,
			'changes' => $changes,
			'tiket' => [new TiketHandler(),'getTicketById']
		];
		return (new Renderer())->render(\Dir::template() . '/../Renderers/Tiket/prehled-zmen-mail.latte', $vars);
	}

	private function renderOdeslane()
	{
		if ($id = \WC::app()->get('id')) {
			if (isset($_POST['save']) && isset($_POST['content']) && $_POST['content'] != '') {
				unset($_POST['save']);
				$post = $_POST;
				if ($new = MailHelper::save($post, $this->web)) {
					\WC::component()->alert()->prepare('Email pro byl uložen do databáze.');
					if ($mailSent = MailHelper::sendById($new, $this->web)) {
						\WC::component()->alert()->prepare('Email byl odeslán.');
					} else {
						\WC::component()->alert()->prepare('Email se nepodařilo odeslat.','error');
					}
				}
				\WC::app()->redirect('/mail/odeslane');
			}
			if ($mail = \WC::component()->mail()->getMailById($id, $this->web)) {
				$sent = $mail->getAttr('sent') ? Date::fullDate($mail->getAttr('sent')) : 'neodesláno';
				$vars = [
					'pageTitle' => 'Email <span>'.$mail->getAttr('recipient').', '.$sent.'</span>',
					'topButtons' => [
						'back' => [
							'name' => 'zpět na výpis',
							'class' => 'btn btn-primary',
							'authorized' => true,
							'link' => '/mail/odeslane'
						]
					],
					'web' => $this->web,
					'mail' => $mail
				];
				return (new Renderer())->render(\Dir::template().'/../Renderers/Mail/emailDetail.latte', $vars);
			}
		}

		$vars = [
			'pageTitle' => 'Odeslané emaily',
			'web' => $this->web,
			'mails' => \WC::component()->mail()->getMails(null,'','created DESC'),
		];
		return (new Renderer())->render(\Dir::template() . '/../Renderers/Mail/odeslane.latte', $vars);
	}

	private function sendMail($tiketId = null)
	{
		$code = 'mail-admin-tip-updates';

		$template = \Classes\Modules\Template\TemplateHelper::getTemplate($code, $this->web);

		if (is_numeric($tiketId)) {
			$changesTiket = TipCheckMailHelper::prepareMail($tiketId);
			$vars = [
				'web' => $this->web,
				'changes' => $changesTiket,
				'tiket' => [new TiketHandler(),'getTicketById']
			];
			$changes = (new Renderer())->render(\Dir::template() . '/../Renderers/Tiket/tiket.latte', $vars);
			if (count($changesTiket) > 0) {
				$tiket = \WC::component()->tiket()->getTicketById($tiketId);
				$vars = \Classes\Modules\Template\TemplateHelper::prepareVariables($this->web, $tiket, ['{$changed_tips}' => $changes]);
				$this->prepareMail($tiket, $code, $vars, $template);
			} else {
				\WC::component()->alert()->prepare('Nebyly nalazeny žádné změny.','error');
			}
		} elseif ($tiketId == 'all') {
			bdump('send all');
		}
		\WC::app()->redirect('/mail/odeslat-zmeny');
	}

	private function prepareMail(\Classes\Modules\Tiket\Tiket $tiket, $code, $vars, $template)
	{
		$savedId = false;
		$mail = [
			'recipient' => $tiket->getAttr('email'),
			'content' => strtr($template['template'], $vars),
			'subject' => strtr($template['subject'], $vars),
			'code' => $code,
		];

		if ($savedId = MailHelper::save($mail, $this->web)) {
			\WC::component()->alert()->prepare('Email pro '.$tiket->getAttr('email').' byl uložen do databáze.');
			if ($mailSent = MailHelper::sendById($savedId, $this->web)) {
				\WC::component()->alert()->prepare('Email pro '.$tiket->getAttr('email').' byl odeslán.');
				$update = [
					'sent_at' => $mailSent->getAttr('sent'),
					'id_cs_web_mail' => $mailSent->getId(),
				];
				TipCheckMailHelper::update($update,'sent_at IS NULL AND id_cs_tiket = '.intval($tiket->getId()));
			} else {
				\WC::component()->alert()->prepare('Email pro '.$tiket->getAttr('email').' se nepodařilo odeslat.','error');
			}
		}
		return $savedId;
	}

}
