<?php

namespace Renderers\Processors;

use Renderers\Renderer;

class Dashboard extends Processor
{

	public function render()
	{
		if ($this->web && \WC::component()->userSession()->isSignedIn()) {
			$vars = [
				'pageTitle' => 'Dashboard',
				'lastLogin' => \WC::component()->users()->getLastLogin(),
				'role' => \WC::component()->users()->getUserRoleCodes(),
				'logins' => \WC::component()->users()->getUsersLogins(),
			];
			return (new Renderer())->render(\Dir::template() . '/../Renderers/Dashboard/dashboard.latte', $vars);
		}
		return (new Renderer())->render(\Dir::template() . '/../Renderers/Error/error.latte', ['messages' => ['danger' => 'Pro vstup na tuto stránku nemáte oprávnění.']]);
	}

}
