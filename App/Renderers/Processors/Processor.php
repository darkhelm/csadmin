<?php

namespace Renderers\Processors;

class Processor
{

	protected $parsed;
	protected $web = null;
	protected $user = null;
	protected $success = [];
	protected $error = [];
	protected $info = [];

	public function __construct($parsed)
	{
		$this->parsed = $parsed;
		$this->web = \WC::component()->web()->getUserCurrentWeb($_SESSION['userId']);
		$this->user = \WC::component()->users()->getUser();
	}

	public function __destruct()
	{

	}

}
