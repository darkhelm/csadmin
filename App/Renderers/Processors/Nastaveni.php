<?php

namespace Renderers\Processors;

use Classes\Formatters\Strings;
use Classes\Helpers\QuestionHelper;
use Classes\Helpers\WordHelper;
use Renderers\Renderer;

class Nastaveni extends Processor
{

	private $subpages = ['typy-otazek', 'otazky', 'word', 'sablony', 'mailgun', 'banka'];
	private $scopeTitle = ['mailgun' => 'POŠTA', 'rezervace' => 'REZERVACE', 'banka' => 'BANKOVNÍ SPOJENÍ'];

	public function render()
	{
		if ($this->web && \WC::component()->users()->isAuthorized(['admin', 'superAdmin'])) {
			$post = isset($_POST['save']) ? $_POST : [];
			if (is_array($post) && count($post) > 0) {
				unset($post['save']);
			}

			if (isset($this->parsed[1]) && in_array($this->parsed[1], $this->subpages)) {
				$name = 'render'.ucfirst(Strings::dashesToCamelCase($this->parsed[1]));
				if (method_exists($this,$name)) {
					return $this->$name($post);
				}
			} elseif (isset($this->parsed[1])) {
				return (new Renderer())->render(\Dir::template() . '/../Renderers/Error/error.latte', ['messages' => ['danger' => 'Pro vstup na tuto stránku nemáte oprávnění.']]);
			}

			return $this->renderSettings($post);
		}
		return (new Renderer())->render(\Dir::template() . '/../Renderers/Error/error.latte', ['messages' => ['danger' => 'Pro vstup na tuto stránku nemáte oprávnění.']]);
	}

	private function renderWord()
	{
		if (isset($_SESSION['exported_file'])) {
			unset($_SESSION['exported_file']);
		}
		$_SESSION['exported_file']['file'] = WordHelper::create($this->web);
		$_SESSION['exported_file']['date'] = date('j. n. Y').' ('.date('H:i:s').')';

		$exportedFile = isset($_SESSION['exported_file']['file']) ? $_SESSION['exported_file'] : null;
		$vars = [
			'pageTitle' => 'Export',
			'exportedFile' => $exportedFile,
			'tipovacka' => $this->web,
		];
		return (new Renderer())->render(\Dir::template() . '/../Renderers/Nastaveni/word.latte', $vars);
	}

	private function renderMailgun($post = null)
	{
		if ($post && $settings['mailgun'] = $post['settings_mailgun']) {
			$this->updateSettings($post, 'mailgun');
		}
		$vars = [
			'pageTitle' => 'Nastavení pošty',
			'mailgun' => $this->web->getSettings('mailgun'),
		];
		return (new Renderer())->render(\Dir::template() . '/../Renderers/Nastaveni/mailgun.latte', $vars);
	}

	private function updateSettings($post, $scope)
	{
		if ($post && $settings[$scope] = $post['settings_'.$scope]) {
			if (\WC::component()->web()->updateSettings($settings[$scope], \WC::component()->userSession()->webId, $scope,$this->web->getAttr('settings'))) {
				$this->success[] = $this->scopeTitle[$scope].': nastavení pro ' . $this->web->getName() . ' bylo aktualizováno.';
				\WC::component()->alert()->success($this->success);
				\WC::component()->logger()->info('UPDATE: ', ['page' => $scope, 'table' => 'cs_web', '#id' => $this->web->getId(), 'user' => \WC::component()->userSession()->user->user['email']]);
			}
			\WC::app()->reload();
		}
	}

	private function renderTipy($otazkaId)
	{
		if ($question = \WC::component()->otazka()->getOtazkaById($otazkaId)) {
			$varsA = [
				'pageTitle' => 'Přehled tipů: <span>'.$question->getAttr('name').'</span>',
				'question' => $question,
			];

			if (\WC::app()->get('q') == 'medals') {
				$sortedTips = QuestionHelper::reviewMedals($question);
				$varsB = [
					'tips' => $sortedTips,
				];
				return (new Renderer())->render(\Dir::template() . '/../Renderers/Nastaveni/uprava-medaile.latte', $varsA+$varsB);
			}

			if (isset($_POST['submit'])) {
				unset($_POST['submit']);
				\WC::component()->tipCheck()->prepareSave($question, $_POST);
//				\WC::component()->tip()->prepareSave();
			}
			$varsB = [
				'tips' => \WC::component()->tip()->getTipsForQuestion($question),
				'questionHelper' => [QuestionHelper::class,'renderTip'],
			];
			return (new Renderer())->render(\Dir::template() . '/../Renderers/Nastaveni/prehled-tipy.latte', $varsA+$varsB);
		}
		\WC::app()->redirect('/nastaveni/otazky');
	}

	private function renderOtazky(array $post = [])
	{
		if ($q = \WC::app()->get('tipy')) {
			return $this->renderTipy($q);
		}
		$create = \WC::app()->get('create') == 'new' ? true : false;
		$question = null;
		if ($id = \WC::app()->get('edit')) {
			$question = \WC::component()->otazka()->getOtazkaById($id);
			$create = true;
		}
		if (count($post) > 0) {
			\WC::component()->otazka()->save($post);
		}

		if (isset($_POST['filterList']) && $_POST['filterList'] == 1) {
			\WC::component()->filter()->setFilter('cs_otazka', $_POST);
		}

//		$limit = 100;
//		$offset = \WC::app()->get('page') && \WC::app()->get('page') > 1 ? (\WC::app()->get('page') - 1) * $limit : 0;
		$filter = [
			'query' => isset($_POST['filterList']) && $_POST['filterList'] == 1 ? $_POST : null,
			'session' => 'cs_otazka',
			'web' => $_SESSION['webId'],
			'setColumns' => [
				'id_cs_otazka_sekce' => [
					'name' => 'sekce',
					'type' => 'select',
					'where' => 'id_cs_web = '.$_SESSION['webId']
				],
			],
		];
		$where = \WC::component()->filter()->prepareWhere('cs_otazka','cs_otazka_sekce.id_cs_web = '.intval($_SESSION['webId']));

		$vars = [
			'pageTitle' => 'Otázky',
			'topButtons' => [
				'new' => ['name' => 'Nová otázka', 'class' => 'plan-add-column btn-primary', 'link' => '/nastaveni/otazky/create=new', 'authorized' => true]
			],
			'webId' => $_SESSION['webId'],
			'userId' => $_SESSION['userId'],
			'create' => $create,
			'filter' => \WC::component()->filter()->render($filter),
			'options' => \WC::component()->otazka()->getOptions($question),
			'otazky' => \WC::component()->otazka()->getOtazky($where,'cs_otazka_sekce.sort, cs_otazka.sort'),
			'types' => \WC::component()->otazkaTyp()->getTypes('id_cs_web = '.intval($this->web->getId())),
			'sections' => \WC::component()->otazkaSekce()->getSekce('id_cs_web = '.intval($this->web->getId())),
			'results' => false,
			'wrongTextAnswers' => false,
			'selected' => $question,
			'session' => isset($_SESSION['otazka']) ? $_SESSION['otazka'] : false,
			'web' => $this->web,
			'path' => \WC::app()->getFullUrl(),
			'canDelete' => true // TODO check questions
		];
		if ($create) {
			unset($vars['topButtons']);
		}
		return (new Renderer())->render(\Dir::template() . '/../Renderers/Nastaveni/otazky.latte', $vars);
	}

	private function renderTypyOtazek(array $post = [])
	{
		$create = \WC::app()->get('create') == 'new' ? true : false;
		$questionType = null;
		if ($id = \WC::app()->get('edit')) {
			$questionType = \WC::component()->otazkaTyp()->getTypeById($id);
			$create = true;
		}
		if (count($post) > 0) {
			\WC::component()->otazkaTyp()->save($post);
		}
		$vars = [
			'pageTitle' => 'Nastavení typů otázek',
			'topButtons' => [
				'new' => ['name' => 'Nový typ', 'class' => 'plan-add-column btn-primary', 'link' => '/nastaveni/typy-otazek/create=new', 'authorized' => true]
			],
			'webId' => $_SESSION['webId'],
			'userId' => $_SESSION['userId'],
			'create' => $create,
			'plusminus' => \WC::component()->otazkaTyp()->getPlusMinus($questionType),
			'poradi' => \WC::component()->otazkaTyp()->getPoradi($questionType),
			'types' => \WC::component()->otazkaTyp()->getTypes('id_cs_web = '.$_SESSION['webId'], 'points DESC'),
			'questionType' => $questionType,
			'web' => $this->web,
			'canDelete' => true // TODO check questions
		];
		if ($create) {
			unset($vars['topButtons']);
		}
		return (new Renderer())->render(\Dir::template() . '/../Renderers/Nastaveni/typy-otazek.latte', $vars);
	}

	private function renderSettings(array $post = [])
	{
		if (count($post) > 0) {
			if ($_FILES['cs_web']['size'] > 0) {
				if ($fileName = \WC::component()->files()->upload($_FILES, $this->web->getId())) {
					$this->success[] = 'Soubor <b>'.$fileName['name'].'</b> o velikosti '.$fileName['size'].' byl nahrán jako '.$fileName['newName'].'.';
					$post['image'] = $fileName['link'];
				} else {
					$this->error[] = 'Soubor se nepodařilo nahrát.';
				}
			}
			if (!isset($post['visible'])) {
				$post['visible'] = 0;
			}
			if (\WC::component()->web()->update($post, $this->web->getId())) {
				$this->success[] = 'Nastavení bylo změněno.';
				\WC::component()->logger()->info('UPDATE',['page' => 'Nastavení akce', '#webId' => $this->web->getId(), 'user' => $this->user->getAttr('email')]);
			} else {
				$this->error[] = 'Nastavení se nepodařilo změnit.';
				\WC::component()->logger()->info('UPDATE ERROR',['page' => 'Nastavení akce', '#webId' => $this->web->getId(), 'user' => $this->user->getAttr('email')]);
			}
			\WC::component()->alert()->success($this->success);
			\WC::component()->alert()->error($this->error);
			\WC::app()->reload();
		}
		$vars = [
			'pageTitle' => 'Nastavení akce',
			'web' => $this->web,
		];
		return (new Renderer())->render(\Dir::template() . '/../Renderers/Nastaveni/nastaveni.latte', $vars);
	}

	private function renderSablony($post = null)
	{
		if ($post) {
			\WC::component()->template()->update($post);
		}
		$vars = [
			'pageTitle' => 'Nastavení šablon',
			'templates' => \WC::component()->template()->getTemplates(),
		];
		return (new Renderer())->render(\Dir::template() . '/../Renderers/Nastaveni/sablony.latte', $vars);
	}

	private function renderBanka($post = null)
	{
		if ($post && $settings['banka'] = $post['settings_banka']) {
			$this->updateSettings($post, 'banka');
		}
		$vars = [
			'pageTitle' => 'Nastavení banky',
			'banka' => $this->web->getSettings('banka'),
		];
		return (new Renderer())->render(\Dir::template() . '/../Renderers/Nastaveni/banka.latte', $vars);
	}

}
