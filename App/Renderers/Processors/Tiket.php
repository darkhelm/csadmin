<?php

namespace Renderers\Processors;

use Classes\Formatters\Strings;
use Renderers\Renderer;

class Tiket extends Processor
{

	private $subpages = [
		'',
	];

	public function render()
	{
		if ($this->web && \WC::component()->users()->isAuthorized([
				'admin',
				'superAdmin'
			])) {
			$post = isset($_POST['save']) ? $_POST : [];
			if (is_array($post) && count($post) > 0) {
				unset($post['save']);
			}

			if (isset($this->parsed[1]) && in_array($this->parsed[1], $this->subpages)) {
				$name = 'render'.ucfirst(Strings::dashesToCamelCase($this->parsed[1]));
				if (method_exists($this, $name)) {
					return $this->$name($post);
				}
			} elseif (isset($this->parsed[1])) {
				return (new Renderer())->render(\Dir::template().'/../Renderers/Error/error.latte', ['messages' => ['danger' => 'Pro vstup na tuto stránku nemáte oprávnění.']]);
			}
			if (\WC::app()->get('create') == 'new') {
				return $this->createNewTicket();
			}
			return $this->renderList($post);
		}
		return (new Renderer())->render(\Dir::template().'/../Renderers/Error/error.latte', ['messages' => ['danger' => 'Pro vstup na tuto stránku nemáte oprávnění.']]);
	}

	private function createNewTicket()
	{
		if (isset($_POST['create'])) {
			unset($_POST['create']);
			if (\WC::component()->tiket()->createTicket($this->web, $_POST, false)) {
				\WC::app()->redirect('/tiket');
			}
		}
		$vars = [
			'pageTitle' => 'Nový tiket',
		];
		return (new Renderer())->render(\Dir::template() . '/../Renderers/Tiket/tiket-form.latte', $vars);
	}

	private function renderList($post)
	{
		$tickets = \WC::component()->tiket()->getTickets($this->web, 'name, email');
		$questionCount = \WC::component()->otazka()->count();
		$vars = [
			'pageTitle' => 'Tikety',
			'topButtons' => [
				'new' => ['name' => 'Nový tiket', 'class' => 'plan-add-column btn-success', 'link' => '/tiket/create=new', 'authorized' => true]
			],
			'web' => $this->web,
			'tickets' => $tickets,
			'questionCount' => $questionCount,
			'path' => 'https://www.tipovacka.org/'.$this->web->getAttr('code').'/tiket/tiket=',
			'adminCode' => $_SESSION['userId']
		];
		return (new Renderer())->render(\Dir::template() . '/../Renderers/Tiket/prehled.latte', $vars);
	}

}
