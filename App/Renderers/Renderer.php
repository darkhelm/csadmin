<?php

namespace Renderers;

use Latte\Engine;
use Symfony\Component\Filesystem\Filesystem;

class Renderer {

	protected $vars = [];
	protected $templateDir = __DIR__;

	public function __construct()
	{

	}

	public function __call($name, $arguments)
	{
		$class = '\\Renderers\\'.ucfirst($name).'\\'.ucfirst($name);
		if (class_exists($class)){
			return (new $class())->prepareVariables();
		}
		return false;
	}

	public function render($template, $variables)
	{
		$latte = new Engine();
		$filesystem = new Filesystem();
		if (!$filesystem->exists(\Dir::temp().'/latte/')) {
			$filesystem->mkdir(\Dir::temp().'/latte/');
		}
		$latte->setTempDirectory(\Dir::temp().'/latte');
		$latte->addFilter('badge', function ($data, $style, $pretext = null, $posttext = null) {
			$text = [$pretext, $data, $posttext];
			$return = '<span class="badge '.$style.'">'.implode(' ',$text).'</span>';
			return $return;
		});
		$latte->addFilter('noempty', function ($data) {
			return $data != '' && $data != '0' ? $data : '';
		});
		$latte->addFilter('yesno', function ($data) {
			return $data != '' && $data != '0' ? 'ANO' : 'NE';
		});
		$latte->addFilter('dateTimeForm', function ($data) {
			return $data != '0000-00-00 00:00:00' ? $data : '';
		});
		$latte->addFilter('dateTime', function ($data) {
			return $data != '0000-00-00 00:00:00' && $data != null ? date('j. n. Y', strtotime($data)).'<span>'.date('G:i:s', strtotime($data)).'</span>' : '';
		});
		$latte->addFilter('myDate', function ($data) {
			return $data != '0000-00-00' && $data != null ? date('j. n. Y', strtotime($data)) : '';
		});
		$latte->addFilter('price', function ($number, $currency, $precision = 2) {
			return $number > 0 ? number_format($number,$precision,',',' ').'<span class="currency">'.$currency.'</span>' : '';
		});
		$latte->addFilter('invoiceFolder', function ($id) {
			return substr($id,0,4).'/'.$id.'.pdf';
		});
		$latte->addFilter('sortabledate', function ($data) {
			return $data != '0000-00-00 00:00:00' && $data != null ? date('YmdHis', strtotime($data)) : '';
		});
		return $latte->renderToString($template, $variables);
	}

}
