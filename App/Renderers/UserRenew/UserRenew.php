<?php

namespace Renderers\UserRenew;

use Classes\Db\Data;
use Classes\Modules\Mail\MailHelper;
use Mailgun\Mailgun;
use Renderers\Renderer;

class UserRenew extends Renderer
{

	protected $templateDir = __DIR__;

	public function prepareVariables()
	{
		if (isset($_POST['userFinish'])) {
			if ($this->processFinish($_POST)) {
				return $this->render($this->templateDir . '/userRenewStep2.latte', ['header' => \Classes\Helpers\Header::getHeader()]);
			}
			return $this->render($this->templateDir . '/userRenewStepOops.latte', ['header' => \Classes\Helpers\Header::getHeader()]);
		}

		if (isset($_POST['email'])) {
			$this->processStart($_POST);
			return $this->render($this->templateDir.'/userRenewStep1.latte',['header' => \Classes\Helpers\Header::getHeader()]);
		}

		$hash = \WC::app()->get('hash');
		if (!isset($hash) || $hash == '') {
			return $this->render($this->templateDir.'/userRenew.latte',['header' => \Classes\Helpers\Header::getHeader()]);
		} else {
			return $this->render($this->templateDir.'/userRenewFinish.latte',['header' => \Classes\Helpers\Header::getHeader()]);
		}

	}

	private function processFinish($post)
	{
		$request = new Data('cs_user_renew');
		$checkTime = date('Y-m-d H:i:s', strtotime('-30 minutes'));

		if ($sel = $request->selectSingle([
			'columns' => '*',
			'where' => 'email LIKE "'.$post['userFinish'].'" AND finished = "0000-00-00 00:00:00" OR finished IS NULL AND time >= "'.$checkTime.'"',
			'order' => 'time DESC',
			'limit' => 1
		])) {
			$hash = md5($sel['time'] . $post['userFinish'] . 'lenam');
			if ($hash == \WC::app()->get('hash')) {
				if (isset($post['password']) && $post['password'] != '' && isset($post['password2']) && ($post['password'] == $post['password2'])) {
					$insert['salt'] = 'clpa2019';
					$insert['password'] = password_hash($insert['salt'] . $post['password'], PASSWORD_DEFAULT);
					if (\WC::component()->users()->update($insert, ['email' => 'LIKE "' . $post['userFinish'] . '"'])) {
						\WC::component()->logger()->info('UPDATE: ', ['page' => 'obnova hesla 2', 'user' => $post['userFinish'], 'data' => date('Y-m-d H:i:s')]);
						return $request->update(['finished' => date('Y-m-d H:i:s')], ['where' => 'id = ' . intval($sel['id'])]);
					}
				}
			}
		}
		return false;
	}

	private function processStart($post)
	{
		$userCheck = new Data('cs_user');

		if ($userCheck->selectSingle([
			'columns' => '*',
			'where' => 'email LIKE "'.$post['email'].'"'
		])) {
			$now = date('Y-m-d H:i:s');
			$time = date('j. n. Y - H:i:s', strtotime('+30 minutes'));
			$hash = md5($now . $post['email'] . 'lenam');
			$odkaz = \WC::app()->getFullUrl() . '/hash=' . $hash;

			$insert = [
				'time' => $now,
				'email' => $post['email']
			];

			$request = new Data('cs_user_renew');
			if ($new = $request->insert(['column' => [$insert]])) {
				$emailText = $this->render($this->templateDir . '/userRenewMail.latte', ['odkaz' => $odkaz, 'time' => $time]);

				if ($id = MailHelper::saveGeneral([
					'sender' => 'info@tipovacka.org',
					'recipient' => $post['email'],
					'content' => $emailText,
					'subject' => 'Tipovacka.org - obnova hesla',
					'attachments' => [],
				])) {
					if (MailHelper::sendGeneralById($id)) {
						\WC::component()->alert()->success('Odkaz pro obnovu hesla byl odeslán.');
					} else {
						\WC::component()->alert()->error('Email se nepodařilo odeslat.');
					}
				}
			}
		}

	}
}
