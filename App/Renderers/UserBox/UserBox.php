<?php

namespace Renderers\UserBox;

use Classes\Modules\User\User;
use Renderers\Renderer;

class UserBox extends Renderer
{

	protected $templateDir = __DIR__;

	public function prepareVariables()
	{
		/**
		 * @var $user User
		 */
		$user = \WC::component()->users()->getUser();

		$vars = [
			'user' => $user,
			'userId' => $user->getId(),
			'webs' => \WC::component()->web()->getUserWebs($_SESSION['userId']),
			'selectedWeb' => \WC::component()->web()->getUserCurrentWeb($_SESSION['userId']),
		];

		return $this->render($this->templateDir.'/userbox.latte', $vars);
	}

}
