<link type="text/css" rel="stylesheet" href="/public/external/css/jquery.fancybox.min.css"  media="screen,projection"/>
<link type="text/css" rel="stylesheet" href="/public/external/css/jquery-ui.min.css"  media="screen,projection"/>
<link type="text/css" rel="stylesheet" href="/public/external/css/bootstrap.min.css"  media="screen,projection"/>
<link type="text/css" rel="stylesheet" href="/public/external/css/normalize.css"  media="screen,projection"/>
<link type="text/css" rel="stylesheet" href="/public/css/screen.min.css"  media="screen,projection"/>

<script src="/public/external/js/jquery-3.4.1.min.js"></script>
<script src="/public/external/js/modernizr-2.6.2.min.js"></script>
<script src="/public/external/js/bootstrap.bundle.min.js"></script>
<script src="/public/external/js/bootstrap-datetimepicker.min.js"></script>
<script src="/public/external/js/fontawesome.min.js"></script>
<script src="/public/external/js/jquery.fancybox.min.js"></script>
<script src="/public/external/js/tooltip.js"></script>
<script src="/public/external/js/toast.js"></script>
<script src="/public/external/js/jscolor.js"></script>
<script src="/vendor/ckfinder/ckfinder.js"></script>
<script src="/vendor/ckeditor/ckeditor.js"></script>
<script src="/public/external/js/drag.min.js"></script>
<script src="/public/external/js/jquery-ui.min.js"></script>
<script src="/public/external/js/jquery.form.min.js"></script>

<script src="/public/js/cookies.js"></script>
<script src="/public/js/default.js"></script>
<script src="/public/js/functions.js"></script>
<script src="/public/js/settings.js"></script>
<script src="/public/js/banners.js"></script>

<link rel="icon" href="/public/images/favicon.ico" type="image/x-icon" />

<!--<script src="//cdnjs.cloudflare.com/ajax/libs/jodit/3.1.39/jodit.min.js"></script>-->
<!--<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jodit/3.1.39/jodit.min.css">-->
