<?php
WC::app()->parseUrl($_GET['page']);
$settings = \Nette\Neon\Neon::decode(file_get_contents(Dir::config().'/settings.neon'));
$exceptions = $settings['menu-exceptions'];

$class = 'full';
if($session->isSignedIn()){
	$user = WC::component()->users();
	$menuNeon = \Nette\Neon\Neon::decode(file_get_contents(Dir::config().'/menu.neon'));
	$userBox = WC::component()->renderer()->userBox();
	$mm = '
	<div class="column-left">
		'.$userBox.'
		<div class="main-menu">
	';
	foreach ($menuNeon as $keySection=>$sections){
		$authorized = false;
		$authorizedRoles = [];
		if (isset($sections['auth'])){
			$auth = explode('|', $sections['auth']);
			$authorized = $user->isAuthorized($auth);
			$authorizedRoles = $auth;
		} elseif (!isset($sections['auth'])){
			$authorized = true;
		}
		if ($authorized) {
			$classSection = '';
			$faIcon = 'chevron-circle-right';
			if (isset($sections['icon'])){
				$faIcon = $sections['icon'];
			}
			$icon = '<span><i class="fad fa-'.$faIcon.' fa-fw"></i></span>';
			if (isset($_COOKIE['csAdmin--menu']) && $_COOKIE['csAdmin--menu'] == $keySection) {
				$classSection = 'active';
			}
			$mm .= '
					<div class="' . $classSection . '" data-menu="' . $keySection . '">
						<h4 class="accordion">' . $icon . $sections['name'] . '</h4>
						<div class="panel">
							<ul>
				
					';
			if (is_array($sections['menu']) && count($sections['menu']) > 0) {
				foreach ($sections['menu'] as $key => $value) {
					$authorized = true;
					$authorizedRule = [];
					if (isset($value['rule'])){
						$authorizedRule = explode('|', $value['rule']);
					}

					if (count($authorizedRule) > 0) {
						$authorized = $user->isAuthorized($authorizedRule);
					}

					if ($authorized) {
						$class = '';
						$p = explode('/', WC::app()->get('baseLink'));
						if (WC::app()->get('baseLink') == $key) {
							$class = 'selected';
						} elseif (!in_array(WC::app()->get('baseLink'), $exceptions) && $p[0] == $key) {
							$class = 'selected';
						}

						$faSubIcon = 'angle-right ';
						if (isset($value['icon'])) {
							$faSubIcon = $value['icon'];
						}

						$badge = '';
						if (isset($value['badge'])) {
							$methodName = ucfirst($value['badge'][0]);
							if (is_callable($methodName,false)) {
								$count = call_user_func($methodName);
								$badgeClass = '';
								if (isset($value['badgeClass'])) {
									$badgeClass = 'badge-'.$value['badgeClass'];
								}
								$badge = '<span class="badge badge-pill badge-menu '.$badgeClass.'">'.$count.'</span>';
							}
						}


						$subIcon = '<span><i class="fad fa-' . $faSubIcon . ' fa-fw fa-sm"></i></span>';
						$mm .= '
								<li class="' . $class . '"><a href="/' . $key . '">' . $subIcon . $value['name'] . $badge .'</a></li>
						';
					}
				}
			}
			$mm .= '
							</ul>
						</div>
					</div>
				';
		}
	}
	$mm .= '
		</div> 
	</div> 
	';

	echo $mm;
	$class = '';
}
?>
	<main>
		<?php include __DIR__.'/../starter.php'?>
	</main>

