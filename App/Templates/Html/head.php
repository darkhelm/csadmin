<!DOCTYPE html>
<html lang="cs">
<head>
	<title>csAdmin</title>
	<?php
	include 'meta.php';
	include 'link.php';
	?>
</head>
<?php
$parsedClass = (isset($parsed) ? \Classes\Formatters\BodyClass::render($parsed) : '');
?>
<body class="page <?=$presenterClass?> <?=$parsedClass?>">

<?php
