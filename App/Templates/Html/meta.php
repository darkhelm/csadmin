<!--Recommended Meta Tags-->
<meta charset="utf-8">
<meta name="language" content="czech">
<meta http-equiv="content-type" content="text/html">
<meta name="author" content="Alex Potesil">
<meta name="designer" content="Alex Potesil">
<meta name="publisher" content="Alex Potesil">
<!--<meta name="no-email-collection" content="http://www.unspam.com/noemailcollection/">-->

<!--Search Engine Optimization Meta Tags-->
<meta name="description" content="csAdmin">
<meta name="keywords" content="csAdmin">
<meta name="robots" content="index,follow">
<meta name="revisit-after" content="7 days">
<meta name="distribution" content="web">
<!--<meta http-equiv="refresh" content="30">-->
<meta name="robots" content="noodp">

<!--Optional Meta Tags-->
<meta name="distribution" content="web">
<meta name="web_author" content="Alex Potesil">
<meta name="rating" content="general">
<meta name="subject" content="Personal">
<meta name="title" content="csAdmin">
<meta name="copyright" content="Copyright 2020">
<meta name="reply-to" content="apotesil@gmail.com">
<meta name="abstract" content="csAdmin">
<meta name="city" content="Liberec">
<meta name="country" content="Czech Republic">
<meta name="distribution" content="global">
<meta name="classification" content="csAdmin">

<!--Meta Tags for HTML pages on Mobile-->
<meta name="format-detection" content="telephone=yes"/>
<meta name="HandheldFriendly" content="true"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<!--http-equiv Tags-->
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
