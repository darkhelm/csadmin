<footer>
	<div class="copyright">
		<?php
		$copy = '2020';
		if (date('Y') != $copy){
			$copy .= ' - '.date('Y');
		}
		$footer = [
			'<i class="fal fa-copyright"></i> '.$copy.'',
			'Alex. Potěšil',
			'administrator@csadmin.cz',
		];

		echo '<span>'.implode('</span><span>', $footer).'</span>';
		?>

	</div>
</footer>

<div class="overlay-container"></div>
<div id="dialog"><div id="dialog-content"></div></div>
<?=\Classes\Helpers\Alert::render();?>
</body>
</html>
