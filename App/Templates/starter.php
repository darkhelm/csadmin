<?php
if(!isset($_COOKIE['csAdmin--cookies']) || (isset($_COOKIE['csAdmin--cookies']) && $_COOKIE['csAdmin--cookies'] != 'Accepted')) {
	include Dir::template()."/Modals/cookie-bar.php";
}
/**
 * @var $presenter \Classes\Helpers\Presenter
 */

WC::init();
if ($link == '/uzivatel/odhlasit'){
	if ($session->isSignedIn() && isset($parsed[1])) {
		$processor = \Classes\Helpers\PresenterProcessor::process($parsed[1]);
		$presenter = new $processor($parsed, $session, $user);
		echo $presenter->process();
	}
} elseif ($link == '/obnova-hesla' || preg_match('/obnova-hesla\/(.*)/',$link)) {
	echo WC::component()->renderer()->userRenew();
} elseif ($link == '/dashboard'){
	if ($session->isSignedIn() && isset($parsed[1])) {
		$content = WC::component()->renderer()->Processors();
	}
} elseif ($link == '/uzivatel/nastaveni') {
	if ($session->isSignedIn()) {
		echo WC::component()->renderer()->user();
	}
} else {
	if ($session->isSignedIn() && isset($parsed[1])) {
		if ($action = WC::app()->get('action')) {
			$templateName = str_replace('/', '-', WC::app()->get('baseLink'));
			$template = \Classes\Helpers\AdminTemplate::getTemplate($templateName);
			if ($template) {
				include Dir::template() . "/Modals/delete.php";
				$processor = \Classes\Helpers\ActionProcessor::process($action);
				$r = new $processor();
				$vars['page'] = $r->$action();
				$vars['title'] = $template['title'];
				echo \Classes\Helpers\Template::render('Admin/admin', ['vars' => $vars]);
			}
		} elseif($parsed[1] == 'settings' || $parsed[1] == 'nastaveni') {
			if (count($parsed) == 2) {
				\Classes\Helpers\Functions::redirect('/');
			} else {
				include Dir::template() . "/Modals/delete.php";
				echo \Classes\Helpers\Template::render('Admin/admin', ['vars' => \Classes\Helpers\AdminTemplate::getPage([$parsed[1], $parsed[2]])]);
			}
		} elseif(isset($parsed[1])) {
			$content = WC::component()->renderer()->Processors();
//			$processor = \Classes\Helpers\PresenterProcessor::process($parsed[1]);
//			$presenter = new $processor($parsed, $session, $user);
//			echo $presenter->process();
		}
//	} else {
//		$processor = new \Classes\Presenters\ErrorPresenter($parsed);
//		echo $processor->process();
	}
}
