<div class="admin-header">
	<h2><?=$vars['title']?></h2>
	<?php
	if (WC::app()->get('action')){
		echo '<div class="top-secondary btn btn-sm btn-outline-primary"><a href="/'.WC::app()->get('baseLink').'">Zpět na seznam</a></div>';
	} elseif (isset($vars['template']['create']) && $vars['template']['create']) {
		if (WC::component()->users()->isAuthorized($vars['template']['canCreate'])) {
			echo '<div class="top-secondary btn btn-sm btn-primary"><a href="/' . WC::app()->get('baseLink') . '/action=create">Nová položka</a></div>';
		}
	}
	?>
</div>