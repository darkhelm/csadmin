
<!-- Trigger the modal with a button -->
<!--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>-->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content alert-danger">
      <div class="modal-header">
        <h2 class="modal-title">Smazání příspěvku</h2>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <h3 class="text-center"><strong>POZOR!</strong> Tento krok je nevratný. Určitě smazat?</h3>
      </div>
      <div class="modal-footer">
       	<a href="" class="btn btn-danger modal_delete_link">Smazat</a>
        <button type="button" class="btn btn-default" data-dismiss="modal">Zrušit</button>
      </div>
    </div>

  </div>
</div>