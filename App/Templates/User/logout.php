<?php
session_destroy();
if(isset($_COOKIE['username'])) {
	unset($_COOKIE['username']);
	setcookie('username', '', time()-86400);
}
WC::component()->logger()->info('LOGOUT', ['user' => WC::component()->users()->getUser()->getAttr('email')]);
WC::app()->redirect('/');
