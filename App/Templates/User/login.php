<div class="login-box">
	<div>
		<h1><i class="fas fa-sign-in"></i> CSAdmin.cz</h1>
	</div>
	<div>
		<?=$message?>
		<form id="login-id" action="" method="post">
			<span class="form-group">
				<label for="login">E-mail</label>
				<input type="text" class="form-control form-control-sm" name="login" required>
			</span>
			<span class="form-group">
				<label for="password">Heslo</label>
				<input type="password" class="form-control form-control-sm" name="password" required>
			</span>
			<span class="form-group">
				<input type="submit" name="submit" value="Přihlásit" class="btn btn-sm btn-primary">
				<span class="renew"><a href="/obnova-hesla" class="link-secondary">obnovit heslo</a></span>
			</span>
		</form>
	</div>
</div>
