$().ready(function () {
    var dialog = $('#dialog');

    $('.alert').alert();

    var navbarHeight = $('.admin-header').outerHeight();
    $('.admin-body').css('margin-top', navbarHeight);

    $('.btn-loader-show').on('click', function () {
        $(".se-pre-con").fadeIn("slow");
    });
    $(".se-pre-con").fadeOut("slow");

    $("#password, #password2").keyup(checkPasswordMatch);

    function checkPasswordMatch() {
        var password = $("#password").val();
        var confirmPassword = $("#password2").val();

        if (password.length > 0 || confirmPassword.length > 0) {
            if (password !== confirmPassword) {
                $("#div-password-check").html("Hesla se neshodují!").removeClass().addClass("form-text text-danger");
                $('.submitUser').prop('disabled', true);
            } else {
                $("#div-password-check").html("Hesla se shodují.").removeClass().addClass("form-text text-success");
                $('.submitUser').prop('disabled', false);
            }
        }
    }

    let editor = CKEDITOR.replace( 'ckeditor' );
    CKFinder.setupCKEditor( editor );

    // ClassicEditor
    //     .create( document.querySelector( '.ckeditor' ), {
    //         toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote' ],
    //         heading: {
    //             options: [
    //                 { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
    //                 { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
    //                 { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' }
    //             ]
    //         }
    //     } )
    //     .catch( error => {
    //         console.log( error );
    //     } );

    $('.accept-button').on('click', function () {
        setCookieJS('csAdmin--cookies', 'Accepted', 30);
        $(".cookie-bar").hide();
        return false;
    });

    var accordion = $(".accordion");
    accordion.on('click', function (e) {
        var parent = $(this).parent();
        $('.main-menu div').removeClass('active');
        parent.addClass('active');
        setCookieJS('csAdmin--menu', parent.data('menu'), 7);
    });

    var loaded = false;
    var timeRemaining = 60*60*5;
    var timeWarning = 59;
    if (window.location.pathname !== '/'){
        startTimer(timeRemaining);
    }

    $(".btn-headerModal").on('click', function (e) {
        e.preventDefault();
        $("#modalHeader").modal();
    });


    $(".delete_link").on('click', function (e) {
        e.preventDefault();
        var id = $(this).attr("rel");
        var url = $(this).attr("page");
        var delete_url = url + "/action=delete/id=" + id + " ";

        $(".modal_delete_link").attr("href", delete_url);
        $("#myModal").modal();

    });
    
    $(".delete_link_file").on('click', function (e) {
        e.preventDefault();
        var file = $(this).data("file");
        var table = $(this).data("table");
        var filename = $(this).data("filename");
        var fileUrl = $(this).data("fileUrl");
        var _this = this;
    
        $.ajax({
            method: 'post',
            url: '/API/file-delete.php',
            data: {
                'table': table,
                'file': file,
                'fileUrl': fileUrl,
            },
            dataType: "html",
            success: function(result) {
                if (result === 'OK'){
                    $(_this).parent().parent().hide();
                    $.toast({
                        icon: 'success',
                        text: 'Soubor '+ filename +' byl smazán.',
                        hideAfter: 7500,
                        showHideTransition: 'fade',
                        position: {
                            top: 65,
                            right: 35
                        },
                        allowToastClose: true,
                    });
                } else {
                    $.toast({
                        icon: 'error',
                        text: 'Soubor '+ filename +' nebyl smazán.',
                        hideAfter: 7500,
                        showHideTransition: 'fade',
                        position: {
                            top: 65,
                            right: 35
                        },
                        allowToastClose: true,
                    });
                }
            }
        });
        
    });

    $(".moveUp,.moveDown").click(function(e){
        e.preventDefault();
        var row = $(this).parents("tr:first");
        var id = $(this).attr("rel");
        var url = $(this).attr("page");
        var position = 'moveUp';
        if ($(this).is(".moveDown")) {
            position = 'moveDown';
            row.insertBefore(row.prev());
        } else {
            row.insertAfter(row.next());
        }
        $.ajax({
            url: '/Api/move.php',
            data: {page: url, id: id, position: position},
            type: "POST",
            success: function (e) {
                if (e == 'up' || e == 'down') {
                    $.toast({
                        icon: 'success',
                        text: 'Položka '+ id +' přesunuta',
                        hideAfter: 7500,
                        showHideTransition: 'fade',
                        position: {
                            top: 65,
                            right: 35
                        },
                        allowToastClose: true,
                    });
                } else {
                }
            }
        });
    });

    $(".switcher").on('click',function(e){
        e.preventDefault();
        var id = $(this).attr("rel");
        var url = $(this).attr("page");
        var $this = $(this);
        $.ajax({
            url: '/Api/switcher.php',
            data: {page: url, id: id},
            type: "POST",
            dataType: "json",
            error: function(info) {
                console.log(info);
            },
            success: function (info) {
                if (info['action'] === 'on' || info['action'] === 'off') {
                    $.toast({
                        icon: 'success',
                        text: info['message'],
                        hideAfter: 7500,
                        showHideTransition: 'fade',
                        position: {
                            top: 65,
                            right: 35
                        },
                        allowToastClose: true,
                    });
                    if (info['action'] === 'on'){
                        $this.removeClass('toggle-off text-notactive').addClass('toggle-on text-success');
                    } else {
                        $this.removeClass('toggle-on text-success').addClass('toggle-off text-notactive');
                    }
                } else {
                    if (info['action'] === 'notauthorized'){
                        var msg = info['message'];
                    } else {
                        var msg = 'Položku '+id+' se nepodařilo aktualizovat.';
                    }
                    $.toast({
                        icon: 'error',
                        text: msg,
                        hideAfter: 7500,
                        showHideTransition: 'fade',
                        position: {
                            top: 65,
                            right: 35
                        },
                        allowToastClose: true,
                    });
                }
            }
        });
    });

    function startTimer(duration) {
        var start = Date.now(),
            diff,
            minutes,
            seconds;

        function timer() {
            // get the number of seconds that have elapsed since
            // startTimer() was called
            diff = duration - (((Date.now() - start) / 1000) | 0);

            minutes = (diff / 60) | 0;
            seconds = (diff % 60) | 0;

            // minutes = minutes < 10 ? "0" + minutes : minutes;
            // seconds = seconds < 10 ? "0" + seconds : seconds;

            if (diff <= 0) {
                start = Date.now() + 1000;
            }
            if (minutes === 0 && seconds === 0){
                window.location.href = '/uzivatel/odhlasit';
            }
            if (minutes === 0 && seconds === timeWarning){
                $.toast({
                    icon: 'info',
                    text: 'Pro nečinnost budete odhlášeni...',
                    hideAfter: 1000*timeWarning,
                    showHideTransition: 'fade',
                    position: {
                        top: 65,
                        right: 35
                    },
                    allowToastClose: false,
                    loader: true,
                    loaderBg: '#FF0000',
                });
            }

        };
        // we don't want to wait a full second before the timer starts
        timer();
        setInterval(timer, 1000);
    }

    var inputs = $('.form-admin input[type="text"]');
    var toastExists = false;
    inputs.each(function () {
        var defValue = $(this).val();
        $(this).on('change', function () {
            if (loaded && !toastExists && defValue !== $(this).val()) {
                $.toast({
                    icon: 'warning',
                    text: 'Formulář může obsahovat změněná data.<br>Nezepomeňte jej uložit.',
                    hideAfter: false,
                    showHideTransition: 'fade',
                    position: 'top-center',
                    allowToastClose: false,
                });
                toastExists = true;
            }
        })
    });

    $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd hh:ii",
        autoclose: true,
        todayBtn: true,
        minuteStep: 10,
        todayHighlight: true,
        weekStart: 1
    });
    $(".form_dateTimeStamp").datetimepicker({
        format: "yyyy-mm-dd hh:ii",
        autoclose: true,
        todayBtn: true,
        minuteStep: 1,
        todayHighlight: true,
        weekStart: 1
    });
    $(".form_date").datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: true,
        todayHighlight: true,
        minView: "month",
        weekStart: 1
    });

    if (typeof getCookie('csAdmin--alert-error') !== "undefined") {
        var items = JSON.parse(decodeURIComponent(getCookie('csAdmin--alert-error')));
        $(items).each(function (e,v) {
            $.toast({
                icon: 'error',
                text: v.replace(/\+/g, ' '),
                hideAfter: 7500,
                showHideTransition: 'fade',
                position: 'top-center',
                allowToastClose: true,
            });
        });
        setCookieJS('csAdmin--alert-error', '', '-1', '/');
    }
    if (typeof getCookie('csAdmin--alert-success') !== "undefined") {
        var items = JSON.parse(decodeURIComponent(getCookie('csAdmin--alert-success')));
        $(items).each(function (e,v) {
            $.toast({
                icon: 'success',
                text: v.replace(/\+/g, ' '),
                hideAfter: 7500,
                showHideTransition: 'fade',
                position: 'top-center',
                allowToastClose: true,
            });
        });
        setCookieJS('csAdmin--alert-success', '', '-1', '/');
    }

    var $chkboxes = $('.chkbox');
    var lastChecked = null;

    $chkboxes.on('click', function(e) {
        if (!lastChecked) {
            lastChecked = this;
            return;
        }

        if (e.shiftKey) {
            var start = $chkboxes.index(this);
            var end = $chkboxes.index(lastChecked);

            $chkboxes.slice(Math.min(start,end), Math.max(start,end)+ 1).prop('checked', lastChecked.checked);
        }

        lastChecked = this;
    });

});

