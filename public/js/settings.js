var settings = {
	dialog: null,
	wrapper: null,
	init: function ($wrapper) {
		this.dialog = $('#dialog');
		this.wrapper = $wrapper;
		this.answerType();
		this.checkType();
		this.medals();
		this.winner();
	},
	winner: function () {
		let $this = this;
		let n = $("input[name=winner]:checked").length;
		this.wrapper.on('change','input[name=winner]', function (e) {
			$this.winner();
		});
		if (n === 1) {
			$('.question-match-winner').show();
		} else {
			$('.question-match-winner').hide();
		}
	},
	medals: function () {
		let $this = this;
		let n = $("input[name=medals]:checked").length;
		this.wrapper.on('change','input[name=medals]', function (e) {
			$this.medals();
		});
		if (n === 1) {
			$('.question-plus-minus-points').hide();
			$('.question-exact-points').hide();
			$('.question-medals-points').show();
		} else {
			$('.question-plus-minus-points').show();
			$('.question-exact-points').show();
			$('.question-medals-points').hide();
		}
	},
	answerType: function () {
		var $this = this;
		this.wrapper.on('change','select[name=answer_type]', function(e) {
			$this.checkType();
			// var selected = $('select[name=answer_type] option:selected');
			// var type = selected.val();
			// var td = $('#answer-type-options');
			// $.ajax({
			// 	method: 'post',
			// 	url: '/Api/answer-type-options.php',
			// 	data: {
			// 		'type': type,
			// 	},
			// 	dataType: "html",
			// 	success: function (result) {
			// 		td.html(result);
			// 	}
			// });
		});
	},
	checkType: function () {
		var selected = $('select[name=answer_type] option:selected');
		var type = selected.val();
		var options = $('#answer-type-options');
		var numeric = $('#wrong-answers-numeric');
		if (type === 'select') {
			options.removeClass('is-hidden');
		} else {
			options.addClass('is-hidden');
		}
		if (type === 'number') {
			numeric.removeClass('is-hidden');
		} else {
			numeric.addClass('is-hidden');
		}
	}
};

$().ready(function() {
	settings.init($('.page-nastaveni'));
});
