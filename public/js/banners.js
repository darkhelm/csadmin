var settings = {
	wrapper: null,
	inputs: null,
	init: function ($wrapper) {
		this.wrapper = $wrapper;
		// this.typeCheck();
		let bannerType = $('select[name="id_cs_banner_type"]');
		this.inputs = {
			bannerContent: $('textarea[name^="content"]').closest('tr'),
			bannerImage: $('input[name="image"]').closest('tr'),
			bannerAnnotation: $('select[name="list_annotation"]').closest('tr'),
			bannerCount: $('input[name="item_count"]').closest('tr'),
			bannerOrder: $('select[name="item_order"]').closest('tr'),
			bannerList: $('.banner_list'),
			bannerArticle: $('.banner_article'),
			bannerArticleTag: $('.banner_article_tag'),
		}

		this.changeBannerType(bannerType.val());
		bannerType.on("change", function () {
			settings.changeBannerType($(this).val());
		});

	},
	changeBannerType: function (type) {
		let $this = this;
		$.each($this.inputs, function () {
			this.hide();
		});
		if (type === '2') {
			$this.inputs.bannerContent.show();
			$('.banner_content').each(function () {
				CKEDITOR.replace(this);
			})
		} else {
			for (var i in CKEDITOR.instances) {
				CKEDITOR.instances[i].destroy(true);
			}
		}
		if (type === '1') {
			$this.inputs.bannerContent.show();
		}
		if (type === '3') {
			$this.inputs.bannerContent.show();
			$this.inputs.bannerImage.show();
		}
		if (type === '4') {
			$this.inputs.bannerCount.show();
			$this.inputs.bannerAnnotation.show();
			$this.inputs.bannerOrder.show();
			$this.inputs.bannerArticle.show();
			$this.inputs.bannerArticleTag.show();
		}
		if (type === '5') {
			$this.inputs.bannerCount.show();
			$this.inputs.bannerAnnotation.show();
			$this.inputs.bannerOrder.show();
			$this.inputs.bannerList.show();
		}
	},

	typeCheck: function () {
		let $this = this;
		let bannerType = $('.banner_type');



		if (bannerContent.length > 0) {
			if ($('.banner_type option:selected').val() !== '2') {
				if (bannerContent.hasClass('ckeditor') === true) {
					for (var i in CKEDITOR.instances) {
						CKEDITOR.instances[i].destroy(true);
					}
				}
			}

			this.wrapper.on('change', bannerType, function () {
				bannerImage.hide();
				if ($('.banner_type option:selected').val() === '2') {
					// html
					bannerContent.each(function () {
						CKEDITOR.replace(this);
					})
				} else {
					for (var i in CKEDITOR.instances) {
						CKEDITOR.instances[i].destroy(true);
					}
				}
				if ($('.banner_type option:selected').val() === '3') {
					bannerImage.show();
				}
			});
		}
	},
};

$().ready(function() {
	settings.init($('.page-bannery'));
});
