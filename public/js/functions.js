var functions = {
	dialog: null,
	wrapper: null,
	init: function ($wrapper) {
		this.dialog = $('#dialog');
		this.wrapper = $wrapper;
		this.deleteImage();
		this.delete();
		this.webSelection();
		this.cloneRow();
		this.deleteRow();
		this.switch();
		this.dragRow();
	},
	deleteImage: function () {
		var $this = this;
		this.wrapper.on('click', '.ui-delete-image', function (e) {
			e.preventDefault();
			let table = $(this).data('table');
			let id = $(this).data('id');
			let image = $(this).data('image');
			let parent = $(this).closest('td');
			let name = $(this).data('name');
			let link = $(this).data('link');
			$.ajax({
				method: 'post',
				url: '/Api/delete-image.php',
				data: {
					'id': id,
					'table': table,
					'image': image,
					'link': link,
				},
				dataType: "json",
				success: function (result) {
					if (result[0] == 'deleted') {
						let del = parent.find('a');
						del.each(function () {
							this.remove();
						});
						functions.showToast('Obrázek '+name+' byl odstraněn.','success');
					}
				}
			});
		});
	},
	dragRow: function () {
		let $sort = $(".table-draggable > tbody");
		let table = $(".table-draggable").attr("id");
		$sort.sortable({
			stop: function (event, ui) {
				let parameters = $sort.sortable('toArray');
				$.ajax({
					method: 'post',
					url: '/Api/dragrows.php',
					data: {
						'value': parameters,
						'table': table,
					},
					dataType: "html",
					success: function (result) {
						console.log(result);
						let msg = 'Přesunuto';
						let type = 'success';
						if (result === 'error') {
							msg = 'Chyba! Obnovte stránku a zkontrolujte pořadí položek.';
							type = 'error';
						}
						functions.showToast(msg,type);
					}
				});
			}
		});
	},
	switch: function () {
		var $this = this;
		this.wrapper.on('click', '.ui-switcher', function (e) {
			e.preventDefault();
			var table = $(this).data('table');
			var check = $(this).data('check');
			var web = $(this).data('web');
			var user = $(this).data('user');
			var item = $(this).data('switch');
			var td = $(this);
			$.ajax({
				method: 'post',
				url: '/Api/switch.php',
				data: {
					'web': web,
					'user': user,
					'table': table,
					'check': check,
					'item': item,
				},
				dataType: "html",
				success: function (result) {
					if (result !== 'error') {
						td.html(result);
						functions.showToast('Položka byla změněna.','success');
					} else {
						functions.showToast('Položku se nepovedlo změnit.','danger');
					}
				}
			});
		});
	},
	deleteRow: function () {
		var $this = this;
		this.wrapper.on('click', '.ui-delete-row', function (e) {
			e.preventDefault();
			var current = $(this).closest('tr');
			current.remove();
			functions.showToast('Nezapomeňte formulář uložit!','warning', 15000);
		});
	},
	cloneRow: function () {
		var $this = this;
		this.wrapper.on('click', '.btn-clone', function (e) {
			e.preventDefault();
			var data = $(this).data('data');
			var rand = 'a' + Math.random();
			var current = $(this).closest('tr');
			var $lastRow = current.prev('tr');
			var $newRow = $lastRow.clone(); //clone it
			$newRow.find('input').val(null); //clear out textbox values
			if (typeof data !== "undefined") {
				$newRow.find('input').each(function () {
					if ($(this).attr('type') === 'checkbox') {
						$(this).val(1);
					}
					$(this).attr('name', data + '[' + rand + ']' + '[' + $(this).data('name') + ']');
				});
				$newRow.find('select').each(function () {
					$(this).attr('name', data + '[' + rand + ']' + '[' + $(this).data('name') + ']');
				});
			}
			$lastRow.after($newRow); //add in the new row at the end
		});
	},
	webSelection: function () {
		var $this = this;
		this.wrapper.on('change','select[name=web-selector]', function(e) {
			var selected = $('select[name=web-selector] option:selected');
			var web = selected.val();
			var name = selected.text();
			var user = $(this).data('user');
			$.ajax({
				method: 'post',
				url: '/Api/change-web.php',
				data: {
					'web': web,
					'user': user,
					'name': name,
				},
				dataType: "json",
				success: function (result) {
					if (result === true) {
						window.location.reload();
					}
				}
			});
		});
	},
	delete: function () {
		var $this = this;
		this.wrapper.on('click','.ui-delete-link', function(e) {
			e.preventDefault();
			var table = 'cs_'+$(this).data('table');
			var id = $(this).data('id');
			var user = $(this).data('user');
			var note = $(this).data('note');
			var tr = $(this).closest('tr');
			$this.dialog.dialog({
				show: "fade",
				hide: "fade",
				modal: true,
				width: 500,
				closeOnEscape: true,
				buttons: {
					OK: function () {
						$.ajax({
							method: 'post',
							url: '/Api/delete.php',
							data: {
								table: table,
								id: id,
								user: user,
							},
							dataType: "html",
							success: function (result) {
								if (result === 'deleted') {
									$this.showToast('Položka byla smazána.', 'success');
									if (typeof tr !== "undefined") {
										tr.remove();
									}
								} else {
									$this.showToast('Položku se nepodařilo smazat.', 'error');
								}
							}
						});
						$this.dialog.removeAttr('title');
						$(this).parent().removeClass('danger');
						$(this).dialog("close");
					}
				},
				open: function () {
					$(this).parent().addClass('danger');
					$('.ui-dialog-title').html('Smazat záznam');
					$('#dialog-content').html('<p>' + note + '</p><div class="alert alert-danger" role="alert">Tento krok je nevratný!</div>');
				}
			});
		});
	},
	showToast: function(message, status, hide = 7500) {
		$.toast({
			icon: status,
			text: message,
			hideAfter: hide,
			showHideTransition: 'fade',
			position: 'top-center',
			allowToastClose: true,
		});
	}
};

$().ready(function() {
	functions.init($('.page'));
});
