<?php
include "init.php";
session_start();
header('Content-Type: application/json');

WC::app()->parseUrl($_POST['page']);
$table = str_replace('csws_','',WC::app()->get('item'));
$column = WC::app()->get('column');
$id = WC::app()->get('id');
$web = WC::app()->get('web');

$userId = $_POST['user'];

$where[] = 'id = '.intval($id);
if ($web) {
	$where[] = 'id_cs_web = '.intval($_SESSION['webId']);
}

$data = new \Classes\Db\Data($table);
$itemsCheck = $data->selectSingle([
	'column' => '*',
	'where' => implode(' AND ', $where),
]);
$active = $itemsCheck[$column] == 1 ? 0 : 1;

$r = [];
$r['action'] = 'failed';

if ($data->update([$column => $active], ['where' => implode(' AND ', $where)])) {
	if ($active == 1) {
		$r['message'] = 'Položka ' . $id . ' byla aktivována.';
		if (isset($itemsCheck['email'])) {
			$r['message'] = 'Uživatel ' . $itemsCheck['email'] . ' byl aktivován.';
		}
		$r['action'] = 'on';
	} else {
		$r['message'] = 'Položka ' . $id . ' byla deaktivována.';
		if (isset($itemsCheck['email'])) {
			$r['message'] = 'Uživatel ' . $itemsCheck['email'] . ' byl zablokován.';
		}
		$r['action'] = 'off';
	}
}

echo json_encode($r);
