<?php
//header('Content-type: application/json');

include "init.php";

$handler = new \Classes\Db\Data($_POST['table']);
if ($handler->delete(intval($_POST['id']))) {
	WC::component()->logger()->info('DELETE: ', ['table' => $_POST['table'], '#id' => $_POST['id'], 'user' => $_POST['user']]);
	print 'deleted';
}
