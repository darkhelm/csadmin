<?php
include "init.php";

$web = $_POST['web'];
$user = $_POST['user'];
$table = $_POST['table'];
$check = $_POST['check'];
$item = $_POST['item'];

$x = 'error';
$data = new \Classes\Db\Data($table);
if ($selected = $data->selectSingle(['where' => 'id = '.intval($check)])) {
	$status = $selected[$item];
	$newStatus = $status == '1' ? '0' : '1';
	if ($data->update([$item => $newStatus], ['where' => 'id = '.intval($check)])) {
		$x = '<i class="fa fa-fw fad fa-check text-success"></i>';
		if ($newStatus == '0') {
			$x = '<i class="fa fa-fw fad fa-times text-danger"></i>';
		}
	}
}

echo $x;
