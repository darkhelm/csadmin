<?php
include "init.php";

WC::app()->parseUrl($_POST['page']);
$table = str_replace('web_','',WC::app()->get('item'));
$id = $_POST['id'];

$data = new \Classes\Db\Data($table);
//$itemsCheck = $data->select([
//	'columns' => '*',
//	'where' => 'sort = 0 AND id = '.intval(WC::app()->get('id'))
//]);
//
//foreach ($itemsCheck as $item){
//	if ($item['sort'] == 0) {
//		$data->update([
//			'sort' => $item['id'],
//		],
//			['where' => 'id = ' . intval($item['id'])]
//		);
//	}
//}

$items = $data->select([
	'columns' => 'MAX(sort) as max, MIN(sort) as min',
	'where' => 'sort > 0'
]);

$min = $items[0]['min'];
$max = $items[0]['max'];

$item = $data->selectSingle([
	'columns' => '*',
	'where' => 'id = '.intval($id)
]);

$originalSort = $item['sort'];
$newSort = $_POST['position'] == 'moveUp' ? $originalSort+1 : $originalSort-1;

if ($newSort >= $min && $newSort <= $max){
	$replaced = $data->selectSingle([
		'columns' => '*',
		'where' => 'sort = '.intval($newSort)
	]);
	$replacedSort = $originalSort;

	$data->update(['sort' => $newSort],['where' => 'id = ' . intval($item['id'])]);
	$data->update(['sort' => $replacedSort],['where' => 'id = ' . intval($replaced['id'])]);

	echo $_POST['position'] == 'moveUp' ? 'down' : 'up';
}
