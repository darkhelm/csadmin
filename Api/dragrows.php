<?php
include "init.php";

$result = [];
foreach($_POST['value'] as $key=>$value) {
	$set['sort'] = $key+1;
	$result[] = update($set, $value);
}

if (in_array(false, $result)) {
	print 'error';
} else {
	print 'ok';
}

function update($set, $id) {
	$data = new \Classes\Db\Data($_POST['table']);
	$updates = [];
	foreach ($set as $k=>$v) {
		$v = "'$v'";
		$updates[] = "$k=$v";
	}
	$array = implode(',',$updates);
	return $data->update($array, ['where' => 'id = '.$id]);
}

