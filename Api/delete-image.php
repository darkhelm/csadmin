<?php
header('Content-type: application/json');
session_start();

include "init.php";

$handler = new \Classes\Db\Data($_POST['table']);
if ($handler->update([$_POST['image'] => ''], ['where' => 'id = '.intval($_POST['id'].' AND id_cs_web = '.$_SESSION['webId'])])) {
	$r[] = 'deleted';
	unlink(Dir::root().$_POST['link']);
}

print json_encode($r);
