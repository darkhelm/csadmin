<?php

use Classes\Db\Data;

include "init.php";

$data = new Data($_POST['table']);

$file_to_delete = Dir::root().'/'.$_POST['file'];
if ($web = $data->selectSingle(['columns' => '*', 'where' => 'code LIKE "'.$_POST['code'].'"'])) {
	if (is_file($file_to_delete) && $web['image'] == $_POST['file'] && $data->update(['image' => ''], ['where' => 'code LIKE "'.$_POST['code'].'"'])) {
		unlink($file_to_delete);
		print 'deleted';
	} else {
		print 'error';
	}
} else {
	print 'error';
}
