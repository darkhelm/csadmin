<?php

use Renderers\Renderer;

include 'init.php';

if ($webs = WC::component()->web()->getWebs()) {
	foreach ($webs as $web) {
		$theme = $web->getAttr('theme');
		if ($mails = WC::component()->mail()->getMails($web,'(sent IS NULL OR sent = "0000-00-00 00:00:00")')) {
			foreach ($mails as $mail) {
				$recipient = $mail->getAttr('recipient');
				$subject = $mail->getAttr('subject');

				$vars = [
					'subject' => $subject,
					'content' => $mail->getAttr('email'),
					'code' => $web->getAttr('code'),
				];
				if (file_exists(Dir::root().'/../www/public/themes/'.$theme.'/templates/mail.latte')) {
					$html = (new Renderer())->render(Dir::root().'/../www/public/themes/'.$theme.'/templates/mail.latte', $vars);
				} else {
					$html = (new Renderer())->render(\Dir::template().'/../Renderers/Mail/mail.latte', $vars);
				}
				if (\Classes\Modules\Mail\MailHelper::send([
					'recipient' => $recipient,
					'subject' => $subject,
					'html' => $html,
				], $web)) {
					$update = [
						'sent' => date('Y-m-d H:i:s')
					];
					WC::component()->mail()->update($update, $mail->getId(), $web);
				};
			}
		}
	}
}


/*
WC::component()->logger()->info('MAIL CRON');

$settings = [
	'api' => 'key-1d59a30fe4a72d020bf7806d2a7bb7bf',
	'endpoint' => 'https://api.eu.mailgun.net/v3/mg.rezervace-vstupenek.cz',
	'domain' => 'mg.rezervace-vstupenek.cz',
	'from' => 'info@rezervace-vstupenek.cz',
	'name' => 'Rezervace Vstupenek',
	'reply' => 'info@rezervace-vstupenek.cz',
	'cc' => '',
];

$mg = \Mailgun\Mailgun::create($settings['api'], $settings['endpoint']);
$mg->messages()->send($settings['domain'], [
	'o:tracking' => true,
	'from' => $settings['name'].' <'.$settings['from'].'>',
	'h:Reply-To' => $settings['reply'],
	'to' => 'apotesil@gmail.com',
	'subject' => 'test mail',
	'html' => 'test zprava',
]);

*/