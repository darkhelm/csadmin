<?php

include 'init.php';
WC::component()->logger()->info('FIO CRON');

$now = date('Y-m-d');
if ($webs = WC::component()->web()->getWebs()) {
	/** @var \Classes\Modules\Web\Web $web */
	foreach ($webs as $web) {
		$settings = $web->getSettings('banka');
		if ($settings['token'] != '' && ($settings['token_end'] == null || $settings['token_end'] == '0000-00-00' || $now <= $settings['token_end'])) {
			$datumStart = date('Y-m-d', strtotime('-30 days',strtotime($now)));
			$datumEnd = $now;
			$data = [];

			$url = 'https://www.fio.cz/ib_api/rest/periods/'.$settings['token'].'/'.$datumStart.'/'.$datumEnd.'/transactions.json';
			$data = json_decode(file_get_contents($url), true);
			$i = 0;
			$fio = [];
			if ($data) {
				foreach ($data['accountStatement']['transactionList'] as $transaction) {
					foreach ($transaction as $column) {
						if (is_array($column) && count($column) > 0) {
							$i++;
							foreach ($column as $item) {
								if (isset($item['name'])) {
									$fio[$i][$item['name']] = $item['value'];
								}
							}
						}
					}
				}
			}
		}
		if (count($fio) > 0) {
			if ($orders = WC::component()->order()->getOrders('status LIKE "R"','', $web)) {
				$success = [];
				foreach ($orders as $order) {
					foreach ($fio as $platba) {
						$vsExists = array_key_exists('VS', $platba);
						$amountExists = array_key_exists('Objem', $platba);
						if ($vsExists && $amountExists && $platba['VS'] == $order->getAttr('vs') && $platba['Objem'] == $order->getAttr('price')) {
							$update = [
								'status' => 'S',
								'approved' => date('Y-m-d H:i:s'),
								'approved_by' => 'cron',
							];
							if (WC::component()->order()->update($update, $order)) {
								$success[] = $order->getId();
								$template = \Classes\Modules\Template\TemplateHelper::getTemplate('reservation-status-change', $web);
								$vars = \Classes\Modules\Template\TemplateHelper::prepareVariables($order, $web);
								$paymentInfo = strtr($template['template'], $vars);
//								\Classes\Modules\Mail\MailHelper::send(['recipient' => $order->getAttr('email'), 'subject' => $template['subject'], 'html' => $paymentInfo], $web);
								\Classes\Modules\Mail\MailHelper::save(['recipient' => $order->getAttr('email'), 'subject' => $template['subject'], 'html' => $paymentInfo], $web);
							}
						}
					}
				}
				if ($success) {
					$insert = [
						'id_cs_web' => $web->getId(),
						'data' => json_encode($success),
					];
					if ($insert) {
						WC::component()->cron()->create($insert);
					}
				}
			}
		}
	}
}
