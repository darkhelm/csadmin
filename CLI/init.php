<?php
ob_start();

error_reporting(E_ALL);
ini_set('display_errors', 1);

ini_set('max_execution_time', 0);
//ini_set('display_errors','off');

if (file_exists(__DIR__ . "/../App/Config/Config_Local.php")){
	require_once( __DIR__ . "/../App/Config/Config_Local.php");
} else {
	require_once(__DIR__ . "/../App/Config/Config.php");
}

require_once __DIR__ . "/../vendor/autoload.php";
$db = new \Classes\Db\Database();
$colors = new ShellColors();
$fileSystem = new \Symfony\Component\Filesystem\Filesystem();
WC::init();
