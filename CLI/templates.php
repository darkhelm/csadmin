<?php
include 'init.php';

$data = new \Classes\Db\Data('cs_templates');

$return = [];
if ($webs = WC::component()->web()->getWebs()) {
	$d = new \Classes\Db\Data('cs_web_templates');
	$d->truncate();
	foreach ($webs as $web) {
		if ($templates = $data->select(['columns' => '*'])) {
			foreach ($templates as $template) {
				$insert = [
					'id_cs_web' => $web->getId(),
					'code' => $template['code'],
					'template' => $template['template'],
					'name' => $template['name'],
					'description' => $template['description'],
					'subject' => $template['name'],
					'type' => $template['type'],
				];
				$d->insert(['column' => [$insert]]);
			}
		}
	}
}

$data = new \Classes\Db\Data('cs_longtexts');
$return = [];
if ($webs = WC::component()->web()->getWebs()) {
	$d = new \Classes\Db\Data('cs_web_longtexts');
	$d->truncate();
	foreach ($webs as $web) {
		if ($templates = $data->select(['columns' => '*'])) {
			foreach ($templates as $template) {
				$insert = [
					'id_cs_web' => $web->getId(),
					'code' => $template['code'],
					'name' => $template['name'],
					'text' => $template['text'],
					'class' => $template['class'],
				];
				$d->insert(['column' => [$insert]]);
			}
		}
	}
}
